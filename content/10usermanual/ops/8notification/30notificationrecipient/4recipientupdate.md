+++
title = "更新通知对象"
description = "更新通知对象的信息，支持更新类型、显示名称、名称。 "
weight = 4
+++

更新通知对象的信息，支持更新类型、显示名称、名称。

**操作步骤**

1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**。

2. 在左侧的导航栏中单击 **通知** > **通知对象**，进入通知对象列表界面。

2. 单击待查看通知对象记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **更新**。

3. 在弹出的 **更新通知对象** 对话框中，参照 [创建通知对象]({{< relref "10usermanual/ops/8notification/30notificationrecipient/2recipientcreate.md" >}}) 更新通知对象的配置信息。
