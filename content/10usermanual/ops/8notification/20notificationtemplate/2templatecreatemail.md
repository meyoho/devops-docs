+++
title = "创建邮箱通知模板"
description = "创建邮箱通知模板。  "
weight = 2
+++

创建邮箱通知模板。


**操作步骤**

1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**。

2. 在左侧的导航栏中单击 **通知** > **通知模板**，进入通知模板列表界面。

2. 单击 **创建通知模板** 按钮，进入 **创建通知模板** 页面。

3. 在 **基本信息** 区域，配置以下参数。

	* **名称**：通知模板的名称。<br>支持输入 `a-z` 、`0-9`、`-`，并且以 `a-z`、`0-9` 开头或结尾。
	
	* **显示名称**：输入通知模板的显示名称。
	
	* **模板类型**：单击选择 **邮箱** 类型。

	* **描述**：输入模板的描述信息。
		
	
4. 在 **模板配置** 区域，配置以下参数。通知内容配置请参考 [通知模板编写说明]({{< relref "10usermanual/ops/8notification/20notificationtemplate/10templatereference.md" >}})。

	* **通知主题**：邮件主题。也可使用平台预置的默认主题。

		预置主题为：`[Alarm] {{ .status }} --{{ range $key, $value := .labels }}{{ if eq $key "alertname" }}{{ $value}}{{ end }}{{ end }}`。
		
		显示内容为：`[Alarm] 告警状态 --告警规则的名称`，例如：`[Alarm] firing --custom-hhefl-236eb1889d84e93c593fa02fd01a2769`。
	
	* **通知内容**：邮件通知的内容，默认显示预置的通知模板内容，您可根据自己的需要修改内容，模板内容只支持文本信息，且支持填写中文文本。

		预置的模板内容及说明如下：
		
		```	
		You are receiving this notification because your alarm status was changed.
		{{- $alert_object_namespace := "" -}}          // 告警资源对象所在命名空间
		{{- $alert_object_kind := "" -}}          // 告警资源对象的类型
		{{- $alert_object_name := "" -}}          // 告警资源对象的名称
		{{- $alert_indicator := "" -}}          // 告警指标
		{{- $alert_current_value := "" -}}          // 告警指标的当前值
		{{- range $key, $value := .labels }}          // 判断是否显示以上几个参数
		    {{- if eq $key "alert_involved_object_namespace" }}{{ $alert_object_namespace = $value }}{{- end }}
		    {{- if eq $key "alert_involved_object_kind" }}{{ $alert_object_kind = $value }}{{- end }}
		    {{- if eq $key "alert_involved_object_name" }}{{ $alert_object_name = $value }}{{- end }}
		    {{- if eq $key "alert_indicator" }}{{ $alert_indicator = $value }}{{- end }}
		{{- end }}
		{{- range $key, $value := .annotations }}
		    {{- if eq $key "alert_current_value" }}{{ $alert_current_value = $value}}{{- end }}
		{{- end }}
		Alarm Info:          // 如果显示以上几个参数，参数的取值
		{{- if ne $alert_object_namespace "" }}- Alarm Namespace: {{ $alert_object_namespace }}{{- end }}
		- Alarm Object: {{ $alert_object_kind }}/{{ $alert_object_name }}
		- Alarm Indicator: {{ $alert_indicator }}
		- Current Value: {{ $alert_current_value }}
		- StartTime: {{ .startsAt }}
		
		Alarm Details:          // 告警的详细信息
		status: {{ .status }}          // 告警的状态
		startsAt: {{ .startsAt }}          // 告警的开始时间
		endsAt: {{ .endsAt }}          // 告警的结束时间
		generatorURL: {{ .generatorURL }}          // 指向 Prometheus 查询表达式的地址
		labels:          // 告警的标签
		{{- range $key, $value := .labels }}
		  {{ $key }}: {{ $value }} {{- end }}
		annotations:
		{{- range $key, $value := .annotations }}
		  {{ $key }}: {{ $value }} {{- end }}	
		```
		
		例如：
		
		```
		You are receiving this notification because your alarm status was changed.
		Alarm Info:
		- Alarm Object: Cluster/global
		- Alarm Indicator: custom
		- Current Value: 1.180960578e+09
		- StartTime: 2019-09-27T05:59:56.858283194Z
		
		Alarm Details:
		status: firing
		startsAt: 2019-09-27T05:59:56.858283194Z
		endsAt: 0001-01-01T00:00:00Z
		generatorURL: <no value>
		labels:
		  alert_cluster: global
		  alert_indicator: custom
		  alert_indicator_aggregate_range: 0
		  alert_indicator_comparison: >
		  alert_indicator_threshold: 1000000000
		  alert_involved_object_kind: Cluster
		  alert_involved_object_name: global
		  alert_name: custom-ikx83
		  alertname: custom-ikx83-1dfcbafff3c4b0008e90e5eda9bc8bfe
		  namespace: alauda-system
		  pod_name: auth-controller2-6bdbff9dbc-4zbvm
		  prometheus: alauda-system/kube-prometheus
		  severity: Medium
		annotations:
		  alert_current_value: 1.180960578e+09
		  alert_notifications: [{"name":"test-xqren11","namespace":"alauda-system"}]
		```

1. 单击 **通知内容样例** 右侧的 ![](/img/00blueeye.png) 图标，可在弹出的对话框中，查看示例内容。
6. 确认信息无误后，单击 **创建** 按钮。	
