+++
title = "删除通知模板"
description = "删除不再使用的通知模板。  "
weight = 5
+++模板


删除不再使用的通知模板。

**操作步骤**

1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**。

2. 在左侧的导航栏中单击 **通知** > **通知模板**，进入通知模板列表界面。

2. 单击待查看通知模板记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**。

	**或**：
	
	1. 单击待更新 ***通知模板的名称***，进入通知模板详情页面。
	
	2. 单击右上角的 **操作** 下拉按钮，在展开的下拉列表中选择  **删除**。

4. 在弹出的确认提示框中，单击 **确定** 按钮。