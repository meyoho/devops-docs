+++
title = "通知模板编写说明"
description = "通知模板编写参考信息。  "
weight = 10
+++


通知消息（NotificationMessage）会绑定一个或多个通知（Notification），通知中的每个 Recipient 则会绑定一个通知模版（NotificationTemplate）。

通知消息与通知模版进行模版化结合后生成最终的发送内容。


以下将通过示例说明配置通知模板过程中将要用到的编写语法和规范。包括：文本和空格处理方式、字段取值、变量声明以及自定义函数的使用。



## 编写语法及规范

以下方通知消息的内容为例，说明通知模版的常规编写方法。

### 通知消息示例

```
apiVersion: aiops.alauda.io/v1beta1
kind: NotificationMessage
metadata:
  creationTimestamp: "2019-12-27T08:32:19Z"
  generateName: cpaas-admin-notification-message-
  generation: 1
  name: cpaas-admin-notification-message-pdmfw
  namespace: alauda-system
  resourceVersion: "21857916"
  selfLink: /apis/aiops.alauda.io/v1beta1/namespaces/alauda-system/notificationmessages/cpaas-admin-notification-message-pdmfw
  uid: 65a0be33-2883-11ea-b912-0a0000c70008
spec:
  body:
    annotations:
      alert_current_value: "7.63"
      alert_notifications: '[{"namespace":"alauda-system","name":"cpaas-admin-notification"}]'
    endsAt: "2019-12-19T03:55:26.738211566Z"
    labels:
      alert_cluster: global
      alert_indicator: cluster.kube.apiserver.request.latency
      alert_indicator_aggregate_range: "0"
      alert_indicator_comparison: '>'
      alert_indicator_threshold: "0.1"
      alert_involved_object_kind: Cluster
      alert_involved_object_name: global
      alert_name: cluster.kube.apiserver.request.latency-hjzmp
      alert_resource: cpaas-cluster-status-rules
      alertname: cluster.kube.apiserver.request.latency-hjzmp-b456a6eccecfeffd2ec69f7937d901f8
      cluster: global
      env: ait-dev
      prometheus: alauda-system/kube-prometheus
      severity: Medium
    startsAt: "2019-12-19T03:55:26.738211566Z"
    status: resolved
  notifications:
  - name: cpaas-admin-notification
```


### 文本和空格编写规范

模版只转换位于 `{{ }}` 中的内容，`{{ }}` 之外的其他文本、空格等内容将会原样输出。

其中：

* `{{-`：会删除 `{{` 左侧的空白内容，包括：换行符、空格、Tab（制表位）。

* `-}}`： 会删除 `}}` 右侧的空白内容，包括：换行符、空格、Tab（制表位）。

**内容编写示例**：

```
比较下面两个数的大小。
  {{23 -}} _ {{- 45}}
```

**输出效果**：

```
比较下面两个数的大小。
  23_45
```


### 引用字段取值

当模板中想引用通知消息中的某个或某些字段的取值时，可通过 `.<字段名称>` 的方式进行引用。

**说明**：字段名称位置需输入通知消息中实际存在的字段名称。例如：`.startsAt`。

**内容编写示例**：

```
{{ .startsAt }}                             # 取出通知消息 Body 中 startsAt 的值
# 2019-12-19T03:55:26.738211566Z
{{ .labels.alert_name }}                    # 取出通知消息 Body 中 labels 的 alert_name 的值
# cluster.kube.apiserver.request.latency-hjzmp
{{ .annotations.alert_current_value }}      # 取出通知消息 Body 中 annotations 中 alert_current_value 的值
# 7.63
```


### 声明变量

通过 `$<变量名称>` 声明一个变量，并通过 `$<变量名称> := .<字段名称>` 的方式，将通知消息中字段的取值赋值给该变量。

**内容编写示例**：

```
{{ $alert_name := .labels.alert_name }}             # 声明了一个 alert_name 的变量，取值 .labels.alert_name；要用 $alert_name 引用这个变量
{{ range $key, $value := .labels }}...{{ end }}     # 遍历 .labels，分别将其下的 label 的 key 和 valeu 赋给变量 $key 和 $value，进行引用
```

### 使用自定义函数

支持基于 `text/template` 引入的部分自定义函数编写模板，说明参见下表。


<style>table th:first-of-type { width: 10%;}</style> 

<style>table th:nth-of-type(2) { width: 40%;}</style>

<style>table th:nth-of-type(3) { width: 20%;}</style>

| 函数名称   | 作用           |   语法                | 示例                    |
| --------- | --------- | --------- | -------------------- |
| dateFormat | 将日期类型的字符串转化为特定格式的日期字符串。 | dateFormat DateString TimeLayout | `input: dateFormat "2019-12-19T03:55:26.738211566Z" "2006-01-02T15:04:05Z"`<br>`output: "2019-12-19T03:55:26Z"` |
| float      | 将 float 字符串转化为 float 数值。   | float FloatString                | `input: float "4.3423"output: 4.3423`                          |

**内容编写示例**：

```
{{ float .annotations.alert_current_value | printf "%2f" }} # 将.annotations.alert_current_value 转为float类型数值，再进行格式化输出，保留两位小数
# 7.63
{{ dateFormat .startsAt "2006-01-02T15:04:05Z" }}       # 将.startsAt转化为"2006-01-02T15:04:05Z"格式
# "2019-12-19T03:55:26Z"
```





