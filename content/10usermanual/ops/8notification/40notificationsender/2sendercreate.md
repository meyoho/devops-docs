+++
title = "创建邮件通知发送人"
description = "创建邮件通知发送人。  "
weight = 2
+++

创建邮件通知发送人。可为同一个邮箱通知服务器添加多个通知发送人。

**说明**：目前，仅支持创建邮箱通知发送人，如需配置短信通知发送人，请联系我们的运维工程师获得支持。

**前提条件**

* 在创建通知发送人之前需要您已经创建了一个有效的通知服务器。

* 已获取到经过通知服务器认证的有效邮箱账号。

**操作步骤**

1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**。

2. 在左侧的导航栏中单击 **通知** > **通知发送人**，进入通知发送人列表界面。

2. 单击 **添加发送人** 按钮，弹出 **添加发送人** 对话框。

3. 单击 **邮件服务器** 右侧的下拉选择框，选择已创建的邮箱类型的通知服务器名称。

	**注意**：需要确保您选择的通知服务器是有效的，否则，无法创建通知发送人。

4. 在 **邮箱** 右侧的输入框中，输入已通过服务器认证的有效的邮箱地址。

5. 在 **密码** 右侧的输入框中，输入邮箱对应的密码。

	**提示**：单击输入框右侧的 ![](/img/eyesecret.png) 图标，可显示密码。
	
1. 确保信息无误后，单击 **添加** 按钮。	

	**注意**：通知服务器配置错误，或邮箱、密码配置错误都会导致添加发送人失败。