+++
title = "通知服务器（Alpha）"
description = "通知支持以邮件、短信、Webhook、钉钉、企业微信的形式，发送平台的运行状况，例如：平台监控、告警等信息。"
weight = 50
alwaysopen = false
+++

通知服务器用于向通知对象发送通知，目前，平台仅支持通过界面配置邮件通知服务器。

如果您需要使用短信通知服务器发送短信通知，请联系我们的运维工程师获得支持。


{{%children style="card" description="true" %}}





