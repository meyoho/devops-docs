+++
title = "通知"
description = "通知支持以邮件、短信、Webhook、钉钉、企业微信的形式，发送平台的运行状况，例如：平台监控、告警等信息。"
weight = 80
alwaysopen = false
+++

为满足企业快速创建通知的场景，支持灵活配置通知服务器、通知发送人、通知模板和通知对象。您可以根据自身业务需求，基于已创建的通知服务器、通知发送人、通知模板和通知对象自定义通知内容，提升通知内容的可读性和用户体验。

通知支持以邮件、短信、Webhook、钉钉、企业微信的形式，发送平台的运行状况，例如：平台监控、告警等信息。

创建不同的通知的基本流程如下图所示。

<img src="/img/notificationflow.png" width = "500" />

{{%children style="card" description="true" %}}





