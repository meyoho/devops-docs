+++
title = "查看通知详情"
description = "查看通知的详细信息。  "
weight = 3
+++


查看已创建通知的详细信息。

**操作步骤**

1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**。

2. 在左侧的导航栏中单击 **通知** > **通知**，进入通知列表界面。

2. 单击待查看 ***通知名称***，进入通知详情页面。

3. 在通知详情页面，查看通知的基本信息和通知方式。