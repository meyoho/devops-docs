+++
title = "通知"
description = "通知支持以邮件、短信、Webhook、钉钉、企业微信的形式，发送平台的运行状况，例如：平台监控、告警等信息。"
weight = 10
alwaysopen = false
+++

通知支持以邮件、短信、Webhook、钉钉、企业微信的形式，发送平台的运行状况，例如：平台监控、告警等信息。

支持基于已创建的的通知服务器、通知发送人、通知模板和通知对象灵活创建通知，同时，支持查看、更新和删除已创建的通知。


{{%children style="card" description="true" %}}





