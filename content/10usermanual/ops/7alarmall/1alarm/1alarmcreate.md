+++
title = "创建告警"
description = "结合监控指标为集群和主机创建指标告警和自定义告警，设置告警的监控指标信息、告警阈值和告警通知方式等参数。  "
weight = 2
+++

结合监控指标为集群和主机创建指标告警和自定义告警，设置告警的监控指标信息、告警阈值和告警通知方式等参数。

**说明**：在运维中心，只能为当前集群和集群下主机创建指标告警和自定义告警。计算组件的告警（指标告警、自定义告警、日志告警）可在业务视图具体的计算组件详情页面中的告警页签下创建。

**前提条件**

创建告警的集群已部署了监控服务。

**操作步骤**


1. 登录平台，单击顶部导航栏的 **平台中心** > **运维中心**，并切换至要创建告警的集群。

2. 在左侧导航栏单击 **告警** > **告警**，进入告警列表页面。

3. 通过顶部导航栏的集群切换下拉选择框切换待创建告警的集群。 

2. 在告警列表页面，单击 **创建告警** 按钮，进入创建告警页面。

3. 在 **基本信息** 区域，配置告警的基本信息。
		
	* **名称**：输入告警的名称。<br>支持输入 `a-z` 、`0-9`、`-`、`.`，并且以 `a-z` 、`0-9` 开头或结尾。
	
	* **描述**：输入告警的描述信息。
	
	* **集群**：根据当前集群的监控指标为集群创建告警。

	* **主机**：根据集群内某个主机的监控指标为主机创建告警，需在 **资源名称** 下拉选择框中，选择集群内的一个主机或全部主机。

1. 在 **告警规则** 区域，单击 **添加告警规则**，弹出 **添加告警规则** 对话框。

	**说明**：在 **添加告警规则** 对话框的最上方显示的监控图表，会根据后续选择的监控指标和聚合时间实时变化。
		
	<img src="/img/nodemonitor.png" width = "600" />

2. 选择告警类型。

	* **指标告警**：根据平台预设的监控指标创建的告警。
		
	* **自定义告警**：根据用户自定义监控指标创建的告警。
	
1. 参照以下说明配置 **指标**、**数据类型**、**单位** 等依赖于 **告警类型** 的参数。
	
	* 当 **告警类型** 是 **指标告警** 时：
	
		1. 通过单击 **指标** 项右侧的下拉选择框选择平台预设的监控指标，参见 [预设指标说明](#1)。

			**提示**：当鼠标滑过监控指标选项时，可查看该指标说明信息。
		
		2. 单击选择 **数据类型**，选择如何采集监控指标数据，支持选择 **原始值** 或 **聚合值**。
		
			* **原始值**：采集监控指标的原始数据。
		
			* **聚合值**：采集一段时间的监控指标后，根据某种规则进行处理后得到的数据。根据处理后的数据判断是否达到告警阈值。需要配置以下两个参数：
			
				* **聚合时间**：选择要采集监控指标数据的时长。可选择 1 分钟、2 分钟、3 分钟、5 分钟、10 分钟、30 分钟。

				* **聚合方式**：选择处理监控指标数据的方式，支持选择 **平均值**、**最大值**、**最小值**。
			
				例如：**聚合时间** 选择 **5 分钟**，**聚合方式** 选择 **平均值**，那么将根据 5 分钟内的监控指标数据平均值，判断是否达到告警阈值。
		
	* 当 **告警类型** 是 **自定义告警** 时：
	
		1. 在 **指标** 输入框中，需要按照您的特定监控场景，自行添加专属的指标规则，满足监控告警方面的高级需求。需要手动输入 Prometheus 可以识别的监控指标算法和指标，例如：`rate(node_network_receive_bytes{instance="$server",device!~"lo"}[5m])`。
		
		2. （非必填）在 **单位** 框中，选择下拉列表框中的监控指标单位，或手动输入自定义单位。

1. 在 **告警等级** 下拉框中，选择告警等级，用于标识告警的严重程度。

	支持选择 **Critical**、**High**、**Medium**、**Low**，按照严重程度排序为：**Critical** > **High** > **Medium** > **Low**。
	
1. 在 **阈值** 区域，通过单击下拉选择框选择比较运算符，并输入阈值。通过监控指标和告警阈值的比较运算结果，判断是否告警。

	* 比较运算符可选择 **>**、**>=**、**==**、**<=**、**<**、**!=**（不等于）。
	
	* 告警阈值仅接受数字，可保留4位小数，整数部分最多 17 位。

1. 在 **持续时间** 框中，选择指标数据持续达到告警阈值多长时间后，触发告警。

1. 单击 **高级**，展开标签（labels）、注解（annotations）配置项。

	* 在标签或注解区域单击 **添加**，输入 **键** 和 **值**。支持添加多条标签和注解。
	
	* 单击已添加标签或注解记录右侧的 ![](/img/delete.png) 图标，可删除该记录。

1. 单击 **添加** 按钮，可添加一条告警规则。

	**提示**：
	
	* 参照以上操作，支持为告警添加多条告警规则。
	
	* 单击已添加告警规则记录右侧的  ![](/img/editor.png) 图标，可编辑已添加告警规则。
	
	* 单击已添加告警规则记录右侧的  ![](/img/delete.png) 图标，可删除已添加告警规则。

5. 在 **操作指令** 区域，支持设置触发告警后的自动化操作，例如：自动发送告警通知。  
	
	单击 ![](/img/add.png)，在新增的记录的 **操作类型** 下拉选择框中，选择 **通知**；在 **资源** 栏中，通过单击选择一个或多个平台中已存在的通知。 

6. 确认信息无误后，单击 **创建** 按钮。

**补充说明**


平台<span id="1">预设的集群、主机监控指标</span>说明如下：

* 集群相关监控指标

	| 指标名称                                    | 指标单位   | 指标含义                                                     |
	| ------- | -------- | -------- |
	| cluster.node.total.count                    | 个         | 集群的主机数。                                               |
	| cluster.node.not.ready.count                | 个         | 集群中不健康的主机数。                                       |
	| cluster.cpu.utilization                     | %(百分比)  | 集群的 CPU 使用率。                                          |
	| cluster.memory.utilization                  | % (百分比) | 集群的内存使用率。                                           |
	| cluster.resource.request.cpu.utilization    | % (百分比) | 集群中 Kubernetes 资源申请的 CPU 大小（resources.requests.cpu）占集群 CPU 大小的比率。<br>**说明**：该值由集群中实际存在的 Kubernetes 资源及其配额需求值决定。 |
	| cluster.resource.request.memory.utilization | % (百分比) | 集群中 Kubernetes 资源申请的内存大小（resources.requests.memory）占集群内存大小的比率。<br>**说明**：该值由集群中实际存在的 Kubernetes 资源及其配额需求值决定。 |
	| cluster.kube.apiserver.up                   | -          | 集群的 kube-apiserver 是否正常，每个 kube-apiserver 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.apiserver.health               | % (百分比) | 集群的 kube-apiserver 的健康程度，例如：三个中两个正常，健康程度就是 66.6%。 |
	| cluster.kube.controller.manager.up          | -          | 集群的 kube-controller-manager 是否正常，每个 kube-controller-manager 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.controller.manager.health      | % (百分比) | 集群的 kube-controller-manager 的健康程度，例如：三个中两个正常，健康程度就是 66.6%。 |
	| cluster.kube.scheduler.up                   | -          | 集群的 kube-scheduler 是否正常，每个 scheduler 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.scheduler.health               | % (百分比) | 集群的 kube-scheduler 的健康程度，例如：三个中两个正常，健康程度就是 66.6%。 |
	| cluster.docker.daemon.up                    | -          | 集群的每个主机的 docker daemon 是否正常，每个 docker daemon 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.docker.daemon.health                | % (百分比) | 集群的 docker daemon 的健康程度，例如：十个中九个正常，健康程度就是 90.0%。 |
	| cluster.kube.dns.up                         | -          | 集群的 kube-dns 是否正常，每个 dns 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.dns.health                     | % (百分比) | 集群的 kube-dns 的健康程度，例如：两个中一个正常，健康程度就是 50.0%。 |
	| cluster.kube.etcd.up                                   | -           | 集群的 kube-etcd 是否正常，每个 ETCD 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.etcd.health                               | % (百分比)    | 集群的 kube-etcd 的健康程度，例如：三个中两个正常，健康程度就是 66.6%。 |
	| cluster.kube.kubelet.up                                | -           | 集群的每个主机 kubelet 是否正常，每个 kubelet 会有一条指标。<br>正常：`1`；<br>异常：`0`。 |
	| cluster.kube.kubelet.health                            | % (百分比)    | 集群的 kubelet 的健康程度，例如：十个中九个正常，健康程度就是 90.0%。 |
	| cluster.kube.proxy.up                                  | -           | 集群的 kube-proxy 是否正常，每个 kube-proxy 会有一条指标。<br/>正常：`1`；<br/>异常：`0`。 |
	| cluster.kube.proxy.health                              | % (百分比)    | 集群的 kube-proxy 的健康程度，例如：十个中九个正常，健康程度就是 90.0%。 |
	| cluster.node.ready                                     | -           | 集群的 Node 是否是 ready 状态，每个一条指标。<br>ready：`1`；<br>未 ready：`0`。 |
	| cluster.node.health                                    | % (百分比)    | 集群的 Node 的健康程度，例如：十个中九个正常，健康程度就是 90.0%。   |
	| cluster.node.network.ipvs.active.connections           | 个          | 集群所有主机的活动连接数总和，包含主机上的本地连接和远程连接。 |
	| cluster.alerts.firing                                  | 个          | 集群的处于告警状态的告警数。                                  |
	| cluster.kube.apiserver.request.latency                 | ms（毫秒）    | 集群的所有 apiserver 过去 5 分钟的平均响应时间。              |
	| cluster.kube.apiserver.request.latency.per.verb        | ms（毫秒）    | 集群的所有 apiserver 过去 5 分钟的平均响应时间，按照请求方法进行分组，每种方法生成一条指标。 |
	| cluster.kube.apiserver.request.latency.per.instance    | ms（毫秒）    | 集群中每个 apiserver 过去 5 分钟的平均响应时间，按照实例进行分组，每个实例生成一条指标。 |
	| cluster.kube.apiserver.request.count                   | -           | 集群的所有 apiserver 过去 5 分钟的请求次数。                  |
	| cluster.kube.apiserver.request.count.per.instance      | -           | 集群的所有 apiserver 过去 5 分钟的请求次数，按照实例进行分组，每个实例生成一条指标。 |
	| cluster.kube.apiserver.request.error.rate              | % (百分比)    | 集群的所有 apiserver 过去 5 分钟的请求错误率(大于等于 300 的请求视为返回错误）。 |
	| cluster.kube.apiserver.request.error.rate.per.instance | % (百分比)    | 集群的所有 apiserver 过去 5 分钟的请求错误率，按照实例进行分组，每个实例生成一条指标。 |
	| cluster.kube.apiserver.request.count.2xx               | -           | 集群中每个 apiserver 过去 5 分钟返回码为 2xx 的请求数。       |
	| cluster.kube.apiserver.request.count.2xx.per.instance  | -           | 集群中每个 apiserver 过去 5 分钟返回码为 2xx 的请求数，在监控图表中每个 apiserver 用一条线表示。 |
	| cluster.kube.apiserver.request.count.4xx               | -           | 集群中每个 apiserver 过去 5 分钟返回码为 4xx 的请求数。       |
	| cluster.kube.apiserver.request.count.4xx.per.instance  | -           | 集群中每个 apiserver 过去 5 分钟返回码为 4xx 的请求数，在监控图表中每个 apiserver 用一条线表示。 |
	| cluster.kube.apiserver.request.count.5xx               | -           | 集群中每个 apiserver 过去 5 分钟返回码为 5xx 的请求数。       |
	| cluster.kube.apiserver.request.count.5xx.per.instance  | -           | 集群中每个 apiserver 过去 5 分钟返回码为 5xx 的请求数，在监控图表中每个 apiserver 用一条线表示。 |
	| cluster.kube.etcd.has.leader                           | -           | 集群中的 ETCD 是不是有 Leader<br>有：`1`；<br/>无：`0`。 |
	| cluster.kube.etcd.leader.elections                     | -           | 集群中的 ETCD 过去 15 分钟发生 leader 选举的次数。       |
	| cluster.kube.etcd.members                              | -           | 集群中的 ETCD 的成员数。                           |
	| cluster.kube.etcd.db.size                              | bytes（字节） | 集群中的 ETCD 的所有实例的 DB（数据库）大小之和。           |
	| cluster.kube.etcd.db.size.per.instance                 | bytes（字节）   | 集群中的 ETCD 的实例的 DB 大小。在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.rpc.rate                             | -           | 集群中的 ETCD 过去 5 分钟的 RPC（Remote Procedure Call，远程过程调用） 次数。 |
	| cluster.kube.etcd.rpc.failed.rate                      | -           | 集群中的 ETCD 过去 5 分钟的 RPC 失败次数。                 |
	| cluster.kube.etcd.active.watch.streams                 | -           | 集群中的 ETCD 的当前还在活跃的 watcher（监听） 数。       |
	| cluster.kube.etcd.active.lease.streams                 | -           | 集群中的 ETCD 的当前还在活跃的 lease（租约） 数。            |
	| cluser.kube.etcd.raft.proposal.committed.rate          | ops/second  | 集群中的 ETCD 过去 5 分钟的提议提交速率。                  |
	| cluser.kube.etcd.raft.proposal.pending.rate            | ops/second  | 集群中的 ETCD 过去 5 分钟的提议排队速率。                   |
	| cluser.kube.etcd.raft.proposal.failed.rate             | ops/second  | 集群中的 ETCD 过去 5 分钟的提议失败速率。                  |
	| cluser.kube.etcd.raft.proposal.applied.rate            | ops/second  | 集群中的 ETCD 过去 5 分钟的提议应用速率。                  |
	| cluster.kube.etcd.wal.fsync.duration.per.instance      | ms（毫秒）    | 集群中的 ETCD 的实例过去 5 分钟 WAL（Write-Ahead Log，预写式日志）同步时间，在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.db.fsync.duration.per.instance       | ms（毫秒）    | 集群中的 ETCD 的实例过去 5 分钟 DB 同步时间，在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.client.traffic.in                    | byte/second(字节/秒) | 集群中的 ETCD 的客户端过去 5 分钟的网络流量接收速率。      |
	| cluster.kube.etcd.client.tranffic.in.per.instance      | byte/second(字节/秒) | 集群中的 ETCD 的客户端过去 5 分钟的网络流量接收速率，在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.client.traffic.out                   | byte/second(字节/秒) | 集群中的 ETCD 的客户端过去 5 分钟的网络流量发送速率。      |
	| cluster.kube.etcd.client.tranffic.out.per.instance     | byte/second(字节/秒) | 集群中的 ETCD 的客户端过去 5 分钟的网络流量发送速率，在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.peer.tranffic.in                     | byte/second(字节/秒) | 集群中的 ETCD 的 peer（对同一个 ETCD 集群中另外一个 Member（成员） 的称呼）过去 5 分钟的网络流量接收速率 |
	| cluster.kube.etcd.peer.tranffic.in.per.instance        | byte/second(字节/秒) | 集群中的 ETCD 的 peer 过去 5 分钟的网络流量发送速率，在监控图表中每个实例用一条线表示。 |
	| cluster.kube.etcd.peer.traffic.out                     | byte/second(字节/秒) | 集群中的 ETCD 的 peer 过去 5 分钟的网络流量接收速率。      |
	| cluster.kube.etcd.peer.tranffic.out.per.instance       | byte/second(字节/秒) | 集群中的 ETCD 的 peer 的网络流量发送速率，在监控图表中每个实例用一条线表示。 |
	| cluster.pod.restarted.total                            | 个        | 集群中过去 15 分钟内发生重启的 Pod 的个数。                  |
	| cluster.pod.restarted.count                            | 个         | 集群中过去 15 分钟内发生重启的 Pod，在监控图表中每个 Pod 用一条线表示，对应的值的是该 Pod 发生重启的次数。 |
	| cluster.pod.status.phase.not.running                   | 个         | 集群当前处于非正常状态的 Pod数，排除 Running 和 Succeeded 状态。 |
	| cluster.pod.status.phase.running                       | 个        | 集群中当前处于 Running 状态的 Pod 数。                       |
	| cluster.pod.status.phase.succeeded                     | 个         | 集群中当前处于 Succeeded 状态的 Pod 数。                    |
	| cluster.pod.status.phase.failed                        | 个          | 集群中当前处于 Failed 状态的 Pod 数。                        |
	| cluster.pod.status.phase.pending                       | 个          | 集群中当前处于 Pending 状态的 Pod 数。                       |
	| cluster.pod.status.phase.unknown                       | 个          | 集群中当前处于 Unknown 状态的 Pod 数。 |


* 主机（Node）相关监控指标

	| 指标名称                                 | 指标单位             | 指标含义                                                     |
	| ------- | ----- | ------- |
	| node.cpu.utilization                     | %(百分比)            | 主机的 CPU 使用率。                                          |
	| node.memory.utilization                  | % (百分比)           | 主机的内存使用率。                                           |
	| node.load.1                              | -                    | 主机过去 1 分钟的负载。                                      |
	| node.load.5                              | -                    | 主机过去 5 分钟的负载。                                      |
	| node.load.15                             | -                    | 主机过去 15 分钟的负载。                                     |
	| node.disk.utilization                    | %(百分比)            | 主机根目录的磁盘利用率。                                     |
	| node.disk.space.utilization              | %(百分比)            | 主机根目录的磁盘使用率。                                     |
	| node.disk.inode.utilization              | %(百分比)            | 主机根目录的 inode（索引节点）的使用率。                     |
	| node.disk.read.bytes                     | byte/second(字节/秒) | 主机每秒读磁盘的字节数。                                     |
	| node.disk.written.bytes                  | byte/second(字节/秒) | 主机每秒写磁盘的字节数。                                     |
	| node.disk.io.time                        | ms(毫秒)             | 主机每秒在磁盘读写上使用的时间。                             |
	| node.network.receive_bytes               | byte/second(字节/秒) | 主机网卡每秒接受字节数。                                     |
	| node.network.transmit_bytes              | byte/second(字节/秒) | 主机网卡每秒发送字节数。                                     |
	| node.resource.request.cpu.utilization    | %(百分比)            | 主机上 Kubernetes 资源申请的 CPU 大小（resources.requests.cpu）占主机 CPU 大小的比率。<br>**说明**：该值由主机上实际存在的 Kubernetes 资源及其配额需求值决定。 |
	| node.resource.request.memory.utilization | %(百分比)            | 主机上 Kubernetes 资源申请的内存大小（resources.requests.memory）占主机内存大小的比率。<br>**说明**：该值由主机上实际存在的 Kubernetes 资源及其配额需求值决定。 |
	| node.network.ipvs.active.connections     | 个                   | 主机的连接数，包含主机上的本地连接和远程连接。               |
