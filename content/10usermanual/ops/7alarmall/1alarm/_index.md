+++
title = "告警"
description = "平台支持平台管理员在运维中心快捷设置集群、节点的监控指标告警，也可以根据实际需求自定义监控指标算法，增加需要的告警指标及规则。"
weight = 1
alwaysopen = false
+++

在运维中心，平台管理员可对集群和节点设置如下两种告警：

* 指标告警：是平台根据客户需求而提炼的满足大部分客户需求大多数的指标项，通过选择企业可快速达到对业务的监控及告警。<br>指标告警是根据监控指标设置的一些条件和阈值，当指标数据达到告警阈值后，可以根据设定的通知，通过短信、邮件和 Webhook 等方式发送告警。

* 自定义告警：则可由客户按照自己公司的使用场景，添加企业专属的指标规则，更好的满足企业对于告警的高阶需求。


<img src="/img/alert.png" width = "860" />

{{%children style="card" description="true" %}}





