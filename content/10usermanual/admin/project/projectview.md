+++
title = "查看项目详情"
description = "在项目的详情页面，查看详细信息、DevOps 工具链、资源、项目成员信息。"
weight = 1
+++

在项目的详情页面，查看项目详细信息、DevOps 工具链、流水线模版等信息。
	
**操作步骤**
	
1. 登录 DevOps 平台，进入管理视图后，单击 **项目**。
	
2. 在项目页面，单击要查看详情的 ***项目名称***。
	
3. 在 **详细信息** 栏，查看项目的基本信息。例如：项目名称、显示名称、创建时间、描述。

4. 在 **DevOps 工具链** 栏，查看项目的工具信息。例如：已绑定的持续集成服务、代码仓库服务、制品仓库服务等。  
		
	* 找到您想要查看详情的工具，单击 ***工具名称***。

		**提示**：如果已绑定的工具较多，单击不同分类，进入具体的分类页面查找。

	* 在绑定工具的详情页面，查看工具的服务名称、绑定时间、凭据等信息，不同工具的详情页面如下：
		
		* 持续集成：在 **流水线** 栏，查看绑定的 Jenkins 信息，例如 Jenkins 实例下的流水线名称、触发器。在 **名称** 栏，同时显示了流水线的创建方式，![pipelinescript](/img/pipelinescript.png) 表示流水线使用脚本的方式创建，![pipelinetemplate](/img/pipelinetemplate.png) 表示流水线使用模版的方式创建，![multibranchpipe](/img/multibranchpipe.png) 表示流水线是多分支流水线，![picturepipe](/img/picturepipe.png) 表示流水线是图形化流水线。
		
		* (Alpha)项目管理：在 **项目管理** 栏，查看绑定的项目管理服务，例如绑定 Jira 的项目名称、项目 ID、项目负责人等，参考 [查看 Issues 列表]({{< relref "10usermanual/project/scrum/viewscrum.md" >}})。
		
		* 代码仓库：在 **仓库名称** 栏，查看绑定的代码仓库信息，例如仓库名称、代码仓库地址、已用容量，参考 [查看代码仓库]({{< relref "10usermanual/project/coderepo/viewcoderepo.md" >}})；同时可以分配代码仓库，参考 [分配代码仓库]({{< relref "10usermanual/admin/project/toolchain/bindcoderepo/assigncoderepo.md" >}})。

		* 制品仓库：在 **制品仓库** 栏，查看绑定的镜像仓库信息，例如 Harbor 的仓库地址，参考 [查看制品仓库]({{< relref "10usermanual/project/artifact/viewartifact.md" >}})。
		
		* 代码质量分析：在 **代码质量分析** 栏，查看绑定的代码质量服务详情，例如绑定的代码仓库、质量阈、最新扫描时间等，参考 [查看代码质量分析]({{< relref "10usermanual/project/sonar/viewsonar.md" >}})。

		* 测试管理：在 **测试管理** 栏，查看绑定的测试管理服务详情，例如测试项目名称、测试用例数量。

5. 在 **流水线模版** 栏，您可以进行 **自定义模版配置** 和查看现有 **项目自定义模版**。

	* **自定义模版配置**：当系统提供的流水线模版不满足需求时，支持导入自定义模版。参考 [配置模版仓库]({{< relref "10usermanual/admin/project/template/setuppipelinerepo.md" >}})。  
	配置自定义模版后，平台会显示模版仓库地址、代码分支、同步时间和结果等。**同步结果** 后面的显示图标代表了本次模版同步的状态和数量，绿色代表成功、红色代表失败、灰色代表无更新。单击显示图标，查看详情。

	* **项目自定义模版**：显示了项目自定义模版。单击某个模版，在模版窗口中，查看流水线模版的基本信息和具体参数。
