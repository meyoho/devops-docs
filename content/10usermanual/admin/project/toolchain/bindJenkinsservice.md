+++
title = "绑定 Jenkins 服务"
description = "绑定 Jenkins 服务，实现服务的持续集成和升级。"
weight = 1
+++

绑定 Jenkins 服务，实现服务的持续集成和升级。

多个项目可以绑定同一个 Jenkins 服务，一个项目也可以绑定多个 Jenkins 服务。

**前提条件**

绑定 Jenkins 服务前，需要集成 Jenkins 服务并创建凭据。  

集成 Jenkins 服务请参考 [集成工具]({{< relref "10usermanual/admin/service/addintegration.md" >}})，创建凭据请参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md#2" >}})。
	
**操作步骤**
	
1. 登录 DevOps 平台，进入管理视图后，单击 **项目**。
	
2. 在项目页面，单击要绑定 Jenkins 服务的 ***项目名称***。
	
3. 在项目页面，单击 **DevOps 工具链** > **绑定**。
	
4. 在 **绑定** 窗口，选择一个已集成的 Jenkins 服务。
	
5. 在绑定页面，绑定 Jenkins：
	
	* 在 **基本信息** 区域填写名称、描述信息。
   
	   * 在 **名称** 框中，输入绑定 Jenkins 服务的名称。  
		名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
	
	   * (非必填) 在 **描述** 框中，输入绑定 Jenkins 服务的描述信息。
  
   * 在 **认证** 区域，选择认证方式、凭据。

	   * 认证方式：目前只支持并默认选择 Token 认证，您需要去仓库生成 Token，再将 Token 添加到凭据。
	   
	   * 在 **凭据** 框中，在下拉菜单中选择一个已创建的凭据。如果凭据列表中没有要使用的凭据，单击 **添加凭据**，创建新的凭据，参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md#2" >}})。
	
5. 单击 **绑定账号**。单击绑定账号后，会判断选择的凭据是否有权限去访问 Jenkins，如果没有权限，会提示错误。


