+++
title = "DevOps 工具链"
description = "绑定 DevOps 工具链，在项目中使用相关工具。"
weight = 2
alwaysopen = false
+++

绑定 DevOps 工具链，在项目中使用相关工具。DevOps 工具链支持绑定：

* 绑定持续集成服务：平台支持集成绑定 Jenkins。集成 Jenkins 后，您可以通过编排流水线，做多语言的持续构建，持续集成，自动化的代码与镜像安全管理、镜像同步等，在 DevOps 上集成并绑定 Jenkins 服务，实现服务的持续集成和升级。参考 [绑定 Jenkins 服务]({{< relref "10usermanual/admin/project/toolchain/bindJenkinsservice.md" >}})。

* 绑定项目管理服务：平台支持集成绑定项目管理平台 (Alpha)Jira，管理源项目流程，应用于事务的缺陷追踪、需求收集、流程审批，任务跟踪等工作领域，实现敏捷管理。参考 [绑定项目管理服务]({{< relref "10usermanual/admin/project/toolchain/bindpm/_index.md" >}})。

* 绑定代码仓库服务：平台支持绑定多种代码仓库，例如：GitHub、Bitbucket、GitLab 公有和私有仓库、码云等。同时，支持多种代码库管理服务，例如：通过平台查看代码仓库信息等。将代码集成到 DevOps 平台，针对不同的项目，分配可用的代码仓库。在整个研发流程中，让用户专注于业务本身，不再关注代码仓库地址、凭据等信息，通过简单的选择即可快速、便捷的使用代码仓库。参考 [绑定代码仓库服务]({{< relref "10usermanual/admin/project/toolchain/bindcoderepo/_index.md" >}})。

* 绑定制品仓库服务：平台集成了多种制品仓库来管理镜像，实现镜像的存储、管理等，例如：Harbor Registry、Docker Registry、Maven。同时平台集成了制品仓库管理工具 （Alpha）Nexus、(Alpha）JForg Artifactory，简化了内部仓库的维护和外部仓库的访问。参考 [绑定制品仓库服务]({{< relref "10usermanual/admin/project/toolchain/bindregistry/_index.md" >}})。

* 绑定代码质量分析服务：绑定代码质量管理平台 SonarQube，管理源代码的质量，执行流水线，进行静态代码扫描，对代码质量做自动化分析和管理。参考 [绑定代码质量分析服务]({{< relref "10usermanual/admin/project/toolchain/bindsonar.md" >}})。

* 测试管理：例如测试管理工具 (Alpha）TestLink，通过自动化的流水线，实现测试用例的管理，提高测试人员的工作效率。参考 [绑定测试管理服务]({{< relref "10usermanual/admin/project/toolchain/bindtest.md" >}})。


