+++
title = "更新仓库服务信息"
description = "更新已绑定的代码仓库、制品仓库或代码检查的基本信息，例如描述信息、更换绑定的凭据等。服务的名称不支持更新。"
weight = 12
+++

更新已绑定的代码仓库、制品仓库或代码检查的基本信息，例如描述信息、更换绑定的凭据等。服务的名称不支持更新。

**操作步骤**
	
1. 登录 DevOps 平台，进入管理视图后，单击 **项目**。
	
2. 在项目页面，单击要更新绑定仓库服务基本信息的 ***项目名称***。
	
3. 在项目页面，单击 **DevOps 工具链**。
	
4. 单击要更新的 **代码仓库**、**制品仓库** 或 **代码质量分析** 的服务。
	
4. 在服务的详情页面，单击 ![operations](/img/operations.png)，再单击 **更新**，进入更新窗口。
	
	* 在 **描述** 框中，输入绑定服务的描述信息。
	
	* 在 **认证方式** 区域，选择工具使用的认证方式，详情请参考 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})。
	  
	* 在 **凭据** 区域，选择一个已创建的凭据。如果凭据列表中没有要使用的凭据，单击 **添加凭据**，创建新的凭据，参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md" >}})。   
	   
	 **注意**：当修改绑定镜像仓库的凭据时，该自动生成的凭据将自动更新，若删除此凭据，系统会自动再次生成。
	
6. 单击 **更新**，完成仓库服务信息的变更。
	