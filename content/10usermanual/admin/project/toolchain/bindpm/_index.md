+++
title = "绑定项目管理服务（Alpha）"
description = "绑定项目管理服务，实现项目的敏捷开发"
weight = 4
+++

绑定项目管理服务，实现项目的敏捷开发。

**前提条件**

绑定项目管理服务前，需要提前集成项目管理服务并创建凭据。

集成 **项目管理** 服务请参考 [集成工具]({{< relref "10usermanual/admin/service/addintegration.md" >}})，创建凭据请参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md#2" >}})。


**操作步骤**
	
1. 登录 DevOps 平台，进入管理视图后，单击 **项目**。
	
2. 在项目页面，单击要绑定项目管理服务的 ***项目名称***。
	
3. 单击 **DevOps 工具链** > **绑定**。
	
4. 在 **绑定** 窗口，选择一个已集成的项目管理工具，例如：**Jira**，单击进入绑定项目管理服务页面。 
	
	1. 在 **绑定账号** 步骤：
	
		* 在 **名称** 框中，输入绑定代码仓库服务的名称，名称默认为 ***工具名称***-***项目名称***。  
		名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
		
		* (非必填) 在 **描述** 框中，输入绑定代码仓库服务的描述信息。
		 
		* 在 **认证方式** 区域，目前只支持用户名/密码。
		
		* 在 **凭据** 区域，选择一个已创建的凭据。如果凭据列表中没有要使用的凭据，单击 **添加凭据**，创建新的凭据，参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md#2" >}})。
	
		单击 **绑定账号**。  
		单击绑定账号后，会判断选择的凭据是否有权限去访问 Jira 服务，如果没有权限，会提示错误。
	
	2. 在 **分配项目** 步骤：

		在 **Jira 项目** 下拉菜单中选择一个或多个项目。

		**提示**：
	
		* 若工具集成时填写了访问地址，平台支持跳转到 Jira 平台新建 Jira 项目，Jira 项目新建成功后，点击选择框选择 Jira 项目。

		* 若工具集成时未填写访问地址，则平台不支持跳转到 Jira 平台新建 Jira 项目。
	
7. 单击 **完成绑定**。  
	绑定后，需要等待一段时间，绑定的 Jira 项目会显示。


