+++
title = "更新数据信息"
description = "更新已创建凭据的数据信息。更新数据信息会影响已绑定工具的访问。"
weight = 4
+++

更新已创建凭据的数据信息。更新数据信息会影响已绑定工具的访问。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，在左侧导航栏单击 **凭据**。

2. 在凭据页面，可以通过以下三种方式更新凭据的数据信息：

    * 找到要更新的 ***凭据名称***，单击 ![operations](/img/operations.png)，再单击 **更新数据信息**。
     
    * 单击要更新的 ***凭据名称***，在凭据详情页面，单击 ![operations](/img/operations.png)，再单击 **更新数据信息**。
    
    * 单击要更新的 ***凭据名称***，在凭据详情页面，在 **数据** 区域，单击 ![pen](/img/pen.png)。 

3. 在 **更新数据信息** 窗口，根据凭据的类型，更新数据信息：

   * **用户名/密码**：即 basic-auth，用于存储用户名和密码的认证信息，若被访问工具支持 **用户名/密码** 或者 **用户名/Token** 访问，则可以选择这种类型。
	   
	   * **用户名**：输入访问服务的用户名，支持中文用户名和英文用户名。
	
	   * **密码**：输入访问服务的密码，密码也可以是 Token 信息。
       
    - **OAuth2**：即 OAuth 2.0 协议的客户端授权模式，平台采用协议中的授权码方式（Authorization Code）来授权获取令牌（Access Token），此类型的 Secret 目前只支持在绑定 **代码仓库** 时使用。

  	 * **Client ID**：输入创建应用后获得的 Client ID。
  
  	 * **Client Secret**：输入应用注册时获得的 Client Secret。 
   
   	- **SSH**：ssh-auth 类型，适用于存储代码仓库的认证信息。
    
   		在 **SSH 私钥** 框中，输入通过 SSH 协议传输数据的私钥。
   	 
   - **镜像服务**：即 dockerconfigjson，用于存储私有镜像仓库（Docker Registry）的 JSON 鉴权串。

     * **镜像服务地址**：输入访问镜像的服务地址。

     * **用户名**：输入可以访问镜像服务地址的用户名。

     * **密码**：输入可以访问镜像服务地址的密码。

     * **邮箱地址**：输入可以访问镜像服务地址的邮箱。
	
	参考 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})。 

5. 单击 **更新**，等待平台校验通过后，完成更新操作。

