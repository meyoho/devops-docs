+++
title = "凭据"
description = "凭据。"
weight = 6
+++

使用凭据保存敏感信息，例如：密码、Token、SSH key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。

凭据支持以下几种类型：

* **用户名和密码**：即 basic-auth，用于存储用户名和密码的认证信息。

* **OAuth2**：即 OAuth 2.0 协议的客户端授权模式，平台采用协议中的授权码方式（Authorization Code）来授权获取令牌（Access Token）。

* **镜像服务**：即 dockerconfigjson，用于存储私有镜像仓库（Docker Registry）的 JSON 鉴权串。

* **SSH**：即 ssh-auth 类型，用于存储通过 SSH 协议传输数据的认证信息。

{{%children style="card" description="true" %}}