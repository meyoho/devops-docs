+++
title = "官方模版"
description = "官方模版是为您预设的流水线模版，其中涵盖了大部分的使用场景，开箱即用，也为您创建自定义的模版流水线提供参考。"
weight = 8
+++

官方模版是为您预设的流水线模版，其中涵盖了大部分的使用场景，开箱即用，也为您创建自定义的模版流水线提供参考。

您可以单击模版查看详情，了解具体的模版信息，也可以通过业务视图中的模版创建流水线，使用指定官方模板创建流水线，目前官方提供的模版如下：

* [Golang 构建]({{< relref "10usermanual/project/CD/build/createbuild/golangbuild.md" >}})

* [Golang 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/golangupdateapp.md" >}})

* [Maven 构建（Alpha）]({{< relref "10usermanual/project/CD/build/createbuild/mavenbuild.md" >}})

* [Maven 构建并分发到仓库（Alpha）]({{< relref "10usermanual/project/CD/build/createbuild/mavenupdateapp.md" >}})

* [JMeter 自动化测试]({{< relref "10usermanual/project/CD/build/createbuild/jmeter.md" >}})

* [镜像构建]({{< relref "10usermanual/project/CD/build/createbuild/imagebuild.md" >}})

* [Java 构建]({{< relref "10usermanual/project/CD/build/createbuild/Javabuild.md" >}})

* [Java 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Javaupdateapp.md" >}})

* [Nodejs 10 构建]({{< relref "10usermanual/project/CD/build/createbuild/Nodejs10build.md" >}})

* [Nodejs 10 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Nodejs10updateapp.md" >}})

* [Python 2.7 构建]({{< relref "10usermanual/project/CD/build/createbuild/Python2.7build.md" >}})
 
* [Python 2.7 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Python2.7updateapp.md" >}})

* [Python 3.6 构建]({{< relref "10usermanual/project/CD/build/createbuild/Python3.6build.md" >}})

* [Python 3.6 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Python3.6updateapp.md" >}})

* [同步镜像]({{< relref "10usermanual/project/CD/syncImage/sync/SyncImage.md" >}})

* [更新应用]({{< relref "10usermanual/project/CD/updateApplication/updateApp/updateapp.md" >}})



