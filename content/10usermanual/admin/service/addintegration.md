+++
title = "集成工具"
description = "集成 DevOps 工具，在项目中重复使用工具，而无需反复输入授权，例如集成代码仓库 GitHub、持续集成工具 Jenkins、制品仓库 Docker Registry 等。"
weight = 1
+++

集成 DevOps 工具，在项目中重复使用工具，而无需反复输入授权，例如集成代码仓库 GitHub、持续集成工具 Jenkins、制品仓库 Docker Registry，制品仓库管理工具 Nexus 等。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，单击 **DevOps 工具链** > **集成**。

4. 在 **集成** 窗口，单击不同分类，选择一个工具进行集成：  

	* 持续集成：例如 Jenkins。集成并绑定 Jenkins 服务，实现服务的持续集成和升级。
	
	* 代码仓库：例如 GitHub、Bitbucket、GitLab 公有和私有仓库、码云等。

	* 项目管理：例如：（Alpha）Jira。使用项目管理工具管理源项目流程，应用于事务的缺陷追踪、需求收集、流程审批，任务跟踪等工作领域，实现敏捷项目管理。

	* 制品仓库：例如 Docker Registry、Harbor Registry、Docker Hub Registry 和（Alpha）Nexus、（Alpha）JForg Artifactory 下的制品仓库。
		
	* 代码质量分析：例如代码质量管理平台 SonarQube。集成 SonarQube，用于管理源代码的质量，对代码质量做自动化分析和管理。

	* 测试管理：例如测试管理工具 (Alpha）TestLink，通过自动化的流水线，实现测试用例的管理，提高测试人员的工作效率。

5. 选择一个工具后，您可以在展开的页面中查看集成工具的推荐版本。集成工具请参考如下步骤：

	* 在 **名称** 框中，输入集成的工具名称。  
	名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
	
	* **（非必填）** 在 **访问地址** 框中，输入工具的访问地址。

	* 在 **API 地址** 框中，输入集成工具的服务地址。服务地址的格式是：HTTP/HTTPS 协议 + IP + 端口号或 HTTP/HTTPS 协议 + 域名。  
	公有代码仓库不支持修改默认的 API 地址。
		
		**注意**：在集成 **Nexus**、**Jforg Artifactory** 时，需要配置以下 **认证方式** 和 **凭据** 两个参数。
	
		* 在 **认证方式** 区域，目前默认且只能选择 **用户名/密码**。
	
		* 在 **凭据** 框中，选择一个已创建的凭据。如果凭据列表中没有要使用的凭据，单击 **添加凭据**，创建新的凭据，参考 [创建凭据]({{< relref "10usermanual/project/secret/createsecret.md#2" >}})。

6. 单击 **集成**。集成工具后，您可以在项目中绑定集成工具的服务，参考 [绑定]({{< relref "10usermanual/admin/service/bindings/_index.md" >}})。