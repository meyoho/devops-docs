+++
title = "查看集成工具详情"
description = "在集成工具的详情页面，查看工具名称、API 地址、集成工具类型、绑定账号信息等。"
weight = 2
+++

在集成工具的详情页面，查看工具名称、API 地址、集成工具类型、绑定账号信息等。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，单击 **DevOps 工具链**。

2. 找到查看详情的集成工具，单击 ***集成工具名称***。  
如果集成的工具较多，单击不同分类，进入具体的分类页面查找。

	**注意**：在 DevOps 工具链中，由于 **Nexus**、**JForg Artifactory** 为制品仓库的管理工具，在该工具下会存在多种制品仓库并对这些仓库进行管理，与其他单一仓库使用逻辑不同，所以它的展示形式位于下图中图二区域，文档也将从逻辑上将详情页分成 **1号 区域** 和 **2号 区域** 两部分来进行阐述，方便您的理解：	
	
	![createsecret](/img/service.png?classes=big)

3. 在上图中的 1 号区域，展示了平台已经集成的工具卡片，包括 **项目管理工具**、**代码仓库工具**、**持续集成工具**、**代码质量分析工具** 和 **制品仓库工具**，卡片上包含了工具的部分信息，方便您快速选择指定工具，单击工具图标后进入集成工具的详情页面：
	
	* 您可在工具详情页上方查看工具的基本信息，例如：工具名称、API 地址、访问地址、集成工具类型、集成时间等信息。

	* 在项目绑定区域，查看绑定的项目相关信息。例如：名称、项目名称、凭据等。单击 ***名称*** 后，进入到对应的项目绑定详情页面。

4. 在上图中 2 号区域，展示了平台已集成的 **制品仓库管理工具**，例如 **Nexus**、**JForg Artifactory**：

 * 单击 **详情**，进入工具的详情页面。

 * 在详情页面您可以查看工具的详情信息，例如：工具名称、API 地址、访问地址、集成工具类型、凭据等。

 * 在 **制品仓库** 区域，您可以查看已集成的制品仓库，并且您可通过以下两种方式将要使用的制品仓库集成到平台，将制品仓库集成到平台后，方可分配到项目中使用：

		* 单击 Nexus 右侧的 **新建仓库**，表单填写请参考 [在 Nexus 下新建制品仓库]({{< relref "10usermanual/admin/project/toolchain/bindregistry/createmaven.md" >}})。
	
		* 单击 Nexus 右侧的下拉箭头再单击 **集成已有仓库**，表单填写请参考 [在  Nexus 下集成已有仓库]({{< relref "10usermanual/admin/project/toolchain/bindregistry/addmaven.md" >}})。

		* 单击 JForg Artifactory 右侧的 **新建仓库**，表单填写请参考 [在 JForg Artifactory 下新建制品仓库]({{< relref "10usermanual/admin/project/toolchain/bindregistry/jfrogcreatemaven.md" >}})。
	
		* 单击 JForg Artifactory 右侧的下拉箭头再单击 **集成已有仓库**，表单填写请参考 [在 JForg Artifactory 下集成已有仓库]({{< relref "10usermanual/admin/project/toolchain/bindregistry/jfrogaddmaven.md" >}})。
