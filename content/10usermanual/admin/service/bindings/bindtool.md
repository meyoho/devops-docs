+++
title = "绑定项目"
description = "在已集成的 DevOps 工具的详情页面，绑定项目。"
weight = 3
+++

在已集成的 DevOps 工具的详情页面，绑定项目。

**操作步骤**
	
1. 登录 DevOps 平台，进入管理视图后，单击 **DevOps 工具链**。
	
4. 单击一个要绑定项目的集成工具，在集成工具的详情页面的 **绑定账号** 区域，单击 **绑定**。
	
3. 在 **选择项目** 窗口，在项目列表中单击要绑定的 ***项目名称***，进入到项目页面的绑定工具页面，请参考以下任务说明。


	* 绑定持续集成服务：绑定 Jenkins，实现服务的持续集成和升级。参考 [绑定 Jenkins 服务]({{< relref "10usermanual/admin/project/toolchain/bindJenkinsservice.md" >}})。

	* 绑定项目管理服务：平台支持集成绑定项目管理平台（Alpha）Jira，管理源项目流程，应用于事务的缺陷追踪、需求收集、流程审批，任务跟踪等工作领域，实现敏捷管理。参考 [绑定项目管理服务]({{< relref "10usermanual/admin/project/toolchain/bindpm/_index.md" >}})。
	
	* 绑定代码仓库服务：例如 GitHub、Bitbucket、GitLab 公有和私有仓库、码云等。参考 [绑定代码仓库服务]({{< relref "10usermanual/admin/project/toolchain/bindcoderepo/_index.md" >}})。
	
	* 绑定制品仓库服务：绑定 Docker Registry、Maven 等制品仓库。参考 [绑定制品仓库服务]({{< relref "10usermanual/admin/project/toolchain/bindregistry/_index.md" >}})。
		
	* 绑定代码质量分析服务：绑定代码质量管理平台 SonarQube，管理源代码的质量，执行流水线，进行静态代码扫描，对代码质量做自动化分析和管理。参考 [绑定代码质量服务]({{< relref "10usermanual/admin/project/toolchain/bindsonar.md" >}})。

	* 绑定测试管理服务：例如测试管理工具 (Alpha）TestLink，通过自动化的流水线，实现测试用例的管理，提高测试人员的工作效率，参考 [绑定测试管理服务]({{< relref "10usermanual/admin/project/toolchain/bindtest.md" >}})。
		


