+++
title = "DevOps 工具链"
description = "在平台上集成 DevOps 工具可以省去反复输入的工作量，提高操作效率，方便统一管理。"
weight = 2
alwaysopen = false
+++

DevOps 强调产品管理、自动化软件交付和基础设施变更的过程，旨在建立一套快速、频繁、稳定地进行构建、测试、发布软件的文化与环境，与业务目标紧密结合，填补开发端和运维端之间的信息鸿沟，提倡自动化和监控，改善团队之间的协作关系。DevOps 可以缩短开发周期，增加部署频率，实现更可靠的发布。

在平台上集成 DevOps 工具可以省去反复输入的工作量，提高操作效率，方便统一管理。

DevOps 工具链支持集成以下工具类型：

* 持续集成：例如 Jenkins。Jenkins 是一个开源软件项目，是基于 Java 开发的一种持续集成工具，用于监控及执行持续重复的工作。在 DevOps 上集成并绑定 Jenkins 服务，实现服务的持续集成和升级。

* 代码仓库：例如 GitHub、Bitbucket、GitLab 公有和私有仓库、码云等。代码仓库分公有和私有代码仓库，用来储存、拉取代码等。

* 项目管理：例如：(Alpha）Jira。使用项目管理工具管理源项目流程，应用于事务的缺陷追踪、需求收集、流程审批，任务跟踪等工作领域，实现敏捷项目管理。

* 制品仓库：例如 Harbor Registry、Docker Registry、Maven。Docker Registry 用来管理镜像，例如镜像的存储、集成等。Harbor Registry 在 Docker Registry 的基础上，添加了一些必需的功能特性，例如安全、标识和管理等；同时平台集成了制品仓库管理工具 （Alpha）Nexus、（Alpha）JForg Artifactory，简化了内部仓库的维护和外部仓库的访问。

* 代码质量分析：例如代码质量管理平台 SonarQube，管理源代码的质量，对代码质量做自动化分析和管理。

* 测试管理：例如测试管理工具 (Alpha）TestLink，通过自动化的流水线，实现测试用例的自动管理，提高测试人员的工作效率。

{{%children style="card" description="true" %}}