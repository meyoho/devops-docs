+++
title = "管理视图"
description = ""
weight = 5
alwaysopen = false
+++

管理视图主要面向平台管理员，专注于平台级资源的创建。管理员可通过集成 DevOps 工具链，用于在各个项目下使用凭据认证的方式绑定 DevOps 工具链，从而为企业实现 CI/CD 提供资源保障。

**注意**：由于左侧导航栏中的 **全局自定义模版** 属于 Alpha 功能，您暂时不可见，如需打开，请在阅读本产品的 [功能成熟度]({{< relref "10usermanual/_index.md#1" >}}) 后，联系工作人员开启。

![manageview](/img/manageview.png?classes=big)

管理视图的功能其中包括：

* 支持集成绑定多种 DevOps 工具链，满足不同项目对工具的需求。

* 支持在项目下绑定工具链资源，合理进行 DevOps 工具链的资源分配。

* 支持绑定自定义流水线模版，补足官方模版未覆盖到的场景。

* 支持添加凭据，保存敏感信息，方便灵活对接工具链资源。

{{%children style="card" description="true" %}}