+++
title = "使用 OIDC 认证账号登录"
description = "使用 OIDC 认证账号登录平台。"
weight = 3
+++

平台支持 OIDC（OpenId Connect）协议，您可以使用本平台认可的第三方平台的账号登录平台。


**前提条件**

您已拥有本平台认可的第三方平台的账号。

**提示**：平台管理员可通过 [添加 OIDC]({{< relref "10usermanual/platformmanagement/roleanduser/idp/2oidc/1addoidc.md" >}})，支持第三方平台账号登录本平台。

**操作步骤**

1. 通过浏览器访问平台。 

2. 在登录界面单击 ***OIDC 登录方式*** 按钮（按钮名称根据 IDP 配置而定），将跳转第三方平台的登录认证页面。

	​**说明**： ***OIDC 登录方式***  按钮的名称为 IDP 配置中，OIDC 的显示名称，支持更新，具体操作请参考 [更新 OIDC]({{< relref "10usermanual/platformmanagement/roleanduser/idp/2oidc/3updateoidc.md" >}})。
3. 在第三方平台的登录认证页面，输入正确的用户名和密码。

4. 单击 **登录** 按钮。 <br>
	如果用户名、密码无误，将会跳转至平台首页（账号权限可见的默认页面）；如果用户名或密码错误导致无法登录，重新输入正确的用户名和密码登录即可。
	
	