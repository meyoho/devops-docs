+++
title = "登录管理"
description = ""
weight = 2
+++

本平台支持 dex 身份认证服务，可通过 dex 已实现的连接器（Connectors）认证的账号登录本平台。

dex 实现的连接器包括：LDAP、GitHub、SAML 2.0、GitLab、OpenID Connect（OIDC）、LinkedIn、Microsoft、AuthProxy、Bitbucket Cloud 等，更多关于 dex 的信息，请参考 [dex 官方文档](https://github.com/dexidp/dex)。

默认支持使用 LDAP、OIDC 认证账号以及本地用户账号登录本平台。当登录方式超过 2 种时，显示 **其他登录方式** 并以缩略按钮的形式展示。

**说明**：成功登录平台后，会默认进入业务视图。

- [使用 LDAP 认证账号登录]({{< relref "10usermanual/useraccount/1ldap.md" >}})

- [使用 OIDC 认证账号登录]({{< relref "10usermanual/useraccount/2oidc.md" >}})

- [本地用户登录]({{< relref "10usermanual/useraccount/3admin.md" >}})

	**提示**：如需使用其他连接器认证账号登录本平台，需要修改 dex 配置文件，请联系我们的运维工程师获得支持。


<img src="/img/00logoin1.png" width = "860" />


切换账号或退出平台时，请参考：


- [退出登录]({{< relref "10usermanual/useraccount/4logout.md" >}})