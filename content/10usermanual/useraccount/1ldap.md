+++
title = "使用 LDAP 认证账号登录"
description = "使用 LDAP 认证账号登录平台。"
weight = 2
+++

平台支持通过同步 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）导入客户企业已有用户体系，用户可通过 LDAP 认证账号登录平台。


**前提条件**

平台已对接客户企业的 LDAP 账号体系。

**提示**：平台管理员可通过 [添加 LDAP]({{< relref "10usermanual/platformmanagement/roleanduser/idp/1ldap/1addldap.md" >}})，对接企业的 LDAP 账号体系。

**操作步骤**

1. 通过浏览器访问平台。 

2. 在登录界面单击 ***LDAP 登录方式*** 的按钮（按钮名称根据 IDP 配置而定），如需切换登录方式，单击登录对话框底部的 **返回** 链接，即可回到初始的登录页面。

	​**说明**： ***LDAP 登录方式***  按钮的名称为 IDP 配置中，LDAP 的显示名称，支持更新，具体操作请参考 [更新 LDAP]({{< relref "10usermanual/platformmanagement/roleanduser/idp/1ldap/3updateldap.md" >}})。

3. 输入正确的 LDAP 认证用户名和密码。

4. 单击 **登录** 按钮。<br>
  如果用户名、密码无误，将会进入平台界面；如果用户名或密码错误，将会弹出提示“无效的用户名或密码”，重新输入正确的用户名和密码登录即可。

  