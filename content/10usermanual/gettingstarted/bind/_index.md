+++
title = "绑定操作"
description = "进行工具的绑定"
weight = 3
alwaysopen = false
+++

当您完成工具及项目初始化之后就可以在具体的项目中进行工具的绑定，前提是您已经在上一步骤中进行了工具的集成。

{{%children style="card" description="true" %}}