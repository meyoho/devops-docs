+++
title = "业务初始化"
description = "进行流水线的相关操作"
weight = 4
alwaysopen = false
+++

当您完成工具的绑定后可以切换到业务视图中，针对具体项目您可以进行流水线的相关操作，通过多种方式满足您对业务的需求。

{{%children style="card" description="true" %}}