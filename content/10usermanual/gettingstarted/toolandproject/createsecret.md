+++
title = "创建凭据"
description = "创建凭据，用于保存敏感信息，例如：密码、token、 ssh key 等。"
weight = 2
+++

创建凭据，用于保存敏感信息，例如：密码、token、ssh key 等。本流程需要创建 3 个凭据，分别用于使用 Jenkins、GitLab 和 Harbor。

### 创建 Jenkins 凭据

创建用户名和密码类型的 Jenkins 凭据，用于访问绑定的 Jenkins 服务。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，单击 **凭据**。

2. 在凭据页面，单击 **创建凭据**。

3. 在创建凭据页面，创建 Jenkins 凭据：

   	1. 在创建凭据页面的 **基本信息** 区域，填写以下信息：

	   - 在 **凭据名称** 框中，输入凭据的名称。  
	     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
	
	   - (非必填) 在 **显示名称** 框中，输入凭据的显示名称，支持中文字符。如未输入，默认显示为空。
	
	   - 在 **使用域** 区域，根据凭据的具体使用情况，选择类型：
	 
	  	 - **平台公共**：任何项目都可以使用此凭据绑定 DevOps 工具。项目中的成员对凭据的内容没有查看权限，只有使用权限。平台管理员可以创建和更新凭据内容。
	  
	  	 - **项目私有**：选择后，您需要在下面的 **所属项目** 框中选择项目才可以使用此域，平台管理员和项目成员都有凭据的查看和更新权限。

	2. 在创建凭据页面的 **数据** 区域，选择 Jenkins 支持的凭据类型中的 **用户名密码**，更多工具类型可参考 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})：
	
	   * 用户名和密码：Jenkins 支持用户名和 access token 访问，可以选择这种类型。
	   
	     * **用户名**：输入访问服务的用户名，支持中文用户名和英文用户名。
	
	     * **密码**：输入在 Jenkins 生成的 token。

5. ​单击 **创建**。

### 创建 GitLab 凭据

创建 OAuth2 类型的 GitLab 代码仓库凭据，使用 GitLab 代码仓库，保存配置 Jenkins 模板的代码等。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，单击 **凭据**。

2. 在凭据页面，单击 **创建凭据**。

3. 在创建凭据页面，创建 GitLab 代码仓库凭据：

   	1. 在创建凭据页面的 **基本信息** 区域，填写以下信息：

	   - 在 **凭据名称** 框中，输入凭据的名称。  
	     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
	
	   - (非必填) 在 **显示名称** 框中，输入凭据的显示名称，支持中文字符。如未输入，默认显示为空。
	
	   - 在 **使用域** 区域，根据凭据的具体使用情况，选择类型：
	 
	  	 - **平台公共**：任何项目都可以使用此凭据绑定 DevOps 工具。项目中的成员对凭据的内容没有查看权限，只有使用权限。平台管理员可以创建和更新凭据内容。
	  
	  	 - **项目私有**：选择后，您需要在下面的 **所属项目** 框中选择项目才可以使用此域，平台管理员和项目成员都有凭据的查看和更新权限。

	2. 在创建凭据页面的 **数据** 区域，选择 GitLab 支持的凭据类型中的 **OAuth2**，更多工具类型可参考 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})：
	   	  
	    - OAuth2：用 OAuth 2.0 协议的客户端模式（Client Credentials）来获取 access token。此类型的 Secret 目前只支持在绑定代码仓库时使用。
	
	  	 * **Client ID**：输入创建应用后获得的 Client ID。
	  
	  	 * **Client Secret**：输入应用注册时获得的 Client Secret。 

4. ​单击 **创建**。

### 创建 Harbor 凭据

创建镜像服务类型的 Harbor 凭据，使用 Harbor Registry 镜像，为镜像增加了安全、标识和管理等。

**操作步骤**

1. 登录 DevOps 平台，进入管理视图后，单击 **凭据**。

2. 在凭据页面，单击 **创建凭据**。

3. 在创建凭据页面，创建 Harbor 凭据：

   	1. 在创建凭据页面的 **基本信息** 区域，填写以下信息：

	   - 在 **凭据名称** 框中，输入凭据的名称。  
	     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。
	
	   - (非必填) 在 **显示名称** 框中，输入凭据的显示名称，支持中文字符。如未输入，默认显示为空。
	
	   - 在 **使用域** 区域，根据凭据的具体使用情况，选择类型：
	 
	  	 - **平台公共**：任何项目都可以使用此凭据绑定 DevOps 工具。项目中的成员对凭据的内容没有查看权限，只有使用权限。平台管理员可以创建和更新凭据内容。
	  
	  	 - **项目私有**：选择后，您需要在下面的 **所属项目** 框中选择项目才可以使用此域，平台管理员和项目成员都有凭据的查看和更新权限。

	2. 在创建凭据页面的 **数据** 区域，选择 Harbor 支持的凭据类型中的 **OAuth2**，更多工具类型可参考 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})：
	
	   * 用户名和密码：如果被访问工具支持用户名和密码或者用户名和 access token 访问，则可以选择这种类型。Harbor 支持用户名和密码访问，可以选择这种类型。
	   
	     * **用户名**：输入访问服务的用户名，支持中文用户名和英文用户名。
	
	     * **密码**：输入制品仓库 Harbor 的密码。
	   
5. ​单击 **创建**。
