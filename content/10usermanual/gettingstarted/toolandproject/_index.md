+++
title = "工具及项目初始化"
description = "进行项目和工具的初始化操作"
weight = 2
alwaysopen = false
+++

当您登录平台后，您可以通过 **项目初始化** 来满足企业的多租户场景，同时您可以通过平台 **工具初始化** 来满足企业对 DevOps 工具链的需求：

* 本流程的工具初始化是在平台已经部署好了相关 DevOps 工具链的基础上，创建 DevOps 工具的凭据和集成相关 DevOps 工具。

* 本流程的项目初始化是在平台上进行项目的创建和命名空间的创建。

{{%children style="card" description="true" %}}