+++
title = "快速开始"
description = ""
weight = 1
alwaysopen = false
+++

DevOps 快速开始将 DevOps 的基本功能，以样例的形式，形成一个使用流程，帮助您快速了解和实践 DevOps 的功能，您可以结合具体的项目需求，实现在开发环境或生产环境的自动化部署和交付应用。




{{%children style="card" description="true" %}}