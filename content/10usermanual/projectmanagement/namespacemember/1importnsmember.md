+++
title = "导入命名空间成员"
description = "为命名空间导入成员并为成员分配角色。"
weight = 10

+++

支持批量为命名空间导入成员，并为成员分配命名空间管理员或开发人员角色。

**说明**：支持导入状态为有效的任意用户，包括：

* 已成功登录平台的 OIDC 用户

* 通过 LDAP 同步至平台的用户

* 通过本地配置添加的用户

* 添加项目成员或命名空间成员时添加的用户

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **项目管理** ，打开项目管理页面。

1. 单击待导入成员命名空间所在的 ***项目名称***，进入项目详情页面。

2. 在左侧导航栏中单击 **命名空间** > **命名空间** 选项，进入命名空间列表页面。
	

3. 单击待导入成员 ***命名空间的名称***，进入命名空间详情页面。

4. 单击 **命名空间成员** 页签，打开命名空间成员列表页面。
	
5. 单击 **导入成员** 按钮，弹出 **导入成员** 对话框。


6. 单击用户名前的选择框，选择一个或多个用户。

	**提示**：可通过对话框右上方的用户组下拉选择框、用户名搜索框对用户名进行过滤。
	
6. 单击对话框底部 **设置角色** 项右侧的下拉选择框，选择要分配的角色名称。

6. 单击 **导入** 按钮，页面弹出提示 “导入成功”。

