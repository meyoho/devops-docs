+++
title = "命名空间联邦化"
description = "将联邦成员下的普通命名空间联邦化，升级为联邦命名空间。"
weight = 35

+++

支持通过将联邦成员集群下的普通命名空间联邦化，升级为联邦命名空间。

**说明**：

* 仅支持将属于联邦集群成员的集群下的普通命名空间升级为联邦命名空间。

* 将以当前命名空间及其资源配置为模版，在联邦集群上创建联邦命名空间。创建过程中，如联邦内其他成员集群包含同名命名空间，将被覆盖。命名空间联邦化后，可在联邦命名空间详情页，进行配额，限额的差异化配置。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **项目管理** ，打开项目管理页面。

1. 单击待更新资源配额命名空间所在的 ***项目名称***，进入项目详情页面。

2. 在左侧导航栏中单击 **命名空间** > **命名空间** 选项，进入命名空间列表页面。

3. 单击待更新 ***命名空间的名称***，进入命名空间详情页面。

4. 单击命名空间基本信息标签栏右侧的 **操作** 下拉按钮，并选择 **命名空间联邦化**。


5. 在弹出的 **命名空间联邦化** 对话框中，可查看联邦命名空间的信息。

	

6. 单击 **确定** 按钮，联邦化成功后将进入联邦命名空间详情页面。

