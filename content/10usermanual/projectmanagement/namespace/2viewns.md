+++
title = "查看命名空间详情"
description = "查看指定的命名空间的详情，例如：名称、显示名称、所属集群、命名空间管理员、资源配额、容器限额等信息。"
weight = 20

+++

查看指定的命名空间的详情，例如：名称、显示名称、所属集群、命名空间管理员、资源配额、容器限额等信息。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **项目管理** ，打开项目管理页面。

1. 单击待查看命名空间所在的 ***项目名称***，进入项目详情页面。

2. 在左侧导航栏中单击 **命名空间** > **命名空间** 选项，进入命名空间列表页面。

3. 单击待查看 ***命名空间的名称***，进入命名空间详情页面。

4. 查看命名空间的基本信息、资源配额、容器限额。

	其中，资源配额的 **分配占比**，通过占比条显示了资源的已分配和总额信息。<br>已分配颜色含义：<br>已分配 >= 90% 时，显示为红色；<br>70% =< 已分配 < 90% 时，显示为黄色；<br>已分配 < 70% 时，显示为绿色。如下图所示。
	
	<img src="/img/00nsdetail.png" width = "600" />
	
	

