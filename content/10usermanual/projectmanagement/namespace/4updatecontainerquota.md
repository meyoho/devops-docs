+++
title = "更新容器限额"
description = "更新已创建命名空间的容器限额。"
weight = 40

+++

更新已创建命名空间的容器限额。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **项目管理** ，打开项目管理页面。

1. 单击待更新容器限额命名空间所在的 ***项目名称***，进入项目详情页面。

2. 在左侧导航栏中单击 **命名空间** > **命名空间** 选项，进入命名空间列表页面。

3. 单击待更新容器限额 ***命名空间的名称***，进入命名空间详情页面。

4. 单击命名空间基本信息标签栏右侧的 **操作** 下拉按钮，并选择 **更新容器限额**。

5. 在弹出的 **更新容器配额** 对话框中，修改 CPU 和内存的 **限制默认值**、**请求默认值**、**最大值** 配置值。

6. 确认配置值无误后，单击 **更新** 按钮，页面弹出提示“更新容器配额成功”。