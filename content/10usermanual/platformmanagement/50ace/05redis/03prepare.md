+++
title = "部署前准备"
description = ""
weight = 3
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style> 

## 部署规划

**部署架构**

![redis](/img/ace/redis/redis01.png?classes=big)

Redis平台整体架构如下：

* **控制台**：该模块为前台管理界面，实际的图形化操作界面，可以对资源、实例进行日常的管理和维护操作。比如实例的生产、监控告警配置、资源的上架等。

* **管控 Control_Center（CC）**：该模块为后台服务，主要负责实例的生命周期管理、资源管理、HA 等。

* **Cache_agent（ccagent）**：该模块为后台服务的 Agent 节点，部署在实际的 IaaS 资源上。

* **监控系统**：该模块负责监控 Redis、Proxy 及机器的存活状态，并上报相应的监控数据到对应的存储系统，目前使用的存储系统是 InfluxDB。

**环境要求**


| **设备类型** | **设备要求** | **最小配置** |
| :--- |:--- | :-------------------- |
| Proxy 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 |
| Cache 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 |
| 管控设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，64GB 磁盘 |
| LVS 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 |
| InfluxDB 设备 | 物理机/虚拟机 | 4 核 CPU，16GB 内存，128GB 磁盘 |
| MySQL 设备 | 物理机/虚拟机 | 4 核 CPU，8GB 内存，256GB 磁盘，版本要求 5.6+，使用 MySQL 高可用版或 TDSQL 集中式版 |		
	
**部署方案**

支持最小部署、高可用部署两种模式。

* **最小部署**：该模式下管控平台、LVS、InfluxDB 将部署在同一台机器上。

	![redis](/img/ace/redis/redis02.png?classes=big)

* **高可用部署**：该模式下管控平台、LVS、InfluxDB 将分别单独部署于各自机器上。

	![redis](/img/ace/redis/redis03.png?classes=big)
	
支持部署标准版或集群版。

* **标准版**：Redis 标准版是最通用的 Redis 版本，兼容 Redis 4.0 版本的协议和命令，提供数据持久化和备份，适用于对数据可靠性、可用性都有要求的场景。支持 0 - 5 个副本，主节点提供日常服务访问，从节点提供 HA 高可用，当主节点发生故障，系统会自动切换至从节点，保证业务平稳运行。

* **集群版**：Redis 集群版是基于社区版 Redis 4.0 打造的全新版本，采用分布式架构，支持垂直和水平的扩缩容，拥有高度的灵活性、可用性和高达千万级 QPS 的高性能。支持水平方向 3 分片 - 128 分片的扩展，垂直方向 1 个 - 5 个副本集的扩展，扩容、缩容、迁移过程业务几乎无感知，做到最大的系统可用性。

不同部署方案的配置要求如下所述：

* 最小部署 + 标准版机器配置要求：管控机器（包含 LVS 、InfluxDB）1 台，Cache 机器 4 台，Proxy 机器 2 台。

* 最小部署 + 集群版机器配置要求：管控机器（包含 LVS 、InfluxDB）1 台，Cache 机器 7 台，Proxy 机器 2 台。

* 高可用部署 + 标准版机器配置要求：

	* 方案 1：管控机器（包含 LVS 、InfluxDB）2 台，Cache 机器 4 台，Proxy 机器 2 台；

	* 方案 2：管控机器 2 台， LVS 2 台，InfluxDB 1 台，Cache 机器 4 台，Proxy 机器 2 台；

	* 方案 3：管控机器（包含 LVS）2 台，InfluxDB 1 台，Cache 机器 4 台，Proxy 机器 2 台。

* 高可用部署 + 集群版机器配置要求:

	* 方案 1：管控机器（包含 LVS、InfluxDB）2 台，Cache 机器 7 台，Proxy 机器 2 台；

	* 方案 2：管控机器 2 台， LVS 2 台，InfluxDB 1 台，Cache 机器 7 台，Proxy 机器 2 台；

	* 方案 3：管控机器（包含 LVS）2 台，InfluxDB 1 台，Cache 机器 7 台，Proxy 机器 2 台；
	
	注意：如果 InfluxDB 不在管控机器上部署，选择独立部署的话，此时部署 CC 组件后，需将管控数据库中 sys_misc_config_t 表中 Influxdb_url 更改为实际部署 InfluxDB 服务的机器 IP。同时需要修改 CC 组件中的配置文件 MulServer.conf。
	
## 系统参数配置

**部署环境设置**

1. 扩缩容说明

	问题描述：扩缩容时管控机器会频繁的连 Cache 机器，由于每次连接都在很短的时间内结束，会导致很多的 TIME_WAIT，以至于会用光可用的端口号，所以新的连接没办法绑定端口，会报错 "Cannot assign requested address"(/data/log/cluster_cc/cc-debug.log)。
	使用命令 `netstat -a|grep TIME_WAIT`，即可发现很多 TIME_OUT 状态的连接。
	
	解决办法：用以下命令修改内核参数（需要 root 权限） --所有机器
	追加以下参数到 `/etc/sysctl.conf`。
	
	```
	net.ipv4.tcp_tw_reuse=1
	net.ipv4.tcp_tw_recycle = 1
	net.ipv4.tcp_timestamps = 1
	net.ipv4.tcp_fin_timeout = 30
	```
	
	最后生效以上设置：
	
	```
	sysctl -p
	```

2. 系统参数优化 --所有 Cache 机器

	追加以下参数到 `/etc/sysctl.conf`。
	
	```
	net.core.somaxconn = 511
	vm.swappiness = 1
	```
	
	最后生效以上设置。
	
	```
	sysctl -p
	```

3. 执行以下命令关闭 Transparent Huge Pages(THP)，注意不同 linux 版本路径稍有不同 --所有 Cache/Proxy 机器

	```
	echo never > /sys/kernel/mm/transparent_hugepage/enabled
	```
 
	注意：如果需要重启生效，重启后该配置会失效，如果需要重启后该配置保持生效，需要追加命令到 `/etc/rc.local`。

4. 查看系统文件句柄数 ulimit -n，如果小于 65535，修改最大文件句柄数 --所有 Cache/Proxy 机器
	
	```
	ulimit -Sn 65535
	```
	
	注意：重启后该配置会失效，如果需要重启后该配置保持生效，需要追加如下配置到 `/etc/security/limits.conf`。
	
	```
	* soft nofile 65535
	
	* hard nofile 65535
	```

**MySQL 客户端配置**

检查是否已有默认配置 mysql 命令进入系统搜索路径，`MYSQL_CLIENT_PATH=mysql`。  

如果没有配置，可配置绝对路径，例如：`/usr/local/mysql/bin/mysql`，否则数据库初始化会失败。

**LVS 机器配置**

在 LVS 机器上，需要配置一个 Linux 的内核参数 net.ipv4.ip_forward 的值为 1，使 Linux 系统具有路由转发功能。

**VIP 配置（仅高可用模式）**

在高可用模式下，需要准备 LVS 的 VIP。目前，Redis 使用 nat 的方式进行 VIP 的转发，还需进行额外的路由配置。
在管控机器和 Proxy 机器上，将以下命令永久生效，写到 /etc/rc.local 中，或者使用其它永久设置的方式。

```
route add default gw {{vip}}
```

## 数据目录准备

1. 分别登录管控机器、Cache 机器、LVS 机器、InfluxDB 机器。

2. 每台机器数据都保存在 data 目录，如果实际安装机器有单独的数据盘做存储，需要挂载数据盘到 /data 目录。
	
	1. `fdisk -l`，会显示类似 /dev/vdb 这样的名称。
	
	2. `mkfs.xfs disk_name`，disk_name 名称就是上一步的结果。
	
	3. 加到开机启动,追加到 /etc/fstab，如：
	
		/dev/vdb   /data    xfs  defaults   1 1
	
	4. 使得挂载生效 `mount -a`


