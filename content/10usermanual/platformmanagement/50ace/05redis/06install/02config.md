+++
title = "配置与部署"
description = ""
weight = 2
alwaysopen = false
+++

在完成部署规划后，配置部署参数并执行部署。

1. 在 **配置** 区域，配置以下参数。

	* **管控数据库地址**：对接作为管控数据库的 MySQL 数据库地址。

	* **管控数据库端口**：管控数据库的端口，例如 3306。

	* **管控数据库账号名**：登录管控数据库的用户名。

	* **管控数据库密码**：登录管控数据库的密码。

	* **LVS_VIP**：LVS 的负载地址。

	* **LVS 网卡名称**：LVS 主机的通信网卡设备名，默认为 eth0。

2. 在 **部署** 区域，单击 **开始部署**，等待部署结果。  
	如提示部署失败，可查看日志、文档，修改未完成部署组件的参数。再次单击 **开始部署**，从头开始检测部署。

	


 