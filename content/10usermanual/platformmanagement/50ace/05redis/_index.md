+++
title = "Redis"
description = ""
weight = 5
alwaysopen = false
+++

云数据库 Redis（TencentDB for Redis）是一款高可用、高可靠的 Redis 服务平台。丰富的数据结构能帮助您完成不同类型的业务场景开发。在使用上，兼容 Redis 协议，支持字符串、链表、集合、有序集合、哈希表等多种数据类型。支持主从热备，提供自动容灾切换、数据备份、故障迁移、实例监控、在线扩容、数据回档等全套的数据库服务。


