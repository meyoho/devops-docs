+++
title = "部署说明（YAML 模式）"
description = ""
weight = 5
alwaysopen = false
+++

本节说明如何使用产品管理，以 YAML 模式部署 TI Matrix。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品管理**。

3. 在 **全部产品** 列表，找到 **TI Matrix** 产品，单击对应的 ![](/img/ace/003point.png) > **部署**。

4. 选择 **YAML 部署** 模式。

5. 在 YAML（读写） 区域，输入部署的 YAML 编排文件内容，同时支持以下操作：

	* 单击 **导入**，导入已有的 YAML 文件内容作为编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。

	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

	YAML 示例如下：
	
	```
    
    apiVersion: product.alauda.io/v1alpha1
    kind: Tmprovision
    metadata:
      name: tmprovision
    spec:
      chart:
        chartName: stable/ti-matrix
        name: ti-matrix
        namespace: alauda-system
        releaseName: ti-matrix
        releaseNamespace: alauda-system
      cluster: region
      clusterManagerIp: 10.0.129.65
      dependency:
        consul:
          ip:
          - 10.0.128.225
          - 10.0.129.65
        database:
          tiInstall:
            ip:
            - 10.0.129.65
            - 10.0.128.225
        es:
          external:
          - host: 10.0.128.189
            password: rbqhbwaNdR
            port: 9200
            user: alaudaes
          mode: 1
        hpApiServer:
          ip:
          - 10.0.128.225
        kafka:
          external:
          - kafkaAddr: 10.0.128.189:9092
            kafkaBootStrap: 10.0.128.189:9092
          mode: 1
        monitor:
          master: 10.0.129.65
          slave:
          - 10.0.128.225
        redis:
          tiInstall:
            master:
            - 10.0.129.65
            slave:
            - 10.0.128.225
        time:
          client:
          - 10.0.128.225
          server:
          - 10.0.129.65
        zookeeper:
          external:
          - host: 10.0.128.189
          mode: 1
      exporter: {}
      k8sMaster: 10.0.129.65
      labelConfig:
        file:
          server: ""
        gpuNode:
        - ""
        sip:
          server: ""
          streaming: ""
      policy:
        retry:
          enable: true
          max: 3
      registry: 10.0.128.72:60080
      sshConfig:
        global:
          password: zlcroot
          port: 22
          user: root
        others:
          10.0.128.225: {}
          10.0.129.65: {}
      storage:
        local:
          storePath: ""
      version: v2.2.1
  ```

6. 单击 **部署**。

	部署完成后，在业务集群 Master 节点执行以下命令，部署算法认证 插件。
	
	`
	helm install . --name hostdev --namespace alauda-system  --set global.registry.address=harbor-b.alauda.cn --set global.namespace=alauda-system
	`