+++
title = "部署前准备"
description = ""
weight = 3
alwaysopen = false
+++

**资源规划**

支持 HA 高可用模式部署，不支持单点部署。需要的服务器资源参考下表。

| **角色** | **服务器配置** | **数量** |
| :--- |:--- | :-------------------- |
| 集群管理节点 | 16 核 CPU，32G 内存，300G+ 硬盘 | 1 | 
| 集群运算节点 | 28 核 CPU，128G 内存，300G+ 硬盘| 2 |

**准备步骤**

1. 新建项目

	打开平台项目管理，创建名称叫 **timatrix** 的项目，并选择所属集群。  
	**注意**：项目名称必须是 timatrix。
	
	![tiproject](/img/ace/ti/tiproject.png?classes=big)
	
	TI Matrix 将会部署到 timatrix 项目所属的集群，需根据集群大小合理配置集群的超售比，能确保安装后，命名空间 timatrix-platform 下的 Pod 能正常启动。
	
	命名空间 timatrix-platform 的资源需求，参考以下数值：  
	CPU: 总请求值 26.630 核  
	CPU: 总限制值 91.630 核  
	内存: 总请求值 36.662 G  
	内存: 总限制值 134.407 G

2. 将 TI 镜像导入本地镜像仓库

	可以在平台的 init 节点操作。
	
	<style>ol ol { list-style-type: lower-roman;}</style> 

	1. 解压服务包部署脚本。

		`tar xvf service_release_<postfix>.tar`
	
	2. 使用以下命令。

		`cd service`
 
	3. 将镜像包导入到私有仓库。  
	命令格式：`bash load_newtag_images.sh <image_tag_prefix> images_release_<postfix>.tar`  
	其中 `<image_tag_prefix>` 为需要替换的新 tag。  
	以 tke 镜像仓库为例：  
	`bash load_newtag_images.sh tke3.global.registry.tke.com images_release_<postfix>.tar`

3. 监控目录配置

	在需要安装监控平台的机器上，确保以下前提条件已得到满足：
	
	InfluxDB 的数据文件在 /var/lib/influxdb 路径下，确保这个路径被软链到了数据盘。
	
	```
	# 确认数据盘挂载在 /var/lib/docker 目录下，新建以下目录
	mkdir /var/lib/docker/influxdb
	
	# 确保 /var/lib/influxdb 目录还不存在，或者没有软链
	ls -l /var/lib/influxdb
	
	# 创建软链
	ln -s /var/lib/docker/influxdb /var/lib/influxdb
	
	# 确认软链正确创建
	ls -l /var/lib/influxdb
	 
	# 查看挂载情况
	df -Th 
	```
	
4. 分布式文件挂载

	若没有分布式文件存储，则需要对部分平台服务打标签(配置fileserver) 
	
	```
	# 将共享存储先挂载在media目录
	 
	mount 33.118.16.16:/FileSystem001 /media/storage_cluster
	 
	# 将共享存储的volume目录挂载软链到/data/timatrix/storage/volume1
	 
	ln -s /media/storage_cluster/timatrix/storage/volume1 /data/timatrix/storage/volume1
	 
	# 修改fstab确保开机生效 vi /etc/fstab
	 
	33.118.16.16:/FileSystem001 /media/storage_cluster nfs defaults 0 0 
	```
	注意:

	* 请不要将共享存储直接挂载在 /data 目录下；
	
	* 最终需要确保每台机器都满足以下两点：
		
		1. /data/timatrix/storage/volume1 为分布式存储目录；
		2. /data/timatrix 为本机的数据盘。
	
	* 假设数据盘挂载在目录 /var/lib/docker 下，执行以下命令软链目录： 
	
		```
		# 在数据盘创建timatrix目录
		mkdir /var/lib/docker/timatrix
		 
		# 确认目录没有创建或创建软件，应返回不存在 
		ls -l /data/timatrix
		
		# 创建软链目录
		ln -s /var/lib/docker/timatrix /data/timatrix
		
		# 确认软链正确创建
		ls -l /data/timatrix
		```

