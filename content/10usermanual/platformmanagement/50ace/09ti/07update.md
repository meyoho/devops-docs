+++
title = "更新 TI Matrix 产品配置"
description = ""
weight = 7
alwaysopen = false
+++

可视化更新 TI Matrix 产品配置。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品管理**。

3. 在 **全部产品** 列表，单击 ***TI Matrix 产品名称***，在部署详情页中，单击 **操作** > **更新**。

4. 在更新部署页面，更新 TI Matrix 部署参数，参考 [部署说明]({{< relref "10usermanual/platformmanagement/50ace/09ti/06installui.md" >}}) 文档。

5. 配置完成后，单击 **更新**。