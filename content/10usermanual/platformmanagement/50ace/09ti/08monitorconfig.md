+++
title = "系统配置"
description = ""
weight = 8
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style>

部署完成后，支持进行系统配置。当状态为部署成功、运行中或运行异常时，可进行配置。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品管理**。

3. 在 **全部产品** 列表，单击 ***TI Matrix 产品名称***，在部署详情页中，单击 **操作** > **系统配置**。

4. 在 **系统配置** 窗口中，输入从 TI 平台上获取的 **Grafana APIKey**。获取 Grafana APIKey 可参考以下方法。

	1. 进入 TI Matrix 部署详情页，单击 **产品入口** 区域的链接。
	
	2. 登录 TI 平台。
	
	3. 单击左导航栏中的 **管理中心** > **运维管理**，在运维管理页面，单击 **TIG 监控** 卡片，进入 TIG 监控系统。
	
	4. 单击左导航栏中的 **Configuration** > **API Keys**。

		![tiproject](/img/ace/ti/config1.png?classes=big)
	
	5. 单击 **Add API Key**，添加一个 admin 角色（Role）的 API Key，在创建成功页面复制 Key 并保存。

		![tiproject](/img/ace/ti/config2.png?classes=big)
		
		![tiproject](/img/ace/ti/config3.png?classes=big)

5. 在 **TI Secret ID** 和 **TI Secret Key** 框中，配置 TI Matrix 的 API 密钥。获取 API 密钥可参考以下方法。

	1. 进入 TI Matrix 部署详情页，单击 **产品入口** 区域的链接。
	
	2. 使用 admin 账号登录 TI 平台。

	3. 进入 **应用中心** > **应用管理**，新建应用。
		
		![tiproject](/img/ace/ti/config4.png?classes=big)
	
	4. 单击刚新建的应用，可获得 SecretID 和 SecretKey。

		![tiproject](/img/ace/ti/config5.png?classes=big) 

6. 完成所有配置后，单击 **配置**。

	![tiproject](/img/ace/ti/config6.png?classes=big)
	
	系统配置成功后，可以在统一门户（portal）中查看到 TI Matrix 产品的信息。
	
	![tiproject](/img/ace/ti/config7.png?classes=big) 
	