+++
title = "配置参数"
description = ""
weight = 2
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style> 

在 **配置参数** 环节，单击页面中的 ***TSF 产品的访问地址***（http://{tsf-init 的 IP}），并参考以下说明，在 TSF 产品页面中配置参数。

1. 用平台管理员账号登录 TSF，管理员账号的初始用户名/密码为qcloudAdmin/qcloudAdmin。

2. 单击顶部导航栏右侧的管理员账号用户名，在下拉菜单中单击 **运营中心**，进入运营中心页面，开始配置基础可用区。

	![tsf](/img/ace/tsf/config1.png?classes=big)
	
	1. 单击左导航栏中的 **资源运营** > **可用区管理**，进入可用区管理页面。
	
		![tsf](/img/ace/tsf/config2.png?classes=big)
	
	2. 单击 **基础可用区** 页签，进入基础可用区页面。

		![tsf](/img/ace/tsf/config3.png?classes=big)
		
	3. 单击 **添加基础可用区** 按钮，在 **添加基础可用区** 窗口，输入基础可用区名称 **tsf**，描述非必填。

		![tsf](/img/ace/tsf/config4.png?classes=big)
		
		单击 **提交**。
		
3. 配置机器列表。

	1. 单击左导航栏中的 **资源运营** > **基础模块管理**，进入基础模块管理页面。

		![tsf](/img/ace/tsf/config5.png?classes=big)
		
	2. 单击 **机器列表** 页签，进入机器列表页面。

		![tsf](/img/ace/tsf/config6.png?classes=big)
		
	3. 单击 **添加机器** 按钮，在 **新增机器** 窗口，依次添加在部署前准备时，确定为业务节点的三个节点主机。其中基础可用区选择刚才创建的基础可用区。

		![tsf](/img/ace/tsf/config7.png?classes=big)

4. 配置参数列表  

	<table>
	  <tr>
	    <th>参数</th>  
	    <th>value</th>
	  </tr>
	  <tr>
	    <td>tsf.container.cp-cluster-name-prefix</td>  
	    <td>sol-tsf</td>
	  </tr>
	  <tr>
	    <td>tsf.container.master.address</td>  
	    <td>http://"adapter 部署地址":32767</td>
	  </tr>
	  <tr>
	    <td>tsf.container.master.pathPrefix</td>  
	    <td>/v1</td>
	  </tr>
	  <tr>
	    <td>tsf.container.master.authorization</td>  
	    <td>Bearer + "空格" + "TKE的token"</td>
	  </tr>
	  <tr>
	    <td>tsf.container.master.addtional-headers.username</td>  
	    <td>admin</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry.type</td>  
	    <td>harbor</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry.address</td>  
	    <td>http://"harbor 仓库地址":31104</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry.path</td>  
	    <td>/api</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry.authorization</td>  
	    <td>Basic YWRtaW46SGFyYm（harbor 鉴权）</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry-usage.address</td>  
	    <td>http://"harbor 仓库地址":11112</td>
	  </tr>
	  <tr>
	    <td>tsf.container.registry-usage.username</td>  
	    <td>admin</td>
	  </tr>
	  <tr>
	    <td>tsf.container.instance-import-mode</td>  
	    <td>M</td>
	  </tr>
	  <tr>
	    <td>tsf.container.master-num-limit</td>  
	    <td>2</td>
	  </tr>
	  <tr>
	    <td>tsf.container.node-num-limit-per-setup</td>  
	    <td>5</td>
	  </tr>
	  <tr>
	    <td>tsf.container.allow-empty-nodes</td>  
	    <td>true</td>
	  </tr>
	  <tr>
	    <td>barad.report.log.url</td>  
	    <td>http://"tsf-metrics 节点 IP":11112/met/saveMonitorData</td>
	  </tr>
	  <tr>
	    <td>barad.report.trace.url</td>  
	    <td>http://"tsf-metrics 节点 IP":11112/met/saveMonitorData</td>
	  </tr>
	  <tr>
	    <td>barad.report.event.url</td>  
	    <td>http://"tsf-metrics 节点 IP":11112/met/saveMonitorData</td>
	  </tr>	  
	</table>	  
	
5. 配置基础模块。

	1. 在基础模块管理页面，单击 **模块列表** 页签，进入模块列表页面。

	2. 单击操作栏中的 **新增节点**，为所有组件新增节点，新增时部分组件需要设置 **主节点**，参考下表。

	3. 单击操作栏中的 **模块配置**，为部分组件完成模块配置，需要配置的组件及配置参数参考下表。

	<table>
	  <tr>
	    <th rowspan="2">组件信息</th>  
	    <th rowspan="2">主节点设置</th>
	    <th colspan="3">模块配置</th>
	  </tr>
	  <tr>
	    <th>vip</th>
	    <th>vport</th>
	    <th>密码</th>
	  </tr>
	  <tr>
	    <td>oss-consul-server</td>
	    <td>无需设置主节点</td>
	    <td></td>
	    <td></td>
	    <td></td>    
	  </tr>
	  <tr>
	    <td>tsf-mysql</td>
	    <td>tsf-node1</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>31306</td>
	    <td>自行配置，请牢记密码</td>   
	  </tr>
	  <tr>
	    <td>tsf-ctsdb</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>9201</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-redis</td>
	    <td>tsf-node1</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>6379</td>
	    <td>自行配置，请牢记密码</td>   
	  </tr>
	  <tr>
	    <td>tsf-elasticsearch</td>
	    <td>tsf-node1</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>9200</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-resource</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>16000</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-consul-authen</td>
	    <td>tsf-node1</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-consul-register</td>
	    <td>tsf-node1</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-consul-config</td>
	    <td>tsf-node1</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-consul-access</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>8000</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-mesh-apiserver</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-mesh-pilot</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>15010</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-mesh-mixs</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>9300</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-repository-access</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>8100</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-repository-server</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-masterapi</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-master</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>8200</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-controler</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-trans-coordinator</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-alarm</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-apm</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-auth</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-dcfg</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-dispatch</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>15000</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-monitor</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-ms</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-ratelimit</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-route</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-scalable</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-template</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-tx</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-metrics</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-record</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-ratelimit-master</td>
	    <td>无需设置主节点</td>
	    <td>tsf-node1 的 IP</td> 
	    <td>7000</td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>tsf-gateway</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	  <tr>
	    <td>license-server</td>
	    <td>无需设置主节点</td>
	    <td></td> 
	    <td></td>
	    <td></td>   
	  </tr>
	</table>
	
	完成上述所有操作后，先不要关闭 TSF 产品页面。  
	在平台的 **配置参数** 环节页面，单击 **配置完成**。