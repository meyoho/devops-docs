+++
title = "检查与调试"
description = ""
weight = 4
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style>

在部署完成后，继续依次完成以下检查、调试操作。

**TSF 业务组件调试**

1. 切换到 TSF 产品页面并进入 **运营中心** > **资源运营** > **基础模块管理**。

2. 在模块列表页面，找到 oss-consul-server 组件，在操作栏中单击对应的 **部署** 按钮，在 **组件部署** 窗口中，单击 **确认**。

3. 刷新页面，直到 oss-consul-server 组件的状态变为 **运行中**。

4. 在模块列表页面，找到 tsf-mysql 组件,在操作栏中单击对应的 **模块配置** 按钮，在 **模块配置** 窗口中，重新输入数据库密码。  

	![tsf](/img/ace/tsf/check1.png?classes=big)
	
**TSF 运营组件数据库处理**

**注意**：以下操作在部署 TSF 产品的业务集群的控制节点上执行。

1. 检查所有组件的 Pod 运行状态，等待除 tsf-dispatch 以外的所有 Pod 都处于 Running。（由于 TSF 还未导入 License，所以 tsf-dispatch 始终异常)

2. 进入 tsf-init 组件的 Pod，操作命令示例如下。

	```
kubectl get pod -n tsf|grep init
tsf-init-d99fbd49c-dbj5t         1/1      Running    0     5m
kubectl exec -it -n tsf tsf-init-d99fbd49c-dbj5t bash	
	```	
	
3. 在 tsf-init 组件 Pod 中，登录数据库。

	```
# Mysql的密码改成自行设置的密码
mysql -uroot -pTcdn@2007	
	```	
4. 执行 SQL 语句

	```
START TRANSACTION;
use tsfmanager_operation;
update tsf_module set alarm_type="";
update tsf_module_instance set status='运行';
COMMIT;	
	```	
	
**TSF 证书申请与导入**

**注意**：以下操作在部署 TSF 产品中完成。

1. 切换到 TSF 产品页面，并进入 **运营中心** > **许可管理**，单击 **申请许可**。

	![tsf](/img/ace/tsf/check2.png?classes=big)
	
2. 单击 **点击申请** 按钮。

	![tsf](/img/ace/tsf/check3.png?classes=big)
	
3. 根据环境实际需求和用途填写证书申请信息，下图为示例。

	![tsf](/img/ace/tsf/check4.png?classes=big)
	
4. 下载《申请文件》，此文件作为附件发送邮件进行证书申请，请联系交付人员。

	![tsf](/img/ace/tsf/check5.png?classes=big)

5. 等待邮件申请成功，获得邮件回复后，下载回复邮件中的 License 附件，上传到 TSF 产品中。

	![tsf](/img/ace/tsf/check6.png?classes=big)
	
	![tsf](/img/ace/tsf/check7.png?classes=big)
	
**检查运行**

1. 检查所有组件的 Pod 运行状态，确认全部都处于 Running。在部署 TSF 产品的业务集群的控制节点上执行，操作命令示例如下。

	```
kubectl get pod -n tsf	
	```	
2. 检查 TSF 产品页面所有组件的运行状态。
切换到 TSF 产品页面并进入 **运营中心** > **资源运营** > **基础模块管理**，在模块列表页面中，认所有组件的状态全部处于 **运行中**。

3. 检查 TSF 部署状态。

	切换到平台的产品管理页面，在产品列表中，确认 **TSF** 的 **部署/运行状态** 为 **部署成功**；进入 TSF 产品详情页，确认 **TSF** 的 **部署/运行状态** 为 **部署成功**。



	

	
