+++
title = "部署业务组件"
description = ""
weight = 3
alwaysopen = false
+++

<style>ol ol { list-style-type: lower-roman;}</style>

1. 在 **部署业务组件** 环节，在 **计算节点 1** 框中，选择部署前准备时确定为 tsf-node1 角色的节点。在 **计算节点 2** 框中，选择部署前准备时确定为 tsf-node2 角色的节点。在 **计算节点 3** 框中，选择部署前准备时确定为 tsf-node3 角色的节点。  
**注意**：上述操作中，计算节点与 tsf-node 必须严格按照 **部署前准备** 的规划进行选择。

2. 全部选择完成后，单击 **部署业务组件** 按钮。

3. 业务组件部署完成后，进行环境检查。  
**注意**：以下操作在部署 TSF 产品的业务集群的控制节点上执行。

	1. 登录部署 TSF 产品的业务集群的任意控制节点。

	2. 检查 oss-consul-client 组件和 oss-consul-server 组件状态是否处于 running。
		
		```
		kubectl get pod --all-namespaces|grep -E "oss-consul-client|oss-consul-server"	
		```
	3. 检查所有业务组件节点的 8500 端口是否已被监听。

		```
		netstat -ntpl|grep 8500	
		```

	