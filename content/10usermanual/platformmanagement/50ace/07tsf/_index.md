+++
title = "TSF"
description = ""
weight = 7
alwaysopen = false
+++

TSF（Tencent Distributed Service Framework，腾讯分布式服务框架）是一个围绕着应用和微服务的 PaaS 平台，提供应用全生命周期管理、数据化运营和立体化监控等功能。TSF 拥抱 Spring Cloud 微服务框架，帮助企业客户解决传统集中式架构转型的困难，打造大规模高可用的分布式系统架构，实现业务、产品的快速落地。