+++
title = "产品管理（Beta）"
description = "产品管理提供相对完整的一套轻量级的、开箱即用的技术中台。除了容器平台、DevOps 平台和微服务治理平台，还提供云原生所需的其它能力支持，比如底层操作系统、存储、业务侧使用中间件等服务，打造轻量级、开箱即用的云原生技术中台。"
weight = 50
alwaysopen = false
+++

部署平台时，如启用 PaaS 云平台及相关产品，可以使用产品管理功能模块。该功能目前处于 Beta 阶段。

| 版本      | 成熟度说明                                                   | 建议使用范围                                                 |
| :-------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| **Beta**  | 可能存在缺陷，所有已支持的功能不会被轻易删除。<br>API 参数、产品设计细节等可能会随版本迭代发生变化。出现这种情况时，我们将提供迁移到下一个版本的说明。执行编辑操作时需要谨慎操作，可能需要停用依赖该功能的应用程序。 | 默认开启 Beta 版本功能，部署时可通过配置相关参数关闭。<br>后续版本中可能存在不兼容的更改，建议仅用于非关键型业务运行环境。如果有多个可以独立升级的集群，则可以放宽此限制，在生产环境使用。 |


产品管理提供相对完整的一套轻量级、开箱即用的技术中台，动态展示可部署使用的产品。  
产品管理在提供企业数字化转型所需的容器平台（Container Platform）、DevOps 平台和微服务治理平台（Service Mesh）的基础之上，还可提供完整拥抱云原生所需的其它云产品及公共服务能力，如下表所述。平台管理员可根据实际情况，部署需要的产品。

产品管理可提供的能力如下：

**云产品**

| **产品名称** | **产品类型** | **说明** |
| :--- |:--- | :-------------------- |
| Container Platform | 计算 | 基于 K8s 的容器调度平台 |
| DevOps | 应用 | 基于 DevOps 理念的云原生研发效能平台 |
| Service Mesh | 应用 | 基于容器和 Istio 的微服务治理平台 |
| TI Matrix | 应用 | 人工智能服务平台 |
| TSF | 应用 | 围绕应用和微服务的 PaaS 平台 |
| Coding | 应用 | 一站式 DevOps 研发实践工具 |
| TDSQL | 中间件 | 分布式数据库 |
| Redis | 中间件 | 弹性缓存和存储服务 |
| CSP | 存储 | 海量数据库存储 |

**公共服务**

| **产品名称** | **说明** |
| :--- | :-------------------- |
| 平台管理 | 管理集群、产品、用户角色、运营统计、审计、许可等 |
| 项目管理 | 管理平台项目、命名空间 |
| 运维中心 | 提供监控、日志、事件、告警、通知等运维能力 |
