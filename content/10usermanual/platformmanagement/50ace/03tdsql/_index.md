+++
title = "TDSQL"
description = ""
weight = 3
alwaysopen = false
+++

TDSQL（Tencent Distributed SQL，分布式数据库）是一款兼容 MySQL 的自主可控的、高一致性、分布式的数据库产品。它支持自动水平拆分，业务感受完整的逻辑表，而数据却均匀的拆分到多个物理分片中，可以有效解决超大并发、超高性能、超大容量的 OLTP 类场景。  

TDSQL 的每个分片默认采用主从高可用架构，提供弹性扩展、备份、恢复、监控等全套解决方案，为您有效解决业务快速发展时面临的各种数据库需求和挑战。

**整体架构**

![tdsqlstructure](/img/ace/tdsql/tdsqlstructure.png?classes=big)

**组件说明**

<style>table th:first-of-type { width: 170px;}</style>
<style>table th:nth-of-type(2) { width: 170px;}</style>
<style>table th:nth-of-type(3) { width: 400px;}</style>

| **组件缩写** | **组件名称** | **说明** |
| :--- |:--- | :-------------------- |
| Zookeeper | Zookeeper 集群 | 高可用和一致性集群 |
| Scheduler（Manager/Keeper） | TDSQL 调度模块 | 主备切换/扩缩容/资源管理 |
| OSS | TDSQL 操作接口 | 提供 http 的操作接口 |
| DB | 数据库模块 | 包含 mysql 和 agent |
| Proxy | 网关模块 | 账号鉴权/ SQL 解析和转发 |
| Monitor | 监控采集模块 | 从 zk 定期采集实例动态数据 |
| 赤兔（chitu） | 运营平台 | 实例管理/告警管理/权限控制 |
| Clouddba | 扁鹊系统 | SQL 优化和诊断 |
| HDFS | 冷备存储模块 | 备份 binlog / slowlog / errlog |
| LVS | 负载均衡模块 | 提供 VIP，对业务透明后端 proxy |
| Kafka | 消息队列 | 是多源同步和 es 的基础组件 |
| Consumer | 多源同步消费者 | 提供 mysql、tdsql 和oracle 之间的同步方案 |
| ES | 数据检索及分析工具 | 用于分析网关日志，帮助定位问题 |
| Onlineddl | 在线表结构变更 | 用于在 chitu 页面变更 tdsql 实例的表结构 |
