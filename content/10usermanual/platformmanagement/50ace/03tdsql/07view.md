+++
title = "查看 TDSQL 产品详情"
description = ""
weight = 7
alwaysopen = false
+++

查看已部署 TDSQL 产品的详情。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击 **产品管理**。

3. 在 **全部产品** 列表，单击 ***TDSQL 产品名称***。

4. 在 TDSQL 产品详情页，在 **基本信息** 区域，查看 TDSQL 产品的基本信息。

5. 在 **核心组件** 区域，查看 TDSQL 产品的核心组件配置，参数说明参考 [核心组件部署]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/01core.md" >}}) 文档。单击 ***赤兔运营平台链接***，可自动鉴权登录赤兔运营平台。

6. 查看已安装可靠性组件的配置信息，例如 HDFS、LVS 等，参数说明参考 [可靠性组件部署]({{< relref "10usermanual/platformmanagement/50ace/03tdsql/06install/04availability.md" >}})。

