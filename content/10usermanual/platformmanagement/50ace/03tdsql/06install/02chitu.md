+++
title = "赤兔初始化"
description = ""
weight = 2
alwaysopen = false
+++

## 赤兔初始化

浏览器登录 `http://<赤兔主机 IP>/tdsqlcloud`，进行赤兔初始化，包括集群接入、集群初始化、为系统配置数据库。

1. 环境检测

	![tdsqlstructure](/img/ace/tdsql/chitu1.png?classes=big)

2. 集群配置

	![tdsqlstructure](/img/ace/tdsql/chitu2.png?classes=big)  
	其中 **OSS 服务列表** 框中，填写 `{OSS_IP}:8080`，然后单击 **测试服务连接**。

3. 集群初始化

	![tdsqlstructure](/img/ace/tdsql/chitu3.png?classes=big)

	**新增 IDC 信息**

	![tdsqlstructure](/img/ace/tdsql/chitu4.png?classes=big)
	
	* **IDC 命名**：IDC 信息要尽量选取的有意义，如“IDC_SZ_YDGL_0401_000001”，对应着实际的“城市+机房+房间号+机架号”信息。  
	IDC 是策略上的配置，要对应到实际的机架信息上。比如有 2 个 DB 机器，为了保证 DB 实例跨机架，新增 2 个 IDC：“IDC_SZ_YDGL_0401_000001” 和 “IDC_SZ_YDGL_0401_000002”。
	
	* **IDC 权重**：IDC 权重皆使用默认值 100。要实现实例可以实际跨机架的话，推荐的做法是：  
		
		* 根据实际的机架信息，精确到编号，创建名字有意义的 IDC
		
		* 根据机器实际的分布，划分到对应的 IDC 中
		
		* 购买实例时不要指定 IP，这样就保证了逻辑上的跨机架，也是实际上的跨机架

	**新增 DB 机器的机型**

	![tdsqlstructure](/img/ace/tdsql/chitu5.png?classes=big)

	* **机型**：根据实际情况设定，机型的名字要全部大写，例如：TS 80。

	* **CPU、内存和磁盘大小**：设置机型的时候要依据实际的机器配置，比如服务器有 CPU 逻辑核数：24，内存：32g，数据盘空间：900G。机型设置的时候 CPU 核数和实际一致，设置为 24；内存设置为实际内存的 75%，为 24g；磁盘总空间设置为实际空间的 90%，为 800G。**注意**：数据盘和日志盘的大小比例应该是 3:1，数据盘 + 日志盘 = 服务器实际空间*90%。

	* **数据磁盘目录**：建议是 `/data1/tdengine/data`。

	* **日志磁盘目录**：建议是 `/data1/tdengine/log`。

	* **安装包目录**：`/data/home/tdsql/tdsqlinstall`，固定不变。

	* **数据库安装目录**：`/data/tdsql_run`，固定不变。

	* **预留资源百分比**：1%，固定不变。

	**设置网关机器的机型**

	![tdsqlstructure](/img/ace/tdsql/chitu6.png?classes=big)
	
	* **机型**：PROXY，固定不变，机型的名字要全部大写。

	* **CPU、内存和磁盘大小**：全部填 1，因为 PROXY 机型比较特殊，这些参数对它无意义。

	* **数据磁盘和日志磁盘的路径**：全部 /data 即可，无意义参数。

	* **安装包目录**：`/data/home/tdsql/tdsqlinstall`，固定不变。

	* **数据库安装目录**：`/data/tdsql_run`，固定不变。

	* **预留资源百分比**：1%，固定不变。

	**上报 DB 设备资源**

	![tdsqlstructure](/img/ace/tdsql/chitu7.png?classes=big)

	* **IP**：填写实际的 DB 机器的通信 IP 地址。

	* **机型**：选择刚才创建的机型，如 TS80。

	* **IDC**：根据实际情况，选择其归属于哪一个 IDC（不同 DB 机器要划分到不同 IDC 中）。

	* **fenceid、frame、zone**：都默认即可。

	**上报网关资源**

	![tdsqlstructure](/img/ace/tdsql/chitu8.png?classes=big)

	* **IP**：填写实际的 PROXY 机器的通信 IP 地址。
	
	* **IDC**：同一网关组内的网关要划分在同一个 IDC 中，比如都划分到“IDC_SZ_YDGL_0401_000001”。


	**新增网关组**

	例如：已上报了 2 台 PROXY 机器，且这 2 台 PROXY 机器划分在同一个 IDC 中，所以这里选择“从 1 个 IDC 中取 2 台机器”。
	
	![tdsqlstructure](/img/ace/tdsql/chitu9.png?classes=big)
	
10. 创建实例（监控 DB）

	![tdsqlstructure](/img/ace/tdsql/chitu10-1.png?classes=big)

	![tdsqlstructure](/img/ace/tdsql/chitu10-2.png?classes=big)
	
	![tdsqlstructure](/img/ace/tdsql/chitu10-3.png?classes=big)
		
	![tdsqlstructure](/img/ace/tdsql/chitu10-4.png?classes=big)

11. 为系统配置数据库

	![tdsqlstructure](/img/ace/tdsql/chitu11.png?classes=big)
	
12. 登录赤兔

	登录赤兔平台。
	
## 监控 DB 测试

在赤兔运营平台完成初始化后，进行监控 DB 测试。测试通过后则赤兔初始化成功，可进入下一步。

1. 在赤兔平台上，查看实例库监控详情，获取监控 DB 测试所需参数。

	![tdsqlstructure](/img/ace/tdsql/chitu12.png?classes=big)

2. 在 **赤兔初始化** 部署页面配置以下测试参数。

	* **监控库 IP** 和 **监控库 Port**：从网关列表中，选取一个网关配置。

	* **监控库备库 IP** 和 **监控库备库 Port**：使用网关列表中的另外一个网关配置。

	* **监控库用户名**

	* **监控库密码**

3. 单击 **开始测试**，等待测试结果。

	* 如提示测试失败，可查看日志、文档。重新设置参数后，单击 **开始测试**，再次执行测试。
	
	* 如提示测试成功，单击 **下一步**。



 