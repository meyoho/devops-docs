+++
title = "调度组件部署"
description = ""
weight = 3
alwaysopen = false
+++

根据已配置信息，部署 Scheduler。单击 **开始部署**，等待部署结果。

* 如提示部署失败，可查看日志、文档。单击 **继续部署**，继续部署未完成的组件。
	
* 如提示部署成功，单击 **下一步**。