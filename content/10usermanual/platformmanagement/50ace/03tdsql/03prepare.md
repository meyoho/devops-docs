+++
title = "部署前准备"
description = ""
weight = 3
alwaysopen = false
+++

## 总体说明

<table>
  <tr>
    <th></th>  
    <th>工作项</th>
    <th>注意项</th>
  </tr>
  <tr>
    <td rowspan="8">TDSQL 部署环境前置要求</td>
    <td>架构规划</td>
    <td>确定集群规模，服务器数量和配置。</td>    
  </tr>
  <tr>
    <td>硬件</td>
    <td>注意跨机架和跨机房上架服务器，至少 DB 机器要跨机架。目前自动化部署只兼容 x86。</td>    
  </tr>
  <tr>
    <td>网络</td>
    <td>所有机器可以互相访问，其中 LVS 的机器要在同一网段内。物理网卡建议是万兆网卡。如果部署的目标机器内网可通，推荐用内网进行部署，以保证部署速度，减少失败可能。</td>    
  </tr>
  <tr>
    <td>操作系统</td>
    <td>推荐使用 CentOS 分支（redhat），7.4 及其以上版本（其它 Linux 分支未做充分测试）</td>
  </tr>
  <tr>
    <td>存储</td>
    <td>建议服务器的 sas 盘做 raid5，ssd 盘做 raid0，文件系统建议用 xfs。</td>    
  </tr>
  <tr>
    <td>yum 源</td>
    <td>所有服务器需配置好对应系统的 yum 源，即 TDSQL 的私有源。</td>
  </tr>
  <tr>
    <td>NTP 服务</td>
    <td>部署 NTP 服务器，所有服务器连接 NTP 服务器，使用北京时间（UTC+08:00），保证服务器间的时间误差不超过 3 秒。</td>
  </tr>
  <tr>
    <td>挂载NAS（可选）</td>
    <td>如需使用 NAS 作为冷备存储，请将 NAS 在对应机器上先挂载好。</td>
  </tr>
  <tr>
    <td rowspan="2">部署</td>
    <td>部署规划</td>
    <td>详细 IP 及部署模块规划列表。</td>
  </tr>
  <tr>
    <td>部署 TDSQL</td>
    <td>安装 TDSQL 集群、系统初始化配置。</td>
  </tr>
  <tr>
    <td>验收</td>
    <td>系统测试验收</td>
    <td>操作赤兔前台，测试常用场景，记录相关内容。</td>
  </tr>
</table>

## 部署规划

**机器配置要求**

* 最小配置（适用于测试环境）

	1. 以下配置仅限于功能测试，不建议性能和可靠性测试。

	2. 至少需要 3 台机器来搭建一个最小的 TDSQL 集群（2 台物理机用于 DB、1 台虚拟机用于调度和运营体系部署）。

	| **组件** | **机器数** | **机器配置（CPU /内存/磁盘）** | **备注** |
	| :--- |:--- | :-------------------- |:--- |
	| Zookeeper | 1 台 | 虚拟机 2C/4G/100G | |
	| Scheduler | 0 台 | 虚拟机 2C/4G/100G | 可以与 zookeeper 同机部署 |
	| OSS | 0 台 | 虚拟机 2C/4G/100G | 可以与 zookeeper 同机部署 |
	| DB | 2 台 | 物理机 8C/16G/500G SSD | |
	| Proxy | 0 台 | 虚拟机 2C/4G/100G | 可以与 DB 机器同机部署 |
	| Monitor | 0 台 | 虚拟机 2C/4G/100G | 可以与 zookeeper 同机部署 |
	| 赤兔（chitu） | 0 台 | 虚拟机 2C/4G/100G | 可以与 zookeeper 同机部署 |
	| HDFS（可选） | 1 台 | 物理机 4C/4G/1T | 磁盘容量看具体需求 |
	| LVS（可选） | 2 台 | 物理机 2C/4G/100G |  |
	| ES | 1 台 | 虚拟机 2C/16G/100G | |
	| Kafka | 3 台 | 虚拟机 2C/4G/100G | 加载 java 虚拟机的时候会占用 3 个 G |
	| OnlineDDL | 0 台 | 虚拟机 2C/4G/100G | 默认与 scheduler1 同机部署 |
	| CloudDBA | 0 台 | 虚拟机 2C/4G/100G | 默认与 chitu1 同机部署 |
		
* 建议配置

	| **组件** | **机器数** | **机器配置（CPU /内存/磁盘）** | **备注** |
	| :--- |:--- | :-------------------- |:--- |
	| Zookeeper | 3 台/5 台 | 虚拟机 8C/16G/500G | 根据 IDC 信息确定具体的数目 |
	| Scheduler | 0 台 | 虚拟机 8C/16G/500G | 可以与 zookeeper 同机部署 |
	| OSS | 0 台 | 虚拟机 8C/16G/500G | 可以与 zookeeper 同机部署 |
	| DB | 3*n 台 | 物理机（建议） 32C/64G/1T SSD+200G | 一主两备，机器配置看具体需求 |
	| Proxy | 每个 IDC 3+ 台 | 物理机 32C/64G/500G | 如果规模很小，也可以与 DB 机器同机部署，机器配置看具体需求 |
	| Monitor | 0 台 | 虚拟机 8C/16G/500G | 可以与 zookeeper 同机部署 |
	| 赤兔（chitu） | 0 台 | 虚拟机 8C/16G/500G | 可以与 zookeeper 同机部署 |
	| HDFS（可选） | 3 台 | 物理机 16C/16G/12T | 磁盘容量看具体需求 |
	| LVS（可选） | 2 台 | 物理机（**必须**）32C/16G/500G | 如没有其它负载均衡则需要提供 |
	| Kafka（可选） | 3 台 | 物理机 12C/24G/1T | 多源同步组件，可以跟 hdfs 复用 |
	| Consumer（可选） | 0 台 | 物理机 16C/16G/500G | 多源同步组件，可与 kafka 混部 |
	| ES | 1 台 | 物理机 16C/32G/1T | 可选，日志汇总组件 |
	| OnlineDDL | 0 台 | 虚拟机 2C/4G/100G | 可在赤兔前台上执行部分DDL操作 |
	| CloudDBA | 0 台 | 虚拟机 2C/4G/100G | 用作性能分析和 SQL 优化等诊断功能 |
	
**机器数量规划说明**
	
TDSQL 机器配置包含 4 个节点角色，规划说明如下。

* 管理节点：包含 zookeeper、scheduler、oss、monitor、chitu 一共需要 3 或 5台机器（取决于 zk 数量）。
	
* DB 节点：包含 DB 和 Proxy，如果是 1 主 2 备的话，需要 3*n 台机器。

* 大数据节点：包含 HDFS 和 Kafka，需要 3 台机器，后续可扩容，整体容量要求是预估数据总容量的 10 倍以上。

* 其它节点：ES、Consumer、LVS。

示例：一个建议的生产环境机器规划（以只用 3 台 DB 机器为例）

* 5 台管理机器（包含 LVS）：8C/16G/500G
* 3 台 DB 机器：32C/64G/1T SSD+200G
* 3 台 PROXY 机器：32C/64G/500G
* 3 台大数据机器：16C/32G/12T
* 1 台 ES 机器：16C/32G/1T

**部署规划样例**

一个最小化部署（测试环境）的规划样例如下：

|  | **10.240.139.35** | **10.120.109.204** | **10.120.109.205** |
| :--- |:--- | :-------------------- |:--- |
| Zookeeper | Y |  |  |
| Scheduler | Y |  |  |
| OSS | Y |  |  |
| 赤兔（chitu） | Y |  |  |
| Monitor | Y |  |  |
| DB |  | Y | Y |
| Proxy |  | Y | Y |


## 免密登录配置

TDSQL 通过 Ansible 工具部署，需要为**所有**机器配置 SSH 免密登录。

复制并使用如下公钥（该固定公钥仅用于一键部署）。

<style>p { word-wrap: break-word;}</style> 
<style>p { word-break: break-all;}</style>

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNJB3HJipWmwuPFEc1YAXX0I4SZFrzg6t8vZ6IxSp9yreW4kvg4Yz10zjBDoW+0CgTbzIymNsEbqhqNX61W/olxOvWsq0NVXW0e59elvS9M3ruym+P0YAUPujo6Ic3lcpf1ZYKDkwduDMXpRqb2kWfBRCYgE2tobUkfnsLUlwh6XEyq+dZ6l2UfoTAXtlLvNdf2iNSyOv0OlzHXsKmt9ssQ+iVZES6KXC/dtXra6QR2RnjVqLpyOFCi0zgMlU4mFxxvNrg3NzGns7JxwrK6HnqoWHpJUWMXr1wqge4BczNl/Xi6T/8d8xS95KPg5/Ii3+T/yPQqYqAtVuxHRYqVnR1

	
以 root 身份登录到目标机器，使用以下命令。

`
echo "公钥" >>  ~/.ssh/authorized_keys
`  

## 数据目录准备

在 **所有** 机器上准备 /data 路径，文件格式必须是 xfs，且要求挂载的磁盘大小在 200G 以上（非 DB 机器的 /data 路径大小可以 100G）。
  
在 **DB** 机器上再准备 /data1 路径，文件格式必须是 xfs，需要单独挂载磁盘（建议做 raid 后挂载），磁盘大小根据业务数据量决定。参考以下操作说明，在 **DB** 机器上做 raid 并挂载。

<style>ol ol { list-style-type: lower-roman;}</style> 

1. 为 DB 机器准备数据文件路径 /data1，首先创建 raid，一般对 SSD 盘做 raid 0。
	
	1. 首先使用 `lsblk` 命令查看磁盘信息。

	1. 用 mdadm 工具做 raid 0，参考以下命令，磁盘路径按实际情况填写。

		```
		mdadm -C /dev/md1 -a yes -l 0 -n 3 /dev/sdc /dev/sdd /dev/sde
		```

2. 用 mkfs 工具将 raid 设备格式化为 xfs 文件系统，参考以下命令，磁盘路径按实际情况填写。
		
	```
	mkfs.xfs -f /dev/md1
	```

3. 创建目录文件 /data1 并挂载（所有磁盘的挂载都建议是 xfs 并且用 noatime 参数）。
	
	1. 使用 `mkdir -p /data1` 命令创建目录。

	2. 修改 /etc/fstab 文件，设置自动挂载到 /data1 路径，使用以下命令。
		
		```
		/dev/md1 /data1 xfs defaults,noatime 0 0
		```

	3. 使用 `mount -a` 命令挂载磁盘。
		
	4. 使用 `df -hT` 命令可以看到挂载成功。


## HDFS 部署前准备（可选）

如规划部署 HDFS，参考以下说明，进行部署前准备。

**准备 HDFS 的数据目录路径**

1. 用 mkfs 工具将磁盘格式化为 xfs 文件系统。

	1. 首先使用 `lsblk` 命令查看磁盘信息。

	2. 将磁盘格式化为 xfs 文件系统。

		```
		mkfs.xfs -f /dev/sdg
		mkfs.xfs -f /dev/sdh
		mkfs.xfs -f /dev/sdi
		```	
2. 创建目录文件并挂载。

	1. 使用 `mkdir -p /data{2..4}` 命令创建目录。

	2. 修改 /etc/fstab 文件，设置自动挂载到指定路径。

		```
		/dev/sdg /data2 xfs defaults,noatime 0 0
		/dev/sdh /data3 xfs defaults,noatime 0 0
		/dev/sdi /data4 xfs defaults,noatime 0 0
		```	
	
	3. 使用 `mount -a` 命令挂载磁盘。

	4. 使用 `df -hT` 命令可以看到挂载成功。

**设置主机名**

在每台 HDFS 机器上设置主机名，HDFS 集群中主机名不能重复，不能有下划线、中划线等特殊字符，例如：
		
```
hostnamectl set-hostname node01
hostnamectl set-hostname node02
hostnamectl set-hostname node03
```	

## yum 源配置

如果是内网部署，必须在目标主机上手动执行以下命令。

其中 baseurl 的 ip 是 yum 源的 ip，默认是平台的 int 节点。

```
cat << eof > /etc/yum.repos.d/alauda.repo
[cpaas]
name=cpaas
baseurl=http://ip:7000/yum
enabled=1
gpgcheck=0
eof
```


