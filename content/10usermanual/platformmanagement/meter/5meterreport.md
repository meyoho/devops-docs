+++
title = "统计报表"
description = "平台的运营概览通过可视化报表的方式，为您展示平台上统计时间范围内，CPU/内存的使用总量，以及使用量排名前 5 的项目或命名空间的使用量统计数据。"
weight = 5

+++

平台可保留近 18 个月的统计数据，支持查看自定义时间范围内（最近 6 个月内），统计项的可视化报表数据。可根据统计粒度（项目、命名空间）、项目（全部、自定义项目）进行筛选。

统计项包括：容器组使用量、容器组配额、项目配额、命名空间配额。

同时，支持将统计数据导出为 csv 格式的统计报表。统计报表导出后，可在 **导出记录** 页面查看导出记录并下载报表。

**说明**：仅当 **统计项** 为 `容器组配额`、`容器组使用量` 时，支持查看明细数据。

**前提条件**

如果需要导出统计报表，需要再平台部署时，为运营统计配置存储。



**操作步骤**

1. 使用平台管理员账号登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

2. 在左侧导航栏中单击  **运营统计** > **统计报表**，进入统计报表页面。

1. 参照下图的说明，选择统计时间范围、统计项、统计粒度，配置项目过滤条件后单击 **搜索** 按钮，可查看相应的统计报表数据。
	
	**说明**：
	
	* 平台仅保留 18 个月内的数据，单次查询，支持的最大时间范围为 6 个月。
	
	* 容器组使用量：按照 Pod 实际使用量统计，每 5 分钟取一次实际使用量的平均值计算得出的总和。
	
	* 容器组配额：按照 Pod 的请求值（request）大小统计。
	
	* 命名空间配额：按照命名空间 CPU、内存配额统计，每天统计 1 次，配额变更后第二天起按照变更后配额统计。
	
	* 项目配额：按照项目 CPU、内存配额统计，每天计算 1 次，配额变更后第二天起按照变更后配额统计。
	
	* 当 **统计项** 为 `容器组配额`、`容器组使用量` 时，支持查看明细。
	
	* 支持根据 **命名空间名称**（首字母）、**所属项目**（名称首字母）、**CPU 使用总量**（大小）、**内存使用总量**（大小）排序。

	 
	
	
	<img src="/img/00meter2.svg" width = "860" />	
	
