+++
title = "扩展组件"
description = "平台支持通过部署扩展组件的方式为 kubernetes 集群集成更多的能力。"
weight = 10
+++

平台支持通过部署扩展组件的方式为 kubernetes 集群集成更多的能力，例如：GPU 管理、IP 地址管理、Tapp 管理等。

平台管理员可根据业务需要，选择为平台上的任意集群部署所需扩展组件。扩展组件部署成功后，会以 Deployment 的形式运行在集群的指定命名空间下。

**注意**：

* 一种类型的扩展组件在指定的集群上不能重复部署。

* 某些组件部署后，还需要其他资源的支持才可提供相应的能力。例如：在集群已部署了 GPU 资源的情况下，部署 GPUManager 扩展组件，才能提供 GPU 管理能力。


{{%children style="card" description="true" %}}



