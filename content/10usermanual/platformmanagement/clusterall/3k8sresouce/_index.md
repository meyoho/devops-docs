+++
title = "资源管理"
description = "平台全面集成 Kubernetes，通过管理视图的资源管理功能，可以查看、更新平台中 Kubernetes 资源的 YAML 编排文件；可以使用自定义的 YAML 编排文件，创建任意类别的 Kubernetes 资源（包括平台中未分类管理的资源），提供 Kubernetes 集群的深度使用体验。"
weight = 2
alwaysopen = false
+++

平台全面集成 Kubernetes，通过管理视图的资源管理功能，可以查看、更新平台中 Kubernetes 资源的 YAML 编排文件；可以使用自定义的 YAML 编排文件，创建任意类别的 Kubernetes 资源（包括平台中未分类管理的资源），提供 Kubernetes 集群的深度使用体验。

<img src="/img/k8sresource.png" width = "860" />

{{%children style="card" description="true" %}}



