+++
title = "集群管理"
description = "平台管理员可通过平台管理集群、主机节点、Pod以及集群下 Kubernetes 资源。"
weight = 1
alwaysopen = false
+++

集群是容器运行所需要的资源合集，包含主机节点、网络、存储等资源。您可以在集群中运行您的应用。

平台管理员可通过平台管理集群、主机节点、Pod 以及集群下 Kubernetes 资源。

{{%children style="card" description="true" %}}



