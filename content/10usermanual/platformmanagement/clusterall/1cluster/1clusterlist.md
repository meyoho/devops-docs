+++
title = "查看集群列表"
description = "查看平台上的集群的列表信息。"
weight = 1

+++

平台管理员可查看平台上的集群的列表信息。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

1. 单击左侧导航栏中的 **集群管理** > **集群**，进入集群列表页面。

	在集群列表页面，可查看集群的以下信息：
	
	<style>table th:first-of-type { width: 20%;}</style> 
	
	| 参数     | 说明                                                         |
	| -------- | --------------------- |
	|       **名称**     |             集群的名称及显示名称。分两行显示，上方为集群的名称，下方为集群的显示名称。<br>名称旁带有 ![](/img/cluster5.png) 图标时，表明该集群为高可用集群。<br>名称旁带有 ![](/img/00fed.png) 图标时，表明该集群为联邦集群成员。        |
	|       **状态**     |             集群的当前状态。<br>**正常**：可连接到集群的 apiserver ，可访问集群下的命名空间。<br>**异常**：无法连接到集群的 apiserver，无法正常访问集群下的命名空间。<br>**创建中** ：集群正处在被创建的过程中。 <br>**接入中**：集群正处在被接入平台的过程中。<br>**删除中**：集群正处在被删除的过程中，此时集群无法使用，且不支持其他操作。                       |
	|       **集群类型**     |             平台上集群的类型。包括：<br>自建：通过平台上的 **创建集群**  功入口创建的集群。<br>接入：通过 **接入集群** 功能入口，接入 平台的已有的标准 Kubernetes 集群或 OpenShift 集群。                      |
	|       **主机数**     |             集群中包含的主机总数。光标悬浮在数字左侧的环形图上时可查看处于正常、异常状态的主机个数，如图所示。<br> ![](/img/clusternode.png)                        |
	|       **利用率**     |      **注意**：仅当集群已部署了 Prometheus 时，可查看 **CPU/内存利用率**；集群状态异常或未部署  Prometheus 时，仅显示资源总额。<br>**利用率** = 实际使用的值 / 总额。<br>当利用率 小于 70% 时，数字显示为正常颜色；当利用率大于 70% 小于 90% 时，数字显示为黄色；当利用率大于 90% 时，数字显示为红色。<br><br>光标悬浮在利用率占比图上时，会显示 **已使用**、**请求值**（request）、**限制值**（limit）、**总额** 以及占比的详细数据信息。如图所示。<br>![](/img/clusteruseratio.png) <br>其中，**请求值/限制值的百分比** = 请求值/限制值 / 总额。              |
	|       **创建时间**     |             集群在平台上创建或接入的时间。                       |
	|       **快捷操作**     |      单击集群记录右侧的 ![](/img/003point.png) 图标，可对集群执行以下操作：<br>**删除**：从平台上删除集群。支持删除处于 **正常**、**异常**、**创建中**、**接入中** 状态的集群。<br>**查看执行进度**：查看状态处于 **创建中** 集群状态（status）的转换情况。                    |
    
    **提示**：
    
	* 列表支持根据 **名称首字母**（a~z、z~a）或 **创建时间**（正序、倒序）排序。
	
	* 单击列表右上方的搜索框，可输入名称进行模糊搜索。
	
	* 单击搜索框右侧的 ![](/img/refresh.png) 图标，可刷新列表。

