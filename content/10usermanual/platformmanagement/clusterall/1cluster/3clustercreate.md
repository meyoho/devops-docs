+++
title = "创建集群"
description = "在平台上创建 Kubernetes 业务集群。"
weight = 3
+++


在平台上创建一个 Kubernetes 业务集群。

**注意**：当控制节点只有 1 个时，为单节点集群，非高可用，不建议将单点集群作为生产环境集群。


集群创建成功后，可查看执行进度、关联项目，具体操作请参考本节的 **后续操作** 内容。


**前提条件**

已准备好待添加至集群的主机。如需为集群配置 GPU 参数，且控制节点会作为 GPU 主机使用，需要先行为 GPU 主机安装 GPU 驱动，请参考 [为 GPU 主机安装 GPU 驱动]({{< relref "10usermanual/platformmanagement/clusterall/1cluster/10node/30gpudriver.md" >}})。

若待添加主机个数超过 1 个时，需要保证待添加主机的端口、网卡、认证信息统一。具体操作请参见平台的部署文档或运维手册。



**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

1. 单击左侧导航栏中的 **集群管理** > **集群**，进入集群列表页面。

3. 单击 **创建集群** 按钮，进入创建集群页面。

4. 参照以下说明，配置集群相关参数。

	<style>table th:first-of-type { width: 20%;}</style> 
	
	<style>table th:nth-of-type(2) { width: 10%;}</style>

	|   参数 |  是否必填  |   说明     |
	| ------ | -------- | -------- |
	|  **名称**  |   是    | 集群的名称。<br>支持输入 `a-z` 、`0-9`、`-`，以 `a-z` 、`0-9` 开头或结尾，且总长度不超过 32 个字符。|
	|  **显示名称**   | 是 |  集群的显示名称。|
	|  **GPU**   | 否 |  是否为集群部署 GPU 资源的滑块开关，默认为关闭，单击后开启。当开关开启时，可通过单击下拉选择框，可选择：<br>**vGPU**：虚拟化 GPU 核心，100 个单位的虚拟核心等于 1 个物理核心；1 个单位的显存为 256Mi。<br>**pGPU**：物理 GPU，只能整颗分配。|
	|  **集群地址**   | 是 |  集群对外暴露 API Server 的访问地址，使用 https 协议。<br>**IP 地址/域名**：<br>当集群只有一个控制节点（master）时，输入 master 节点的 **私有 IP** 或域名。<br>当集群有 2 个或 2 个以上控制节点（master）时，需要输入 **VIP** 或域名。<br><br>**端口**：端口号。**说明**：有负载均衡时，请输入负载均衡的端口，没有负载均衡时，必须输入 `6443`。|
	|  **控制节点**   | 是 |  集群的控制节点（master）的信息。<br>在 **IP 地址** 输入框中，输入控制节点的 IP 地址。处于内网模式创建集群，请输入节点的 **私有 IP**。<br>单击 ![](/img/add.png) 按钮可添加多个控制节点；单击 ![](/img/00delete1.png) 图标，可移除一个控制节点配置。<br>**注意**：控制节点数大于 1 时，为高可用集群，所有节点的端口、网卡、认证信息要统一。 <br><br>（非必填）若 **GPU** 开关开启时，可通过单击 **GPU 主机** 下方的 ![](/img/switch.png) 开关，控制是否在该主机节点上部署 GPU 相关组件。<br><br>**注意**：一台主机只能属于一个集群，待添加主机不能被其他集群占用，否则，无法通过 Kubernetes 校验，不能成功创建集群。|
	|  **计算节点**   | 否 |  是否将控制节点作为计算节点（node）使用的滑块开关，默认为关闭，单击后打开。<br>**注意**：如果集群中没有计算节点，集群仅能运行平台组件和少量业务，无法正常运行生产业务。可通过 [添加主机]({{< relref "10usermanual/platformmanagement/clusterall/1cluster/10node/4nodeadd.md" >}}) 为集群添加计算节点。|
	|  主机认证-**SSH 端口**   | 是 |  主机的 SSH 协议端口号，例如：`22`。|
	|  主机认证-**认证方式**   | 是 |  登录已添加节点的认证方式及对应的认证信息。可选择：<br>**密码**：需要输入具有 root 权限的 **用户名** 及对应的 **密码**。<br>**密钥**：需要输入具有 root 权限的 **用户名**、**私钥** 以及 **私钥密码**。|
	|  **网卡**   | 是 |  网卡设备的名称，默认为 eth0。|
	|  **网络模式**   | 否 |  集群的网络模式，默认为 `Flannel`。支持选择：<br>**Flannel**：为集群内所有容器提供一个扁平化的网络环境。所有容器在 Flannel 提供的网络平面上可以看作是在同一网段，可自由通信。其模型让集群上全部的容器使用一个网络，从网络中为每个节点划分一个子网。节点上的容器创建网络时，从子网中划分一个 IP 给容器，提高了容器之间通信效率，不用考虑 IP 转换问题。<br>**Kube-OVN**：该模式下不同 Namespace 对应着不同的子网，同一台机器的不同 Pod 可以使用不同的子网，多个子网之间网络可以联通。在容器访问外部网络的策略中，Kube-OVN 设计了两种方案：一种是分布式网关，每台主机都可以作为当前主机上 Pod 的出网节点，做到出网的分布式；一种是和 Namespace 绑定的集中式网关，可以做到一个 Namespace 下的 Pod 流量使用一个集中式的主机出网。<br>**Calico**：Calico 是一种可为容器提供安全网络连接的三层网络方案，具有简单、易扩展、安全性高、性能好等优势。<br>**Galaxy-FloatingIP**：Galaxy 是由腾讯提供的 CNI 框架，实现了 Overlay、Floating-IP、NAT、Host 四种网络解决方案，可以动态地为容器配置 Underlay/Overlay 网络以及端口映射。<br>**无网络**：如需安装其他网络插件，请选择 **无网络** 模式 ，集群创建成功后可手动安装。|
	|  **容器网络**   | 是 |  集群的容器网络（CNI）的配置信息。<br><br>**Cluster CIDR**：容器网络的子网地址（CIDR 格式），例如：`10.1.0.0/16`，该地址应与节点子网不同。集群创建时的 Pod 可使用该网段内的 IP。<br>**Service CIDR**：Kubernetes Service 的网络地址范围（CIDR 格式），例如：`10.96.0.0/12`，不可与 **Cluster CIDR** 的网段重叠。<br>**节点 IP 个数**：单击下拉选择框，选择当前容器网络下允许在每个节点上分配给容器组的最大 IP 个数。可选：128、256，默认为 128。<br>**说明**：会根据以上配置，自动计算集群最多可容纳的节点个数，并在输入框下方的提示内容中展示。|
	
4. （非必填）单击 **扩展参数** 按钮，展开扩展参数配置区域，可选择为集群设置以下扩展参数：

	*  Docker 参数：`dockerExtraArgs`，Docker 额外的配置参数，将写入 `/etc/sysconfig/docker`，建议不要修改。通过 `daemon.json` 文件配置 Docker，必须以键、值的方式进行配置。
	*  Kubelet 参数：`kubeletExtraArgs`，Kubelet 额外的配置参数。
	*  Controller Manager 参数：`controllerManagerExtraArgs`，Controller Manager 额外的配置参数。
	*  Scheduler 参数：`schedulerExtraArgs`，Scheduler 额外的配置参数。
	*  APIServer 参数：`apiServerExtraArgs`，APIServer 额外的配置参数。
	* APIServer 地址：`publicAlternativeNames`，签发到证书中的 APIServer 访问地址，只能填写 IP 或域名，最多 253 个字符。

	**键的输入规则**：支持输入 `a-z`、`A-Z`、`0-9`、`-`、`_`、`.`，以 `a-z`、`A-Z`、`0-9` 开头/结尾，最多 253 个字符。
	
	**值的输入规则**：支持输入 `a-z`、`A-Z`、`0-9` 以及特殊字符（`~!@#$%^&*()_+-;[]{}'/?.<>|\=），最多 253 个字符。


	**注意**：
	
	* 不建议设置扩展参数，设置错误可能导致集群不可用，且集群创建后无法修改。
	
	* 如果填写的 **键** 与默认参数 **键** 重复，则会覆盖默认配置。

	
		
	单击要配置参数区域的 ![](/img/add.png) 按钮，在添加的输入框中输入 **键**、对应的 **值** 或 APIServer 地址即可。

4. 确认信息无误后，单击 **创建** 按钮，返回集群列表页面，集群处于 **创建中** 状态。

	**提示**：单击搜索框右侧的 ![](/img/refresh.png) 图标，可刷新列表。

	
## 后续操作	

### 查看执行进度
	
在集群列表页面，可查看已创建的集群的列表信息，其中，处在 **创建中** 状态的集群，可查看执行进度。

**操作步骤**

1. 单击处在  **创建中** 状态的集群右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中，选择 **查看执行进度**。

2. 在弹出的执行进度弹窗中，可查看集群的执行进度（status.conditions）。

	**提示**：当某个类型正在执行中或状态为失败且有原因时，光标悬浮在对应的原因（显示蓝色字体）上时，可查看原因（status.conditions.reason）的详细信息。

	
### 关联项目

通过基于集群 [创建项目]({{< relref "10usermanual/projectmanagement/project/1addproject.md" >}})，或通过 [添加项目关联集群]({{< relref "10usermanual/projectmanagement/project/5addcluster.md" >}}) 的方式将集群添加至已有项目，可将新创建的集群关联至项目。

进而，通过  [创建命名空间]({{< relref "10usermanual/projectmanagement/namespace/1createns.md" >}}) 可在集群下创建命名空间。


	

