+++
title = "接入集群"
description = "支持在平台上接入已部署的标准的原生 Kubernetes 集群或 OpenShift 集群，并通过平台管理接入的集群。"
weight = 5
+++

支持在平台上接入已部署的标准的原生 Kubernetes 集群或 OpenShift 集群，并通过平台管理接入的集群。


**注意**：如需接入 OpenShift 集群，请联系管理员来完成相关接入准备。若未进行相关准备操作，即使成功接入集群，数据也会显示异常且无法正常使用。


集群接入成功后，可查看执行进度、纳管集群下命名空间，具体操作请参考本节的 **后续操作** 内容。

**前提条件**

在接入 标准的 Kubernetes 集群之前，需要修改集群中所有 Master 节点的配置文件 “/etc/kubernetes/manifests/kube-apiserver.yaml”，如果没有该配置文件，请在相同路径下创建同名配置文件。

请严格按照以下参数设置配置文件中 Kubernetes API Server 相关的配置信息，未设置或设置错误可能会遇到权限不同步等问题，影响您正常使用平台。

```
--oidc-issuer-url=https://10.0.129.100/dex；OIDC 的服务提供商，必须使用 https 协议
--oidc-client-id=alauda-auth；OIDC 客户端的唯一标识（ID）
--oidc-ca-file=/etc/kubernetes/pki/apiserver-dex-ca.crt；OIDC 证书路径，您可在接入集群页面的提示框内单击“下载”链接下载证书，并在目标接入集群的如上路径内导入该证书
--oidc-username-claim=email；从 JWT 的声明字段中选取 email 字段作为用户名
--oidc-groups-claim=groups；从 JWT 的声明字段中选取 groups 字段作为分组名
```

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

1. 单击左侧导航栏中的 **集群管理** > **集群**，进入集群列表页面。

3. 单击 **接入集群** 按钮，进入接入集群页面。

4. 参照以下说明，配置接入集群的参数。

	<style>table th:first-of-type { width: 20%;}</style> 
	
	<style>table th:nth-of-type(2) { width: 10%;}</style>

	|   参数 |  是否必填  |   说明     |
	| ------ | -------- | -------- |
	|  **集群类型**  |   是    | 单击选择 **标准 Kubernetes 集群** 或 **OpenShift 集群**。<br>**注意**：如需接入 OpenShift 集群，需联系管理员来完成相关接入准备；若未进行相关准备操作，即使成功接入集群，数据也会显示异常且无法正常使用。|
	|  **名称**  |   是    | 集群的名称。<br>支持输入 `a-z` 、`0-9`、`-`，以 `a-z` 、`0-9` 开头或结尾，且总长度不超过 32 个字符。|
	|  **显示名称**   | 是 |  集群的显示名称。|
	|  **集群地址**   | 是 |  集群对外暴露 API Server 的访问地址，用于平台对集群 API Server 的访问。<br>**IP 地址/域名**：输入访问集群 API Server 的 IP 地址或域名。<br>**端口**：端口号。|
	|  **Token**   | 是 | 集群的 Token，是访问集群 API Server 的认证信息（Token）。 |
	|  **CA 认证**   | 是 | 登录集群的 CA 证书。 |
	|  **集群管理地址**   | 否 | 用于管理 Kubernetes 集群的企业级 Kubernetes 发行版的访问地址。配置了该参数后，在集群详情页面的基本信息区域，单击 **集群管理地址**，可跳转对应的集群管理页面。 |
    
    

3. 确认信息无误后，单击 **接入** 按钮。

4. 在弹出的确认提示框中，单击 **接入** 按钮，返回集群列表页面，集群处于 **接入中** 状态。

	**提示**：单击搜索框右侧的 ![](/img/refresh.png) 图标，可刷新列表。


## 后续操作	

	
	
### 纳管集群下命名空间

接入集群后，您可通过基于集群 [创建项目]({{< relref "10usermanual/projectmanagement/project/1addproject.md" >}})，或通过 [添加项目关联集群]({{< relref "10usermanual/projectmanagement/project/5addcluster.md" >}}) 的方式将集群添加至已有项目，可将新接入的集群关联至项目。

进而，通过 [导入命名空间]({{< relref "10usermanual/projectmanagement/namespace/2importns.md" >}}) 操作，将集群下已有的 Kubernetes 命名空间纳入到平台的项目下进行管理。	