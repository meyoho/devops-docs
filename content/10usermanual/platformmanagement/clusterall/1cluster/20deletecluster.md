+++
title = "删除集群"
description = "删除不再计划通过本平台管理使用的 Kubernetes 集群。"
weight = 20
+++

删除不再计划通过本平台管理使用的 Kubernetes 集群。

**注意**：当集群已被项目使用，集群删除后，项目将无法使用该集群。集群下的项目资源会保留。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

1. 单击左侧导航栏中的 **集群管理** > **集群**，进入集群列表页面。

3. 单击待设置主机所在的 ***集群名称***，进入集群详情页面。

4. 单击待删除集群记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**。

	**或**：
	
	1. 单击待设置的 ***集群名称***，进入集群详情页面。
	
	2. 单击右上角 **操作** 下拉按钮并选择 **删除集群**。

3. 在弹出的确认对话框中，输入集群名称后，单击 **删除** 按钮。
	