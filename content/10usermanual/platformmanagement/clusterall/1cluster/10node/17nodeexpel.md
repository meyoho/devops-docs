+++
title = "驱逐容器组"
description = "将不可调度主机设定为可调度状态，允许将新创建的 Pod 调度到主机上。"
weight = 17
+++

将状态正常的主机上除 DaemonSet 管理的容器组之外的所有容器组，驱逐到集群内其他主机上，并将该主机设置为不可调度状态。

**注意**：本地存储的容器组被驱逐后数据将丢失，请谨慎操作。


**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

1. 单击左侧导航栏中的 **集群管理** > **集群**，进入集群列表页面。

3. 单击待设置主机所在的 ***集群名称***，进入集群详情页面。
	
1. 单击待设置的 ***主机名称***，进入主机详情页面。
	
2. 单击右上角 **操作** 下拉按钮并选择 **驱逐容器组**。

5. 在弹出的确认对话框中，可查看待驱逐的容器组信息，单击 **驱逐** 按钮。

