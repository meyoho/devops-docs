+++
title = "为 GPU 主机安装 GPU 驱动"
description = "为集群部署 GPU 资源的前提是集群下存在具备 GPU 计算能力的 GPU 主机，且 GPU 主机已安装了 GPU 驱动。"
weight = 30
+++

为集群部署 GPU 资源的前提是集群下存在具备 GPU 计算能力的 GPU 主机，且 GPU 主机已安装了 GPU 驱动。

本节将为您介绍如何下载并安装 GPU 驱动。

以下两种情况，需要先为主机安装 GPU 驱动。

* 创建集群页面，勾选了 GPU 选项，并选择为部分或全部具备 GPU 计算能力的控制节点打开 GPU 开关时，需要为打开 GPU 开关的主机先行安装 GPU 驱动。

	
	<img src="/img/00clustergpu.png" width = "800" />

* 为部署了 GPU 资源的集群，添加具备 GPU 计算能力的主机时，需要为待添加的主机先行安装 GPU 驱动。

	<img src="/img/00nodegpu.png" width = "600" />
	

**注意**：目前，仅 **CentOS 7.6** 版本的操作系统经过了安装验证并测试通过，CentOS 7.x 其他版本未经充分测试，可能存在安装失败的风险。

## 下载 GPU 驱动	

在安装 GPU 驱动之前，需要根据 GPU 主机的 GPU 型号，下载相应的驱动。主要操作步骤如下：

1. 登录 GPU 主机，执行命令 `lspci |grep -i NVIDIA` 查看主机的 GPU 型号，以下示例中 GPU型号为：Tesla V100 SXM2。

	```
	[root@VM_0_4_centos ~]# lspci |grep -i NVIDIA
	00:06.0 3D controller: NVIDIA Corporation GV100GL [Tesla V100 SXM2 32GB] (rev a1)
	```

2. 前往 [NVIDIA 官网](https://www.nvidia.cn/) 下载驱动。 

	1. 在官网首页的导航栏中选择 **驱动程序** > **所有 NVIDIA 驱动程序**。
	
	2. 根据 GPU 主机的型号，填写驱动下载所需信息，如下图所示。

		<img src="/img/00gpudriver.png" width = "400" />
		
	3. 单击 **搜索** 按钮，进入下载页面。
	
	4. 单击 **DOWNLOAD** 按钮，进入填写个人信息页面。
	
	5. 单击 SUBMIT 按钮下方 **Skip & Continue to Download >>**，进入下载页面。
	
	6. 右键单击 **AGREE & DOWNLOAD** 按钮，在弹出的列表中选择 **复制链接地址** 即可复制驱动的下载链接。

3. 在 GPU 主机上创建 `/home/gpu` 目录，使用 `wget` 命令下载并将驱动文件保存至 `/home/gpu` 目录下。参考命令如下：

	```
	[root@VM_0_4_centos ~]# mkdir -p /home/gpu
	[root@VM_0_4_centos ~]# cd /home/gpu/
	[root@VM_0_4_centos ~]# wget http://us.download.nvidia.com/tesla/440.33.01/NVIDIA-Linux-x86_64-440.33.01.run
	[root@VM_0_4_centos ~]# ls NVIDIA-Linux-x86_64-440.33.01.run
	NVIDIA-Linux-x86_64-440.33.01.run
	```

## 在 GPU 主机上安装 GPU 驱动

以下将以 p40 这个型号的 GPU 驱动为例，说明如何在 GPU 主机上安装 GPU 驱动。

1. 将安装目录下的文件 `other/gpu/NVIDIA-Linux-x86_64-440.64.00.run`  拷贝到需要安装 GPU 驱动的主机上。

3. 安装当前系统对应的 gcc 和 kernel-devel 包。如需 `*.run` 类型的驱动依赖，请自行安装。

	`sudo yum install -y gcc kernel-devel-xxx`
	
	**说明**：`xxx` 是内核版本号，可以执行 `uname -r` 命令查看。

4. 执行以下命令，为 GPU 主机安装 GPU 驱动。

	```
	chmod a+x NVIDIA-Linux-x86_64-440.64.00.run
	sudo /bin/bash ./NVIDIA-Linux-x86_64-440.64.00.run
	```


2. 安装完成后，执行 `nvidia-smi ` 命令，如果返回类似如下的 GPU 信息，说明驱动安装成功。

	```
	[root@VM_0_4_centos gpu]# nvidia-smi
	Wed Jan  8 16:53:07 2020      
	+——————————————————————————————————————+
	| NVIDIA-SMI 440.33.01    Driver Version: 440.33.01    CUDA Version: 10.2     |
	|———————————————+———————————+———————————+
	| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
	| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
	|===============================+======================+======================|
	|   0  Tesla V100-SXM2…  Off  | 00000000:00:06.0 Off |                    0 |
	| N/A   33C    P0    38W / 300W |  31014MiB / 32510MiB |      1%      Default |
	+———————————————+———————————+———————————+
	                                                                                
	+——————————————————————————————————————+
	| Processes:                                                       GPU Memory |
	|  GPU       PID   Type   Process name                             Usage      |
	|=============================================================================||
	+——————————————————————————————————————+
	```
