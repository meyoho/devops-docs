+++
title = "创建联邦集群"
description = "在平台上创建一个联邦集群。"
weight = 2
+++


在平台上创建一个联邦集群。

**注意**：建议添加空集群作为联邦集群的成员，如果集群中已存在的资源与联邦化资源同名，可能会被覆盖。

**前提条件**

平台上存在不属于联邦集群的集群。一个集群只能属于一个联邦集群。


**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

2. 单击左侧导航栏的 **集群管理** > **联邦集群**，进入联邦集群列表页面。

3. 单击 **创建联邦集群** 按钮，进入创建联邦集群页面。参照以下说明，配置参数。

	<style>table th:first-of-type { width: 20%;}</style> 
	
	<style>table th:nth-of-type(2) { width: 20%;}</style>
	
	| 参数     | 是否必填  |  说明                                                         |
	| -------- |  ---- | --------------------- |
	|       **名称**     |   是   |         联邦集群的名称。<br>支持输入 `a-z` 、`0-9`、`-`，并且以 `a-z`、`0-9` 开头或结尾，长度不超过 32 字符。                         |
	|       **显示名称**     |   是 |          联邦集群的显示名称。                       |
	|       **联邦成员**     |   是 |           单击下拉选择框，选择平台上不属于联邦集群的集群，支持选择多个，至少需要一个成员（控制集群）。                    |
	|       **控制集群**     |     是 |          单击下拉选择框，在已选择的联邦成员当中，选择一个集群作为控制集群。 <br>**说明**：<br> 一个联邦集群必须有且可有一个控制集群，控制集群是联邦集群的成员之一。<br>控制集群不可从联邦集群中移除。                     |

4. 单击 **创建** 按钮。

	将进入联邦集群的详情页面，联邦集群处于创建中状态。


 

