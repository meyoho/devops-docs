+++
title = "用户角色管理"
description = "平台的用户管理、角色管理功能，基于 RBAC（Role-Based Access Control，角色的权限访问控制）实现。"
weight = 30
alwaysopen = false
+++

平台的用户管理、角色管理功能，基于 RBAC（Role-Based Access Control，角色的权限访问控制）实现。

平台基于企业常用的权限使用场景预设了 5 个系统角色，同时也支持平台管理员根据实际使用场景自定义角色。将权限和角色进行关联后，通过为不同的用户分配角色，即可将权限赋予用户；同理，通过删除用户已拥有的角色，即可实现权限的回收。

权限和角色之间多对多关系、角色和用户之间多对多关系的建立，能够满足更为广泛且复杂的权限管理场景，极大地简化了权限管理操作。



{{%children style="card" description="true" %}}
