+++
title = "IDP 配置"
description = "通过配置 IDP，可灵活添加 LDAP、OIDC。"
weight = 30
alwaysopen = false
+++


平台的 IDP（Identity Provider）配置，支持平台管理员手动添加 LDAP 和 OIDC。

通过同步 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）导入企业已有的用户体系；同时，支持 OIDC（OpenId Connect）协议，可使用平台认可的第三方账号登录平台。

{{%children style="card" description="true" %}}
