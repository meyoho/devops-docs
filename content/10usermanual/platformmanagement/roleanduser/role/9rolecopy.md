+++
title = "复制已有角色为新角色"
description = "支持将平台上的已有角色，包括系统角色和自定义角色，复制为新的角色。"
weight = 9
+++


支持将平台上的已有角色，包括系统角色和自定义角色，复制为新的角色。复制角色时，将会复制已有角色的所有权限信息，您可根据实际使用场景在已复制的角色的基础上为新角色增、删权限。

**前提条件**

您的账号已拥有平台管理员角色。

**操作步骤**

1. 登录平台后，在顶部导航栏单击 **平台中心** > **平台管理** ，打开平台管理页面。

2. 在左侧导航栏中单击 **用户角色管理** > **角色管理**，进入角色列表页面。

3. 单击待复制的 ***角色的名称***，进入角色的详情页面。

4. 单击页面右上角的 **操作** 下拉按钮，并选择 **复制为新角色**。

5. 在 **复制为新角色** 页面，可参考  [创建自定义角色]({{< relref "10usermanual/platformmanagement/roleanduser/role/2rolecreate.md" >}}) 配置新角色的名称、显示名称、描述、类型、权限。

6. 单击 **创建** 按钮。


	
	
	

