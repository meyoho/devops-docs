+++
title = "凭据"
description = "使用凭据保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中"
weight = 11
+++

使用凭据保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。

凭据支持以下几种类型：

* 用户名和密码：用于验证访问 Jenkins 服务或仓库用户的身份信息。

* OAuth2：⽤ OAuth 2.0 协议的客户端模式（Client Credentials），使⽤⾃⼰的 client 证书来获取 access token。

* 镜像服务：用于存储私有 Docker registry 的认证信息。

* SSH：ssh-auth 类型，用于存储通过 SSH 协议传输数据的认证信息。



{{%children style="card" description="true" %}}