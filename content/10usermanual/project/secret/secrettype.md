+++
title = "DevOps 工具的凭据类型"
description = "DevOps 工具的凭据类型和凭据内容。"
weight = 6
+++

DevOps 工具的凭据类型和凭据内容:

| 工具            | 凭据类型               | 凭据内容                 |官方帮助文档|
| --------------- | ---------------------- | ----------------------|-----------|
| GitHub          | 用户名/Token             | 用户名、Personal Access Token   |[GitHub 官方帮助文档](https://help.github.com/cn/github/authenticating-to-github/creating-a-personal-access-token-for-the-command-line)|
| GitHub          | OAuth2          		  | Client ID、Client Secret |[GitHub 官方帮助文档](https://help.github.com/cn/github/authenticating-to-github/authorizing-oauth-apps)|
| GitLab          | 用户名/Token             | 用户名、Personal Access Token   |[GitLab 官方帮助文档](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens)|
|  GitLab         | OAuth2          		  | Application ID、Secret   |[GitLab 官方帮助文档](https://docs.gitlab.com/ee/integration/oauth_provider.html#gitlab-as-oauth2-authentication-service-provider)|
| Bitbucket       | 用户名/Token            | 用户名、App password   |[Bitbucket 官方帮助文档](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html#Apppasswords-Createanapppassword)|
| Bitbucket       | OAuth2          	     | Application ID、Secret  |[Bitbucket 官方帮助文档](https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html)|
| Gitee             | 用户名/Token           | 用户名、私人令牌  |-|
| Gitee            | OAuth2          		   | Application ID、Secret |[Gitee 官方帮助文档](https://gitee.com/api/v5/oauth_doc#/list-item-3)|
| Gogs             | 用户名/Token            | 用户名、Personal Token  |-|
| Gitea             | 用户名/Token            | 用户名、私人令牌  |[Gitea 官方帮助文档](https://docs.gitea.io/en-us/oauth2-provider/)|
| Gitea            | OAuth2          	| Application ID、Secret |[Gitea 官方帮助文档](https://docs.gitea.io/en-us/oauth2-provider/)|
| Docker Registry | 根据使用的认证方式设定    | -                      |-|
| Docker Hub      | 用户名/密码            | 用户名、密码             |  [Docker Hub 官方帮助文档](https://docs.docker.com/docker-hub/access-tokens/)|
| Harbor          | 用户名/密码            | 用户名、密码             |-|
| Jenkins         | 用户名/Token            | 用户名、Token            |-|
| SonarQube       | Token            | Token    |[SonarQube 官方帮助文档](https://docs.sonarqube.org/latest/user-guide/user-token/)|
| SVN             | SSH                  | SSH 私钥                |-|
|SVN				  | 用户名/Token 		    |用户名、Token             |-|
|Nexus            |用户名/密码            |用户名、密码            |-|
|JFrog Artifactory           |用户名/密码            |用户名、密码            |-|
|Maven			     |用户名/密码 		       |用户名、密码        |-|
|Jira			     |用户名/密码 		       |用户名、密码        |[Jira 官方帮助文档](https://support.atlassian.com/jira-core-cloud/docs/allow-oauth-access/)|
|TestLink			     |Token 		       |Token        |-|


