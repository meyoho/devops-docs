+++
title = "删除凭据"
description = "删除不再使用的凭据。"
weight = 5
+++

删除不再使用的凭据。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，单击 **凭据**。

2. 在凭据页面，可以通过以下两种方式删除凭据：

   * 找到要删除的凭据名称，单击 ![operations](/img/operations.png)，再单击 **删除**。
 
   * 单击要删除的 ***凭据名称***，在凭据详情页面，单击 ![operations](/img/operations.png)，再单击 **删除**。

3. 单击 **删除**。

