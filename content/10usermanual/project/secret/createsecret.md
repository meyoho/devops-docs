+++
title = "创建凭据"
description = "创建凭据，用于保存敏感信息，例如：密码、token、SSH key 等。"
weight = 1
+++

创建凭据，用于保存敏感信息，例如：密码、token、SSH key 等。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，单击 **凭据**。

2. <span id="2"></span>在凭据页面，单击 **创建凭据**。

	![createsecret](/img/usercreatesecret.png?classes=big)

	1. 在创建凭据页面的 **基本信息** 区域，填写以下信息：

  	 	- 在 **凭据名称** 框中，输入凭据的名称。  
     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线开头或结尾。

   		- (非必填) 在 **显示名称** 框中，输入凭据的显示名称，支持中文字符。如未输入，默认显示为空。  

   2. 在创建凭据页面的 **数据** 区域，选择一个 [DevOps 工具的凭据类型]({{< relref "10usermanual/admin/secret/secrettype.md" >}})：

	   	* 用户名和密码：用于验证访问 Jenkins 服务或仓库用户的身份信息。
	
	     * **用户名**：输入访问服务的用户名，支持中文用户名和英文用户名。
	
	     * **密码**：输入访问 Jenkins 的 token、代码仓库的 token、制品仓库的密码。  
	     如果此凭据用于验证 Jenkins 或仓库服务，需要填写用户名对应的 API token，获取方式各有不同。
	  
	    - OAuth2：用 OAuth 2.0 协议的客户端模式（Client Credentials）来获取 access token。此类型的 Secret 目前只支持在绑定代码仓库时使用。
	
	  	 * **Client ID**：输入创建应用后获得的 Client ID。
	  
	  	 * **Client Secret**：输入应用注册时获得的 Client Secret。 
	   
	   - SSH：ssh-auth 类型，用于存储通过 SSH 协议传输数据的认证信息。   
	   	在 **SSH 私钥** 框中，输入通过 SSH 协议传输数据的私钥。
	   
	   - 镜像服务：用于存储私有 Docker Registry 的认证信息。
	
	     * **镜像服务地址**：输入访问镜像的服务地址。
	
	     * **用户名**：输入可以访问镜像服务地址的用户名。
	
	     * **密码**：输入可以访问镜像服务地址的密码。
	
	     * **邮箱地址**：输入可以访问镜像服务地址的邮箱。
  
	    

5. ​单击 **创建**。
