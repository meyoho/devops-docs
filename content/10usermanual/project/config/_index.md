+++
title = "配置（Alpha）"
description = "配置支持配置字典 (ConfigMap) 和保密字典 (Secret)"
weight = 17
+++

配置支持配置字典 (ConfigMap) 和保密字典 (Secret)。

   * 配置字典 (ConfigMap)，用于保存配置数据的键值对，可以用来保存单个属性，也可以用来保存配置文件，可以更方便地处理不包含敏感信息的字符串。

   * 保密字典 (Secret)，用于保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全，使数据免于暴露到镜像或 Pod Spec 中。保密字典支持以下几种类型：Opaque、TLS 认证、SSH 请求认证、用户名和密码、镜像服务。

{{%children style="card" description="true" %}}





