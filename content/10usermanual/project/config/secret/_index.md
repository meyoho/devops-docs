+++
title = "保密字典"
description = "使用保密字典保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。支持用环境变量或挂载存储卷的形式引用 Secret。"
weight = 2
+++

使用保密字典保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。支持用环境变量或挂载存储卷的形式引用 Secret。

保密字典支持以下几种类型：Opaque、TLS 认证、SSH 请求认证、用户名和密码、镜像服务，其中用户名和密码、镜像服务类型可以作为凭据来使用。

* Opaque：用于存储密码、密钥等。Opaque 类型的数据是一个 map 类型的 base64 编码格式。

* TLS 认证：用于存储 TLS 协议证书等认证信息。 

* SSH 请求认证：ssh-auth 类型，用于存储通过 SSH 协议传输数据的认证信息。 

* 用户名和密码：basic-auth 类型。用于验证访问 Jenkins 服务或仓库用户的身份信息。

* 镜像服务：dockerconfigjson 类型。用于存储私有 Docker Registry 的认证信息。

{{%children style="card" description="true" %}}





