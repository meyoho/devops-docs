+++
title = "配置字典"
description = "配置字典可以更方便地处理不包含敏感信息的字符串。"
weight = 1
+++

配置字典 (ConfigMap)，用于保存配置数据的键值对，可以用来保存单个属性，也可以用来保存配置文件，可以更方便地处理不包含敏感信息的字符串。

支持环境变量引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key；同时支持挂载文件或目录引用完整的 ConfigMap 文件，或引用 ConfigMap 文件中的部分 key 作为文件。

{{%children style="card" description="true" %}}





