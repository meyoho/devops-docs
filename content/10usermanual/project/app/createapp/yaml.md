+++
title = "YAML 创建"
description = ""
weight = 2
+++

通过 YAML 编排文件来创建应用，具有便捷性、灵活性和可维护性。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，选择命名空间后，在这个项目下的命名空间内创建应用。

2. 在应用页面，单击 ![addoperation](/img/addoperation.png)，再单击 **YAML 创建**。

3. 在 YAML 创建页面的 **应用** 区域，配置应用的名称：

   * 在 **应用名称** 框中，输入应用的名称。如果 YAML 中也指定了此参数，会被覆盖，即在 **应用名称** 框中的名称会被使用。  
     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 63 个。不支持以中横线开头或结尾。

   * (非必填) 在 **显示名称** 框中，输入应用的显示名称，支持中文字符，如未输入，默认显示应用名称。
4. 在 **YAML 创建应用** 区域，在 **YAML (读写)** 框中，输入创建应用的 YAML 内容，例如：
	
	`````yaml
	apiVersion: extensions/v1beta1
	kind: Deployment
	metadata:
	  labels:
	    app: test
	  name: test-deployment
	spec:
	  replicas: 1
	  template:
	    metadata:
	      labels:
	        app: test
	    spec:
	      containers:
	      - image: index.cn/library/
	      hello-world
	        name: test-container
	        env:
	        - name: HTTP_PORT
	          value: "30603"
	`````

   - 单击 **导入**，选择已有的 YAML 文件内容作为编辑内容。

   - 单击 **导出**，将 YAML 导出保存成一个文件。

   - 单击 **恢复**，回退到上一次的编辑状态。

   - 单击 **清空**，清空所有编辑内容。

   - 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。

   - 单击 **复制**，复制编辑内容。

   - 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。

   - 单击 **全屏**，全屏查看；单击 **退出全屏**，退出全屏模式。

	- 单击 **显示更新**，更新的 YAML 内容会以不同颜色显示。
   
6. 编辑好 YAML 内容后，单击 **创建**。



