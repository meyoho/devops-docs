+++
title = "创建应用"
description = "在项目中通过 UI 界面编辑，选择计算组件镜像，并进行组件设置、容器实例设置来创建应用，便于应用的零宕机升级。"
weight = 1
+++

在项目中通过 UI 界面编辑，选择计算组件镜像，并进行组件设置、容器实例设置来创建应用，便于应用的零宕机升级。

本平台支持以下方式创建应用：

   * **创建应用**：通过选取镜像，关联凭据，设置网络和环境变量等来创建应用。参考 [创建应用]({{< relref "10usermanual/project/app/createapp/image.md" >}})。

   * **YAML 创建**：通过 YAML 编排文件来创建应用，具有便捷性、灵活性和可维护性。参考 [YAML 创建]({{< relref "10usermanual/project/app/createapp/yaml.md" >}})。

   * **工作负载创建**：通过选择该集群下已存在的工作负载创建应用。参考 [工作负载创建]({{< relref "10usermanual/project/app/createapp/workload.md" >}})。

{{%children style="card" description="true" %}}