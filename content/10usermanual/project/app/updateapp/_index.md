+++
title = "更新应用"
description = "更新已创建的应用配置。"
weight = 6
+++

更新已创建的应用配置。

支持两种方式更新应用：

* [更新]({{< relref "10usermanual/project/app/updateapp/update.md" >}})：通过表单的方式更新应用配置，其中不支持更新应用名称、组件名称、容器名称、镜像地址。

* [YAML 更新]({{< relref "10usermanual/project/app/updateapp/updateapp.md" >}})：不支持更新应用名称、组件名称、容器名称。

