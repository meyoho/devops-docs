+++
title = "应用（Alpha）"
description = "应用聚合了单一的 Kubernetes 组件（例如，服务、Deployments、StatefulSets、Ingresses）并将这些组件作为一个组来进行管理。应用提供 UI 可以实现在应用程序中聚合和显示所有组件的资源。"
weight = 15
+++

应用聚合了单一的 Kubernetes 组件（例如，服务、Deployments、StatefulSets、Ingresses）并将这些组件作为一个组来进行管理，由一个或多个关联的计算组件构成整体业务应用。

在项目中创建应用，您可通过可视化界面操作和查看应用组件资源信息方便地管理您的业务，方便实现应用组件的零宕机升级。


{{%children style="card" description="true" %}}





