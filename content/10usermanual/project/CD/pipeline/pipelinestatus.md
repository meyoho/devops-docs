+++
title = "流水线执行状态"
Description = "流水线触发执行后的状态说明。"
weight = 18
+++

流水线触发执行后的状态说明。

<style>table th:first-of-type { width: 150px;}</style>
<style>table th:nth-of-type(2) { width: 150px;}</style>
<style>table th:nth-of-type(3) { width: 400px;}</style>

| **状态** | **图示** |**状态说明** |
| :---: |:---:| :-------------------- |
| 已触发 | ![](/img/pending.png) | 表示流水线已触发执行 |
| 排队中 | ![](/img/queued.png) | 表示流水线触发后未开始执行，根据流水线的执行数量情况，会出现排队中的状态 |
| 取消中 | ![](/img/cancelled.png) | 表示流水线正在取消 |
| 执行中 | ![](/img/running.png) | 表示流水线正在执行中 |
| 执行成功 | ![](/img/complete.png) | 表示流水线执行成功 |
| 执行失败 | ![](/img/failed.png) | 表示流水线执行失败 |
| 等待输入 | ![](/img/paused.png) | 表示流水线需要等待批准 |
| 已停止 | ![](/img/stop.png) | 表示流水线已经终止 |
| 异常 | ![](/img/error.png) | 表示流水线异常 |