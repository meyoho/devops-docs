+++
title = "升级模版流水线（Alpha）"
description = "随着业务规模的增加，模版的使用场景趋向多元，当前模版可能不满足日益增长的需求，平台支持升级模版流水线。"
weight = 13
+++

随着业务规模的增加，模版的使用场景趋向多元，平台支持升级 **官方模版** 和 **自定义模版** 流水线，通过对当前模版增加参数，以提供新的能力来满足业务需求。

**提示**：

* 升级平台版本后，官方模版版本号有更新时，官方模版流水线才支持更新，请您参阅当前版本 **DevOps 发版说明** 文档查看可以升级的模版流水线。

* 若您的自定义模版想要升级，请修改代码仓库中的 Jenkinsfile 以及版本号，修改完成后，导入到平台中，即可开始升级该模版流水线。

* 若您想使用批量升级模版流水线功能，需要访问 API 来完成升级，具体参数说明请您查看 **DevOps 接口文档**。

* 若模版流水线详情页有![update](/img/update.png)图标，则代表该模版可升级，若没有此图标，则该模版流水线没有新版本，不支持升级。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **持续交付** > **流水线**。

2. 您有两种方法升级模版流水线：

	* 找到待升级的 ***模版流水线*** 名称，单击 ![operations](/img/operations.png)，再单击 **升级**。

	* 单击待升级的 ***模版流水线*** 名称，在流水线详情页面，单击 **操作** > **升级**。

3. 在 **升级** 页面，您需要参考 [官方模版]({{< relref "10usermanual/admin/pipelinetemplate/official/_index.md" >}}) 的填写表单。

	**注意：**在升级模版流水线表单中，您曾使用的流水线信息依然保留在表单中，若有新增的功能项，则为空，需要您填写；若新版本的模版流水线删除了某功能，则在升级时默认删除该功能，不展示该功能项。

4. 单击 **升级**，待平台校验完成后，完成升级操作。




   