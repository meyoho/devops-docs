+++
title = "更新流水线"
description = "更新已创建的流水线。"
weight = 14
+++

更新已创建的流水线。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **持续交付** > **流水线**。

2. 在流水线页面，可以通过以下两种方式更新流水线：

   * 找到要更新的流水线名称，单击 ![operations](/img/operations.png)，再单击 **更新**。

   * 单击要更新的 ***流水线名称***，在流水线详情页面，单击 **操作** > **更新**。

4. 在更新流水线页面：

	* 如果更新的流水线是使用脚本创建的，参考 [使用脚本创建流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createfilepipeline.md" >}})。 

	* 如果更新的流水线是使用模版创建的，参考 [使用模版创建流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createtemplate.md" >}})。 
	
	* 如果更新的流水线是多分支流水线，参考 [创建多分支流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createmultibranch/_index.md" >}})。

	* 如果更新的流水线是图形化流水线，参考 [创建图形化流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createpatternpipeline.md" >}})。 
	
7. 单击 **更新**。