+++
title = "等待输入（Alpha）"
Description = "流水线支持人工干预功能，在流水线中，等待输入能够暂停流水线的执行，等待人工响应。"
weight = 5
+++

流水线支持人工干预功能，在流水线中，**等待输入** 能够暂停流水线的执行，等待人工响应。

* **等待输入** 支持流水线 **通过**，**终止** 功能，若您单击 **通过**，流水线继续执行；若您点击 **终止**，流水线执行终止。

* **等待输入** 支持对话框，您在选择 **通过**、**终止** 的同时，还可以增加一些输入参数，支持类型为：**String**。

**提示**：本平台现在没有关于等待输入的官方模版，如需使用等待输入功能，您可以通过以下两种方式快速使用等待输入功能：

* 参考下面的 Jenkinsfile 代码样例，在 [使用脚本创建流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createfilepipeline.md" >}}) 中，使用 **页面编写** 的方式，快速创建使用等待输入功能的流水线。

* 参考下面的 Jenkinsfile 代码样例，编写自定义模版并放到代码仓库中，通过 [配置模板仓库]({{< relref "10usermanual/admin/project/template/setuppipelinerepo.md" >}}) 导入到平台，[使用模版创建流水线]({{< relref "10usermanual/project/CD/pipeline/createpipeline/createtemplate.md" >}}) 选择您自定义的模版，使用等待输入功能。

**操作步骤**
	
1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，进入项目后，单击 **持续交付** > **流水线** 。
	
2. 在顶部导航栏单击 **等待输入**，在等待输入的详情页面可以查看流水线的执行 ID、流水线名称、执行状态、开始时间、耗时、触发方式和操作。

3. 若您在流水线的 `Jenkinsfile` 中配置了等待输入的相关代码，则会在等待输入页面展示等待输入的流水线，单击页面右侧的 **输入** 按钮，您可以输入相关信息，对流水线进行通过和终止操作，如下图：

	![waitinput](/img/waitinput.png?classes=big)

	* 单阶段输入：当您的 Jenkinsfile 文件中配置了一个等待输入，单击输入按钮，在弹出的框中确认信息并执行操作。  
	单阶段输入 Jenkinsfile 代码样例如下，更多参数详见 [Jenkins 官方文档](https://jenkins.io/zh/doc/book/pipeline/syntax/#input )：
	
		```
		pipeline {
		
			  agent any
			  
			  stages{
			    stage('simple'){
			      steps{
			        input message: 'Please input your name!!', ok: 'Confirm',
			          parameters: [
			          	string(
			          		defaultValue: 'Bob',
			          		description: 'This is your real name.', 
			          		name: 'name', 
			          		trim: true)]
			      }
			    }
			  }
			}
		
		```
	
	* 多阶段输入：当您的 Jenkinsfile 文件中配置了多个等待输入，单击输入按钮，选择想要输入的 stage 后，在弹出的框中确认信息并执行操作。  
	多阶段输入 Jenkinsfile 代码样例如下，更多参数详见 [Jenkins 官方文档](https://jenkins.io/zh/doc/book/pipeline/syntax/#input )：

		```
		pipeline {
		
			  agent any
			  
			  stages{
			    
			    stage('complex'){
			      parallel {
			        stage('channel-1'){
			          steps{
			            input message: 'Please input your age!', ok: 'Confirm',
			              parameters:[
				              string(
				              	defaultValue: '18',
				              	description: 'Just a sample.', 
				              	name: 'age', 
				              	trim: true)]
			          }
			        }
			        stage('channel-2'){
			          steps{
			          
			            input message: 'Please input your weight!', ok: 'Confirm',
			              parameters: [
			              	string(
				              	defaultValue: '50',
				              	description: 'Just a sample.', 
				              	name: 'weight', 
				              	trim: true)]
				       }
			        }
			      }
			    }
			  }
			}
		```

3. 单击 **通过** 或者 **终止** 完成操作。

	