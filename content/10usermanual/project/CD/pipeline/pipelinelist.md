+++
title = "查看流水线列表"
description = "查看流水线列表，例如：流水线名称、执行记录、触发器等。"
weight = 2
+++

查看流水线列表，例如：流水线名称、执行记录、触发器等。

**操作步骤**

1. 登录 DevOps 平台，切换至业务视图，在项目列表页面，单击待查看流水线所在的 ***项目名称***。

3. 在左侧导航栏依次单击 **持续交付** > **流水线**，进入流水线列表页面。

4. 在流水线列表页面，支持查看流水线的基本信息，如图所示：

	![pipelinedetail1](/img/viewpipeline.png?classes=big)
	
	* **名称**：显示流水线的名称，若创建流水线时配置了显示名称，则显示名称在流水线名称的下方显示，同时支持查看流水线类型和流水线状态：  
		
		* 流水线类型：![pipelinescript](/img/pipelinescript.png) 表示流水线使用脚本的方式创建；![pipelinetemplate](/img/pipelinetemplate.png) 表示流水线使用模版的方式创建；![multibranchpipe](/img/multibranchpipe.png) 表示流水线是多分支流水线；![picturepipe](/img/picturepipe.png) 表示流水线是图形化方式创建。

		* 流水线状态：![pipelineerror](/img/pipelineerror.png) 表示流水线异常，异常信息请将鼠标移至图标上查看；![pipelinerunning](/img/pipelinerunning.png) 表示流水线正在与 Jenkins 同步信息；![pipelinestop](/img/pipelinestop.png)表示流水线已经被禁止触发。
	
	* **执行记录**：显示流水线的最近一条执行记录的执行 ID、执行时间和执行状态。
	
		* 如果流水线已执行超过一次，移动鼠标到 ***最近 x 条*** 中的数字上，单击即弹出最近执行记录的信息，最多五条。执行信息包括执行 ID、执行时间和执行状态，查看流水线执行状态请参考 [流水线执行状态]({{< relref "10usermanual/project/CD/pipeline/pipelinestatus.md" >}})。  
		
			**提示**：**多分支流水线** 还会显示执行流水线的代码分支，单击分支进入执行记录详情页。
	
		* 单击各条执行记录的 ***执行 ID***，例如 **#1**，跳转到该条执行记录的详情页，参考 [查看执行记录详情]({{< relref "10usermanual/project/CD/pipeline/viewpipeline.md#2" >}})；

		* 单击执行日期右侧的![log](/img/log.png)图标，可查看该条执行记录的 [概览日志]({{< relref "10usermanual/project/CD/pipeline/viewpipeline.md#1" >}})。

		* （当流水线配置了代码扫描时）单击 ***执行 ID*** 附近的![sonaricon](/img/sonaricon.png) 图标，跳转到 SonarQube 的详情页。

	* **触发器**：显示流水线的触发器，![code](/img/codedark.png) 图标代表代码仓库触发器；![time](/img/timedark.png) 图标代表定时触发器。

		**注意**：
		
		* 若流水线未配置触发器时，默认为`-`。

		* 若流水线配置了触发器且触发器图标为灰色时，请查看该流水线是否被禁用。
		
	* 单击 ![operations](/img/operations.png)，可对流水线进行执行、更新、删除等操作，说明如下：

		* **执行**：手动运行流水线。参考 [执行流水线]({{< relref "10usermanual/project/CD/pipeline/startpipeline.md" >}})。

			**注意**：执行流水线只支持使用 **脚本创建** 的流水线、**图形化创建** 的流水线和 **模版创建** 的流水线。

		* **扫描**：扫描指定的代码分支后，触发流水线。参考 [扫描并触发流水线]({{< relref "10usermanual/project/CD/pipeline/scan.md" >}})。

			**注意**：扫描并触发流水线只支持 **多分支流水线**。

		* **更新**：更新已创建的流水线，重新配置流水线以适应新的场景。参考 [更新流水线]({{< relref "10usermanual/project/CD/pipeline/update.md" >}})。

		* **复制**：复制已创建的流水线配置，并通过创建流水线流程，生成一条新的流水线。参考 [复制流水线]({{< relref "10usermanual/project/CD/pipeline/copypipeline.md" >}})。

		* **升级**：若官方模版的版本有更新，可对指定模版流水线进行升级，使用新模版的功能。参考 [升级模版流水线]({{< relref "10usermanual/project/CD/pipeline/updatetemplate.md" >}})。

		* **删除**：删除已创建的流水线。参考 [删除流水线]({{< relref "10usermanual/project/CD/pipeline/deletepipeline.md" >}})。


	
