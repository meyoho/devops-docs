+++
title = "GitLab 企业版支持多分支相关配置"
description = "配置 GitLab 企业版以支持发现 MR（Merge requests），从而实现使用 Web Hook 触发流水线的功能。"
weight = 1
+++

配置 GitLab 企业版以支持发现 MR（Merge requests），从而实现使用 Web Hook 触发流水线等功能。

**注意：**

* 目前只是 **GitLab 企业版** 支持通过配置发现 MR，**GitLab 公有版** 暂时不支持发现 MR。

* 同一个 Jenkins 可以配置多个 GitLab，多个 Jenkins 支持配置同一个 GitLab。

**前提条件**：

1. 您对接平台中的 GitLab 版本高于 11.0、Jenkins 版本高于 176.2 LTS（**本平台上的工具版本已满足要求**）。

2. Web Hook 默认是不允许使用内网地址，对于私有环境使用 Web Hook 触发流水线，需要在 **gitlab** > **Settings** > **network** > **outbound request** 中，勾选 **allow requests to the local network from hooks and services** 选项。

**操作步骤**：

1. 您可以通过以下两种方法来配置 GitLab 企业版：

	* 方法一：配置 Jenkins chart。使用 Chart 进行 GitLab 企业版服务的配置，添加相关参数。在安装 Jenkins 的时候，需要添加进 gitlabConfigs 的配置和 Jenkins 地址的配置，配置如下：

		```
		Master:
		  gitlabConfigs:
		    - name: <GitLab服务的名字>
		      manageHooks: true
		      serverUrl: <GitLab服务的地址>
		      token: <GitLab凭据token>
		  Location:
    		url: <Jenkins Location URL>
		```
		
		![gitlab-enterprise](/img/gitlab-enterprise.png?classes=big)
		
		* gitlabConfigs：
			
			* name：GitLab 服务的名字，需要与平台上集成工具时的名称对应，如上图![1](/img/1.png)位置的集成服务名称。
				
			* manageHooks：管理 Hooks，配置 Web Hook 可以使平台发现 MR，实现代码提交触发流水线等功能，您需要将此参数设置为 true。
					
			* serverUrl：GitLab 服务器地址，需要与平台上集成工具时的 API 地址对应，如上图![2](/img/2.png)位置的 API 地址。
					
			* token：GitLab 的 Personal Access Token。获取令牌需要在 GitLab 我的头像 > Settings > Access Tokens > Add a personal access token，添加完成后会得到 GitLab 的令牌。

		* Location：

			* url：Jenkins 的本地环境地址，例如 `http://10.0.128.54:32001/`。

 
	* 方法二：在已部署的 Jenkins 本地环境界面中，单击 **系统管理** > **系统设置** ，找到 **配置 GitLab 服务**，相关配置如下：

		**提示**：您对接平台中的 Jenkins 需要提前安装 gitlab-branch-source-plugin 插件（在您使用本平台的安装包部署 Jenkins 时已经默认安装此插件）。
		
		**注意**：11.4.0 版本的 gitlab-branch-source-plugin 插件使用时会对 Jenkins Location URL 进行判断，如当前默认值为 `http://jenkins:8080/`时会出现流水线无法同步成功的情况，所以在配置的时候，需要同时修改在系统设置中 Jenkins Location URL 的地址，例如修改为 `http://10.0.128.54:32001/`。
				
	    ![gitlab-setting](/img/gitlab-setting.png?classes=big)
	    
	    ![gitlab-enterprise](/img/gitlab-enterprise.png?classes=big)
		
		* 显示名称：填写集成工具名称，需要与平台上集成工具时的名称对应，如上图![1](/img/1.png)位置的集成工具名称。
		
		* 服务 URL：填写集成 GitLab 服务的地址，需要与平台上集成工具时的 API 地址对应，如上图![2](/img/2.png)位置的 API 地址。
		
		* 凭据：在 Jenkins 上创建 GitLab Token 类型，填写 GitLab 生成的 Personal Token。若没有可供选择的凭据，单击添加，在添加凭据框中添加 GitLab 凭据。
		
			![addgitlabsecret](/img/addgitlabsecret.png?classes=big)
	
			* Domain：凭据适用域。默认使用全局凭据。
	
			* 类型：凭据类型，暂时只支持使用 **GitLab 个人令牌** 类型。
	
				* 范围：凭据作用范围。默认使用全局范围。
	
				* 令牌：GitLab 的 Personal Access Token。获取令牌需要在 GitLab 我的头像 > Settings > Access Tokens > Add a personal access token，添加完成后会得到 GitLab 的令牌。
	
				* ID：用于标识凭据的唯一 ID，为空时，会自动生成一个 ID。
				
				* 描述：添加凭据描述。
			
		* Web Hook：勾选此选项以使用 Web Hook 触发多分支流水线。配置 Web Hook 可以使平台发现 MR，实现代码提交触发流水线等功能。
	
		* 系统 Hook：当需要 GitLab 系统触发的事件进行通知时，您可以勾选此选项。

4. 配置完成，您可以在项目中绑定 GitLab 企业版，并在业务视图中创建多分支流水线时使用 GitLab 仓库，实现发现 MR，使用 Web Hook 触发流水线等功能。


