+++
title = "构建模版"
description = "通过创建构建可以选择不同构建方式和编程语言的模版。"
weight = 2
+++

通过创建构建您可以选择不同构建方式和编程语言的模版创建流水线。

**操作步骤**
	
1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***。

2. 进入项目后，单击 **持续交付** > **构建**。

3. 单击 **创建构建**，在 **模板** 步骤选择要使用的构建模版并单击选择，创建构建模版参考如下： 

* [Golang 构建]({{< relref "10usermanual/project/CD/build/createbuild/golangbuild.md" >}})

* [Golang 构建并部署新应用]({{< relref "10usermanual/project/CD/build/createbuild/golangupdateapp.md" >}})

* [JMeter 自动化测试]({{< relref "10usermanual/project/CD/build/createbuild/jmeter.md" >}})

* [镜像构建]({{< relref "10usermanual/project/CD/build/createbuild/imagebuild.md" >}})

* [Java 构建]({{< relref "10usermanual/project/CD/build/createbuild/Javabuild.md" >}})

* [Java 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Javaupdateapp.md" >}})

* [Nodejs 10 构建]({{< relref "10usermanual/project/CD/build/createbuild/Nodejs10build.md" >}})

* [Nodejs 10 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Nodejs10updateapp.md" >}})

* [Python 2.7 构建]({{< relref "10usermanual/project/CD/build/createbuild/Python2.7build.md" >}})
 
* [Python 2.7 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Python2.7updateapp.md" >}})

* [Python 3.6 构建]({{< relref "10usermanual/project/CD/build/createbuild/Python3.6build.md" >}})

* [Python 3.6 构建并部署应用]({{< relref "10usermanual/project/CD/build/createbuild/Python3.6updateapp.md" >}})
 
* [Maven 构建（Alpha）]({{< relref "10usermanual/project/CD/build/createbuild/mavenbuild.md" >}})

* [Maven 构建并分发到仓库（Alpha）]({{< relref "10usermanual/project/CD/build/createbuild/mavenupdateapp.md" >}})




