+++
title = "同步镜像"
Description = "使用同步镜像模版创建流水线。"
weight = 12
+++

使用 **同步镜像** 模版创建流水线，同步镜像模板。

**模板描述**：同步镜像。  
将一个镜像从源仓库同步到目标仓库。若源镜像或目标镜像仓库为私有仓库（内网地址），需确保 Jenkins 服务与其在同一网络环境内，方可使用。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，进入项目后，单击 **持续交付** > **构建** > **创建构建**。
	
2. 在 **选择模版** 步骤，选择官方模版创建流水线：  
	
	* 默认会显示所有构建的流水线模版，并根据模版的标签分组展示，支持分组查找模版。每个模版显示了模版名称、版本号、标签分类、模版包含的流水线任务名称。
		
	* 单击每个 ***模版名称*** 的 **详情**，在模版的新窗口中，查看模版详情。模版详情包括基本信息和模版的具体参数和描述，方便配置模版参数。
	
	在想要选择的流水线模版右侧单击 **选择** 按钮。		
4. <span id="4"></span>在 **基本信息** 步骤，配置流水线的基本信息： 
	
	* 在 **名称** 框中，输入 Jenkins 流水线的名称。  
	名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线或数字开头且不支持以中横线为结尾。
		
	* (非必填) 在 **显示名称** 框中，输入 Jenkins 流水线的显示名称，支持中文字符。如未输入，默认显示为空。
	
	* 在 **Jenkins 实例** 框中，选择一个已创建的 Jenkins 实例。
	
	* **执行方式**：选择流水线的执行方式，支持 **依次执行** 和 **并发执行**：
	
		* **依次执行**：流水线被触发后，在没有结束执行前，如果再次触发，后一次触发的流水线会变成等待状态。
	
		* **并发执行**：流水线被触发后，在没有结束执行前，如果再次触发，两次触发的流水线会同时执行。

	* （Alpha）**禁用流水线** ：选择是否打开禁用流水线开关，默认关闭。若开启，此流水线不可以被触发。

		**提示**：流水线正在执行时，若更新执行中的流水线并打开禁用流水线开关，运行中的流水线会正常执行，执行结束后禁用流水线功能生效。
	
	配置完成后，单击 **下一步**。 
	
4. 在 **模版参数设置** 步骤，配置模版参数。不同模版类型的配置信息不同，可以查看模版详情了解每个参数的定义。
	
	* 单击页面右侧的 **流水线全局配置** ，您可以配置流水线模版的构建节点。

		* 在 **构建节点** 区域，您可以选择使用 **标签** 方式或 **自定义** 方式来指定流水线在哪个节点运行。

			* **标签**：您可以选择或者输入 Jenkins 上已经定义的标签，例如：tools。

			* **自定义**：您可以选择自定义的方式来指定运行的节点，例如：``agent {label ‘${label-select-by-user}’}``。
		
		* **配置项**：您可以在此填写流水线构建的配置。例如：``buildDiscarder(logRotator(numToKeepStr: '200'))``。更多流水线配置项语法请参考 [Jenkins 官方文档](https://jenkins.io/doc/book/pipeline/syntax/#options)。

	* 在 **源镜像信息** 区域，填写源镜像仓库地址和凭据信息。

		* **源镜像仓库地址**：输入要使用的源镜像仓库地址。  
		 			
	   * （非必填）**源镜像凭据**：如果访问的镜像需要验证，您可以在下拉框中选择使用镜像仓库的凭据，若没有可供使用的凭据，可以添加凭据，请参考 [创建保密字典]({{< relref "10usermanual/project/config/secret/create.md" >}}) 中的 **镜像服务** 类型。    
	   			
	   * **源镜像标签**：输入源镜像的标签，默认为${imageTag}。  
	   	
	* 在 **目标镜像信息** 区域，填写目标镜像仓库地址和凭据信息。  
	   	
	  * **目标镜像仓库地址**：输入要使用的目标镜像仓库地址。
	  
	  * （非必填）**目标镜像凭据** ：如果访问的镜像需要验证，您可以在下拉框中选择使用镜像仓库的凭据，若没有可供使用的凭据，可以添加凭据，请参考 [创建保密字典]({{< relref "10usermanual/project/config/secret/create.md" >}}) 中的 **镜像服务** 类型。    
	   			
	   	* **目标镜像标签**：输入源镜像的标签，默认为${imageTag}。  

	* **（Alpha）** 在 **通知** 区域，根据您的需要决定是否打开通知功能，若开启，可选择已创建好的通知，当流水线执行完成后，会根据通知设置发送通知。

		* **发送通知**：选择已创建好的通知（由管理员创建），流水线将根据已选择的通知对象，对流水线进行通知。

				
		配置完成后，单击 **下一步**。
	
4. 在 **触发器** 步骤，根据实际策略，设置定时触发器。配置相应的触发规则，可实现流水线的自动触发。
		
	* **定时触发器**：在指定的时间触发流水线，选择 **定时触发器** 后，在 **触发器规则** 区域，选择星期中的某一天或某几天，再单击 **配置时间**，选择某个特定时间来触发流水线，支持添加多个时间点的触发。单击 **自定义**，输入自定义触发器规则。  
		
	自定义触发器规则的语法说明，参考 [触发器规则]({{< relref "10usermanual/project/CD/pipeline/timetrigger.md" >}})。
			
	配置完成后，如果需要预览 Jenkinsfile，单击 **预览 Jenkinsfile**，在新窗口中查看详情。确认配置后，单击 **创建**。  
	流水线创建完成后，跳转到流水线详情页面，参考 [查看流水线及详情]({{< relref "10usermanual/project/CD/pipeline/viewpipeline.md" >}})。
