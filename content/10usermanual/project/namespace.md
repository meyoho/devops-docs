+++
title = "命名空间（Alpha）"
description = "您可以通过选择 Namespace 命名空间，查看该命名空间下的应用、配置和存储信息。"
weight = 13
+++

您可以通过选择 Namespace 命名空间，查看该命名空间下的应用、配置和存储信息。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，单击左侧导航栏	![overview](/img/namespace.png) 。

2. 单击想要进入 **集群** 下的某个 ***命名空间***，即可进入命名空间内查看相关应用，配置和存储信息。 


**注意：**命名空间只能由平台管理员创建且需要平台管理员导入命名空间成员后，该成员才能进入相应的命名空间。

	