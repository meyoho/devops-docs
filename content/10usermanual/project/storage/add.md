+++
title = "添加存储"
description = "添加持久卷声明，请求使用特定大小和访问模式的持久卷。"
weight = 1
+++

添加持久卷声明，请求使用特定大小和访问模式的持久卷。PersistentVolumeClaim (PVC，持久卷声明) 是对 PersistentVolume (PV，持久卷) 的使用请求，请求特定大小和访问模式的 PV。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，选择命名空间后，再单击 **存储**。

2. 在存储列表页面，单击 **添加存储**。

3. 在添加存储页面，填写以下信息：

	![storagecreate](/img/storagecreate.png?classes=big)
	
	* 在 **名称** 框中，输入存储的名称。  
名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 63 个。不支持以中横线开头或结尾。

	* (非必填) 在 **显示名称** 框中，输入存储的显示名称，支持中文字符。如未输入，默认显示为空。
	
	* 在 **存储类** 区域，选择一个已存在的存储类，系统将根据存储类的设置，绑定适合的 PV。  
	存储类需要提前创建好。支持创建 Ceph 存储类的方案。
	
	* 在 **访问模式** 区域，选择持久卷声明（PVC）的访问模式。
	
		* **读写**：即 ReadWriteOnce（RWO），请求的 PVC 只能在单个节点上以读写模式挂载。
		
		* **只读**：即 ReadOnlyMany（ROX），请求的 PVC 可以在多个节点上以只读模式挂载。
		
		* **共享**：即 ReadWriteMany（RWX），请求的 PVC 可以在多个节点上以读写模式挂载。
	
	* 在 **大小** 框中，输入 PVC 请求的存储大小（Request），支持不同单位。
		
4. 单击 **创建**。