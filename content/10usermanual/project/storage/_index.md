+++
title = "存储（Alpha）"
description = "存储提供添加持久卷声明，请求使用特定大小和访问模式的持久卷"
weight = 19
+++

存储提供添加持久卷声明，请求使用特定大小和访问模式的持久卷。

PersistentVolumeClaim (PVC，持久卷声明) 是对 PersistentVolume (PV，持久卷) 的使用请求，请求特定大小和访问模式的 PV。

PV 和 PVC 将 Pod 以及存储卷解耦，提供了方便的持久化存储，PV 提供存储资源，PVC 请求访问存储资源。

{{%children style="card" description="true" %}}





