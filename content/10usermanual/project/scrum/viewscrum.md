+++
title = "查看 Issues 列表"
description = "查看分配到项目下的敏捷项目管理工具的 Issues 列表信息"
weight = 1
+++

查看分配到项目下的敏捷项目管理工具的 Issues 列表信息。

**提示**：

如果有多个 Jira 项目，请在顶部导航栏处切换项目。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **敏捷项目管理** > **Issues 列表**。

2. 在 **Issues 列表** 页面，您可以通过过滤条件或者搜索来查看项目相关信息，支持过滤 **所属项目**、**类型**、**优先级**、**状态**，并支持搜索关键字 **主题** 和 **ID** ，如下图所示：

	![scrum](/img/scrum.png?classes=big)

   * **ID**：显示 Issue ID。单击 ![thirdparty](/img/thirdparty.png)，跳转到 Jira 地址，查看 Issue 详情。

   * **主题**：显示 Issue 的主题。单击主题，进入到主题的详情页面：

   		* **基础属性**：显示 Issue 的基础属性，包括所属项目、创建时间、类型、更新时间、优先级、创建人、状态、指派给。

   		* **描述**：显示了对该 Issue 的具体描述。

   		* **链接**：显示了该关联 Issue 的链接。
   		
   		* **评论**：显示了该 Issue 的相关评论。
 
   * **所属项目**：显示了该 Issue 属于哪个项目。

  * **类型**：显示了该 Issue 的类型，例如：故事。

  * **优先级**：显示了该 Issue 的优先级，例如：Low。

  * **状态**：显示了该 Issue 的处理状态，例如：代办。

  * **指派给**：显示了该 Issue 的指派人。

  * **更新时间**：显示了该 Issue 的更新时间。

  * **创建人**：显示了该 Issue 的创建人。
 


