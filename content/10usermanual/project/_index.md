+++
title = "业务视图"
description = ""
weight = 4
alwaysopen = false
+++

## 简介

业务视图专注于项目的具体业务，例如创建流水线、查看代码仓库、查看制品仓库等。

用户登录进入平台后，默认进入业务视图。在业务视图中，用户需要选择一个项目，进入指定项目才可进行作业生产。若用户非首次登录，则会默认进入上次退出时的项目。

![businessview](/img/businessview.png?classes=big)

### 切换项目和命名空间 

**注意**：由于左侧导航栏中的 **敏捷项目管理**、**命名空间**、**应用**、**配置**、**存储** 属于 Alpha 功能，您暂时不可见，如需打开，请在阅读本产品的 [功能成熟度]({{< relref "10usermanual/_index.md#1" >}}) 后，联系工作人员开启。

![businessview](/img/businessviewswitch.png?classes=big)

{{%children style="card" description="true" %}}



