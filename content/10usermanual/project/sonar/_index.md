+++
title = "代码质量分析"
description = "在代码仓库中查看已绑定的的代码仓库信息。"
weight = 9
+++

查看已扫描的的代码信息，例如：扫描的代码仓库名称、质量阈结果、缺陷和漏洞的级别等。

{{%children style="card" description="true" %}}





