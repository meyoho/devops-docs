+++
title = "查看代码质量分析"
description = "查看已扫描的的代码信息，例如：扫描的代码仓库名称、质量阈结果、缺陷和漏洞的级别等。"
weight = 3
+++

查看已扫描的的代码信息，例如：扫描的代码仓库名称、质量阈结果、缺陷和漏洞的级别等。

代码质量数据的生成条件：要求流水线配置代码扫描。当扫描成功后，会自动生成扫描结果。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **代码质量分析**。

2. 在代码质量分析页面，查看已扫描的的代码信息。支持按时间进行排序，并显示最新一次扫描的时间。
	
	![sonar](/img/sonar.png?classes=big)

	1. 扫描的代码仓库名称
	
	2. 质量阈状态：通过、失败、警告
	
	3. 跳转至 SonarQube：单击每条扫描过的代码信息后的 ![thirdparty](/img/thirdparty.png)，跳转到 SonarQube 服务此项目的详情页。例如：
		
		![sonardetail](/img/sonardetail.png?classes=big)
	
	4. 代码质量信息：
		* 缺陷（Bug）：代码中存在明显错误，需要立即修复。显示了缺陷的数量和评级（A\B\C\D\E）。   
		A/B/C/D/E 评级说明：A=0-0.05，即 <=5%；B=0.06-0.1，即在 6~10% 之间；C=0.11-0.20，即在 11~20% 之间；D=0.21-0.5，即在 21~50% 之间；E=0.51-1，即 >50%。
		
		* 漏洞（Vulnerability）：代码中出现容易被黑客利用的潜在安全风险点。显示了漏洞的数量和评级（A\B\C\D\E）。   
		A/B/C/D/E 评级说明：A=0-0.05，即 <=5%；B=0.06-0.1，即在 6~10% 之间；C=0.11-0.20，即在 11~20% 之间；D=0.21-0.5，即在 21~50% 之间；E=0.51-1，即 >50%。
		
		* 代码异味（Code Smell）：代码中与可维护性相关的问题，这使维护代码难度增加，并降低开发效率，并极易在更改代码时引入其他错误。显示了代码异味的数量和评级（A\B\C\D\E）。   
		A/B/C/D/E 评级说明：A=0-0.05，即 <=5%；B=0.06-0.1，即在 6~10% 之间；C=0.11-0.20，即在 11~20% 之间；D=0.21-0.5，即在 21~50% 之间；E=0.51-1，即 >50%。
		
		* 覆盖率（Coverage）：代码覆盖率，显示范围为：<30%、30%~50%、50%~70%、70%~80%、>80%。
		
		* 重复率（Duplication）：代码重复率。显示范围为：<3%、3%~5%、5%~10%、10%~20%、>20%。
		
		* 代码语言、Size（类的大小）、代码行数。
