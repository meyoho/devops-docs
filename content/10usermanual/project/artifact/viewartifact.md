+++
title = "查看制品仓库"
description = "查看已绑定的的制品仓库信息，例如：Docker Registry 或 Harbor Registry 的仓库地址、版本信息等。"
weight = 1
+++

查看已绑定的的制品仓库信息，例如：Docker Registry 或 Harbor Registry 的仓库地址、版本信息等。

**操作步骤**

1. 登录 DevOps 平台，进入业务视图后，在项目列表页面，单击一个 ***项目名称***，再单击 **制品仓库**。

2. 在制品仓库页面，查看已绑定的仓库信息，例如在 **镜像仓库** 栏，以列表的形式展示了已创建的镜像仓库。

	* Harbor Registry 支持跳转到 Harbor 页面，查看详情。单击 ![thirdparty](/img/thirdparty.png) 跳转。

3. 单击仓库地址，查看详情：

	![artifactrepo](/img/artifactrepo.png?classes=big)
	
	* Docker Registry、Docker Hub Registry：显示了绑定的仓库地址和使用镜像的版本总数，并列举了每个镜像版本和摘要的信息，例如 tag、更新时间。摘要即 Digest，是镜像的 manifes 文件的 sha256 码，作为镜像的唯一标识。镜像 tag 和摘要也支持复制。

	* Harbor Registry：

		* 基本信息：显示了绑定的仓库地址和使用镜像的版本总数，并列举了每个镜像的版本、大小、摘要的信息，例如 tag、更新时间。摘要即 Digest，是镜像的 manifes 文件的 sha256 码，作为镜像的唯一标识。镜像 tag 和摘要也支持复制。

		* 扫描信息：显示了镜像的扫描状态，并支持扫描操作。

