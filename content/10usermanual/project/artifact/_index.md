+++
title = "制品仓库"
description = "在项目中，查看已绑定的制品仓库详情，并扫描镜像。"
weight =7
+++

制品仓库支持：

* 查看已绑定的制品仓库详情，例如：Docker Registry、Harbor Registry、Docker Hub Registry 的仓库地址、版本信息等。

* 扫描镜像。

{{%children style="card" description="true" %}}





