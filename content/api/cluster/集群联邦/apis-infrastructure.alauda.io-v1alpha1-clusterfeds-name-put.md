+++
title = "更新联邦集群"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name} PUT"
weight = 40
path = "PUT /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}" verb="PUT" %}}
