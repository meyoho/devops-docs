+++
title = "删除指定的联邦集群"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name} DELETE"
weight = 150
path = "DELETE /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}" verb="DELETE" %}}
