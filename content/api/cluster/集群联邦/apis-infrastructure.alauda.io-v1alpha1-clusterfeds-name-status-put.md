+++
title = "更新联邦集群的状态"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status PUT"
weight = 70
path = "PUT /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status" verb="PUT" %}}
