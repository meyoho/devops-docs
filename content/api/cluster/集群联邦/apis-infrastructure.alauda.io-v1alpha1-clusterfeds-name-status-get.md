+++
title = "查看联邦集群的状态"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status GET"
weight = 60
path = "GET /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}/status" verb="GET" %}}
