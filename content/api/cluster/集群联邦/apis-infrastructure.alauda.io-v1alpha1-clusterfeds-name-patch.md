+++
title = "差异化更新联邦集群"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name} PATCH"
weight = 50
path = "PATCH /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}" verb="PATCH" %}}
