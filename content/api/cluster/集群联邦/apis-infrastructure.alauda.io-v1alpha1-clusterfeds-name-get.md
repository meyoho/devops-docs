+++
title = "查看联邦集群详情"
description = "/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name} GET"
weight = 30
path = "GET /apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}"
+++


{{%api path="/apis/infrastructure.alauda.io/v1alpha1/clusterfeds/{name}" verb="GET" %}}
