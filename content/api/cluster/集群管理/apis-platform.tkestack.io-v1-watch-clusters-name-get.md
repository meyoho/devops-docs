+++
title = "监视指定的集群"
description = "/apis/platform.tkestack.io/v1/watch/clusters/{name} GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/clusters/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/clusters/{name}" verb="GET" %}}
