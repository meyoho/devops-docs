+++
title = "查看集群列表"
description = "/apis/platform.tkestack.io/v1/clusters GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/clusters"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters" verb="GET" %}}
