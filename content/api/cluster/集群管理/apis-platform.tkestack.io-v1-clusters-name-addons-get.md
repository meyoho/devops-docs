+++
title = "查看指定集群的扩展组件列表"
description = "/apis/platform.tkestack.io/v1/clusters/{name}/addons GET"
weight = 210
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}/addons"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}/addons" verb="GET" %}}
