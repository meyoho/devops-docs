+++
title = "查看集群详情"
description = "/apis/platform.tkestack.io/v1/clusters/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/clusters/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}" verb="GET" %}}
