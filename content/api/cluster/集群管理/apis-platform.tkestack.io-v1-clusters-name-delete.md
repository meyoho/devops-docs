+++
title = "删除指定的集群"
description = "/apis/platform.tkestack.io/v1/clusters/{name} DELETE"
weight = 888
path = "DELETE /apis/platform.tkestack.io/v1/clusters/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters/{name}" verb="DELETE" %}}
