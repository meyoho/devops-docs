+++
title = "批量删除集群"
description = "/apis/platform.tkestack.io/v1/clusters DELETE"
weight = 10000
path = "DELETE /apis/platform.tkestack.io/v1/clusters"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusters" verb="DELETE" %}}
