+++
title = "监视集群列表"
description = "/apis/platform.tkestack.io/v1/watch/clusters GET"
weight = 90
path = "GET /apis/platform.tkestack.io/v1/watch/clusters"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/clusters" verb="GET" %}}
