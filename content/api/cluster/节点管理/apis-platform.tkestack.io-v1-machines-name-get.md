+++
title = "查看主机详情"
description = "/apis/platform.tkestack.io/v1/machines/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/machines/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}" verb="GET" %}}
