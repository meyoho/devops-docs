+++
title = "监视指定的主机"
description = "/apis/platform.tkestack.io/v1/watch/machines/{name} GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/machines/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/machines/{name}" verb="GET" %}}
