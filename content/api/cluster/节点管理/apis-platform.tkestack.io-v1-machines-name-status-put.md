+++
title = "更新主机的状态"
description = "/apis/platform.tkestack.io/v1/machines/{name}/status PUT"
weight = 70
path = "PUT /apis/platform.tkestack.io/v1/machines/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/status" verb="PUT" %}}
