+++
title = "监视主机列表"
description = "/apis/platform.tkestack.io/v1/watch/machines GET"
weight = 90
path = "GET /apis/platform.tkestack.io/v1/watch/machines"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/machines" verb="GET" %}}
