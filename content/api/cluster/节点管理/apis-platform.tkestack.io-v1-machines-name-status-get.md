+++
title = "查看主机的状态"
description = "/apis/platform.tkestack.io/v1/machines/{name}/status GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/machines/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/status" verb="GET" %}}
