+++
title = "差异化更新主机的终结器"
description = "/apis/platform.tkestack.io/v1/machines/{name}/finalize PATCH"
weight = 130
path = "PATCH /apis/platform.tkestack.io/v1/machines/{name}/finalize"
+++


{{%api path="/apis/platform.tkestack.io/v1/machines/{name}/finalize" verb="PATCH" %}}
