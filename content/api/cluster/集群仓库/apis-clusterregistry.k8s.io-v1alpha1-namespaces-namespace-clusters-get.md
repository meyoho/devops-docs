+++
title = "查看指定命名空间下的集群列表"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters GET"
weight = 3
path = "GET /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters" verb="GET" %}}
