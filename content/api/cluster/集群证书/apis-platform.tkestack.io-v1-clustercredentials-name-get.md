+++
title = "查看集群凭证详情"
description = "/apis/platform.tkestack.io/v1/clustercredentials/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/clustercredentials/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials/{name}" verb="GET" %}}
