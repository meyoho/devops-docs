+++
title = "删除指定的集群凭证"
description = "/apis/platform.tkestack.io/v1/clustercredentials/{name} DELETE"
weight = 80
path = "DELETE /apis/platform.tkestack.io/v1/clustercredentials/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials/{name}" verb="DELETE" %}}
