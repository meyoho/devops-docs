+++
title = "查看集群凭证列表"
description = "/apis/platform.tkestack.io/v1/clustercredentials GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/clustercredentials"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials" verb="GET" %}}
