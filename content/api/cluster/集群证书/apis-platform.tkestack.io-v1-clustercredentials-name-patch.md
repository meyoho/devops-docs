+++
title = "差异化更新集群凭证"
description = "/apis/platform.tkestack.io/v1/clustercredentials/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/clustercredentials/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials/{name}" verb="PATCH" %}}
