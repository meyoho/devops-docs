+++
title = "更新集群凭证"
description = "/apis/platform.tkestack.io/v1/clustercredentials/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/clustercredentials/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clustercredentials/{name}" verb="PUT" %}}
