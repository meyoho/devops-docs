+++
title = "查看 CronHPA 的状态"
description = "/apis/platform.tkestack.io/v1/cronhpas/{name}/status GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/cronhpas/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/cronhpas/{name}/status" verb="GET" %}}
