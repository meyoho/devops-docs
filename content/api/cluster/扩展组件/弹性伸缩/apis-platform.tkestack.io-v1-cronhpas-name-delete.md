+++
title = "删除 CronHPA"
description = "/apis/platform.tkestack.io/v1/cronhpas/{name} DELETE"
weight = 110
path = "DELETE /apis/platform.tkestack.io/v1/cronhpas/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/cronhpas/{name}" verb="DELETE" %}}
