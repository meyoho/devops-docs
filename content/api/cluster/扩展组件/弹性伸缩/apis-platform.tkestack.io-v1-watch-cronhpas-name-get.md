+++
title = "监视指定的 CronHPA"
description = "/apis/platform.tkestack.io/v1/watch/cronhpas/{name} GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/cronhpas/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/cronhpas/{name}" verb="GET" %}}
