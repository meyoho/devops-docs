+++
title = "查看 GPUManager 的状态"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name}/status GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/gpumanagers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}/status" verb="GET" %}}
