+++
title = "差异化更新 GPUManager"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name} PATCH"
weight = 50
path = "PATCH /apis/platform.tkestack.io/v1/gpumanagers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}" verb="PATCH" %}}
