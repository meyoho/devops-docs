+++
title = "删除指定的 GPUManager"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name} DELETE"
weight = 110
path = "DELETE /apis/platform.tkestack.io/v1/gpumanagers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}" verb="DELETE" %}}
