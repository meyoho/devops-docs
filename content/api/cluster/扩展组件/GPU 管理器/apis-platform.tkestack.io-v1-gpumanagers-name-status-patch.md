+++
title = "差异化更新 GPUManager"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name}/status PATCH"
weight = 80
path = "PATCH /apis/platform.tkestack.io/v1/gpumanagers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}/status" verb="PATCH" %}}
