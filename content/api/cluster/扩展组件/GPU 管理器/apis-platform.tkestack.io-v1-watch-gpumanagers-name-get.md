+++
title = "监视指定的 GPUManager"
description = "/apis/platform.tkestack.io/v1/watch/gpumanagers/{name} GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/gpumanagers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/gpumanagers/{name}" verb="GET" %}}
