+++
title = "更新 GPUManager"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/gpumanagers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}" verb="PUT" %}}
