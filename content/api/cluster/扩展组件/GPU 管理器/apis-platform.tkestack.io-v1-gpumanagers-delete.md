+++
title = "批量删除 GPUManager"
description = "/apis/platform.tkestack.io/v1/gpumanagers DELETE"
weight = 120
path = "DELETE /apis/platform.tkestack.io/v1/gpumanagers"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers" verb="DELETE" %}}
