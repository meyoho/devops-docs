+++
title = "GPUManager"
description = "GPU Manager 提供一个 All-in-One 的 GPU 管理器，基于 Kubernets DevicePlugin 插件系统实现,，该管理器提供了分配并共享 GPU、GPU 指标查询、容器运行前的 GPU 相关设备准备等功能，支持用户在 Kubernetes 集群中使用 GPU 设备"
weight = 20
+++


{{% apichildren depth="1" %}}
