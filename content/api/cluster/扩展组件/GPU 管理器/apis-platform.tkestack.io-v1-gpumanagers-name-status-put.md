+++
title = "更新 GPUManager 的状态"
description = "/apis/platform.tkestack.io/v1/gpumanagers/{name}/status PUT"
weight = 70
path = "PUT /apis/platform.tkestack.io/v1/gpumanagers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers/{name}/status" verb="PUT" %}}
