+++
title = "查看 GPUManager 列表"
description = "/apis/platform.tkestack.io/v1/gpumanagers GET"
weight = 10
path = "GET /apis/platform.tkestack.io/v1/gpumanagers"
+++


{{%api path="/apis/platform.tkestack.io/v1/gpumanagers" verb="GET" %}}
