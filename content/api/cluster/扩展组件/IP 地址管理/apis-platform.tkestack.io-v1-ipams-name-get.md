+++
title = "查看 IPAM 详情"
description = "/apis/platform.tkestack.io/v1/ipams/{name} GET"
weight = 30
path = "GET /apis/platform.tkestack.io/v1/ipams/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}" verb="GET" %}}
