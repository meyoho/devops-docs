+++
title = "监视 IPAM 列表"
description = "/apis/platform.tkestack.io/v1/watch/ipams GET"
weight = 90
path = "GET /apis/platform.tkestack.io/v1/watch/ipams"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/ipams" verb="GET" %}}
