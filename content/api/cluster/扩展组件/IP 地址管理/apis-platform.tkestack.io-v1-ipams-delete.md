+++
title = "批量删除 IPAM"
description = "/apis/platform.tkestack.io/v1/ipams DELETE"
weight = 130
path = "DELETE /apis/platform.tkestack.io/v1/ipams"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams" verb="DELETE" %}}
