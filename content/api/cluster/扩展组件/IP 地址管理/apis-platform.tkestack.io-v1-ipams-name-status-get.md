+++
title = "查看 IPAM 的状态"
description = "/apis/platform.tkestack.io/v1/ipams/{name}/status GET"
weight = 60
path = "GET /apis/platform.tkestack.io/v1/ipams/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}/status" verb="GET" %}}
