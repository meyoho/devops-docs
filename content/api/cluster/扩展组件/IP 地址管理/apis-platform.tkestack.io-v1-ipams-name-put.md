+++
title = "更新 IPAM"
description = "/apis/platform.tkestack.io/v1/ipams/{name} PUT"
weight = 40
path = "PUT /apis/platform.tkestack.io/v1/ipams/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/ipams/{name}" verb="PUT" %}}
