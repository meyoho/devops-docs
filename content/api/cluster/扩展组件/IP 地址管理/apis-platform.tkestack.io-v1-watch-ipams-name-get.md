+++
title = "监视指定的 IPAM"
description = "/apis/platform.tkestack.io/v1/watch/ipams/{name} GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/ipams/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/ipams/{name}" verb="GET" %}}
