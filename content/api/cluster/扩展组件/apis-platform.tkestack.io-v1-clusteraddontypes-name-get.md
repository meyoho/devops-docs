+++
title = "查看指定名称的扩展组件类型"
description = "/apis/platform.tkestack.io/v1/clusteraddontypes/{name} GET"
weight = 2
path = "GET /apis/platform.tkestack.io/v1/clusteraddontypes/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/clusteraddontypes/{name}" verb="GET" %}}
