+++
title = "TappController"
description = "Tapp 是一种扩展类型的工作负载，所属容器组实例具有可以标识的 ID，不同的容器组实例支持使用不同的配置，支持同时运行多个版本。"
weight = 40
+++


{{% apichildren depth="1" %}}
