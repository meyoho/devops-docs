+++
title = "监视 TappController 列表"
description = "/apis/platform.tkestack.io/v1/watch/tappcontrollers GET"
weight = 100
path = "GET /apis/platform.tkestack.io/v1/watch/tappcontrollers"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/tappcontrollers" verb="GET" %}}
