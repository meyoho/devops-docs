+++
title = "更新 TappController 的状态"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status PUT"
weight = 80
path = "PUT /apis/platform.tkestack.io/v1/tappcontrollers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status" verb="PUT" %}}
