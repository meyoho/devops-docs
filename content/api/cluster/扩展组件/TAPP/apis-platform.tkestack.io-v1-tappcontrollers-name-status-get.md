+++
title = "查看 TappController 的状态"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status GET"
weight = 70
path = "GET /apis/platform.tkestack.io/v1/tappcontrollers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status" verb="GET" %}}
