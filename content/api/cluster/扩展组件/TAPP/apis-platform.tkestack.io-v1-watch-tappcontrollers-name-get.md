+++
title = "监视指定的 TappController"
description = "/apis/platform.tkestack.io/v1/watch/tappcontrollers/{name} GET"
weight = 110
path = "GET /apis/platform.tkestack.io/v1/watch/tappcontrollers/{name}"
+++


{{%api path="/apis/platform.tkestack.io/v1/watch/tappcontrollers/{name}" verb="GET" %}}
