+++
title = "差异化更新 TappController 的状态"
description = "/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status PATCH"
weight = 90
path = "PATCH /apis/platform.tkestack.io/v1/tappcontrollers/{name}/status"
+++


{{%api path="/apis/platform.tkestack.io/v1/tappcontrollers/{name}/status" verb="PATCH" %}}
