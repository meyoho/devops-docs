+++
title = "查看指定命名空间下的 Prometheus 告警规则列表"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules GET"
weight = 3
path = "GET /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules" verb="GET" %}}
