+++
title = "查看 Prometheus 告警规则详情"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name} GET"
weight = 4
path = "GET /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}" verb="GET" %}}
