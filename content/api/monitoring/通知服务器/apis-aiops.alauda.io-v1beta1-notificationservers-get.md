+++
title = "查看通知服务器列表"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers GET"
weight = 10
path = "GET /apis/aiops.alauda.io/v1beta1/notificationservers"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers" verb="GET" %}}
