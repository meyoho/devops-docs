+++
title = "查看通知服务器详情"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers/{name} GET"
weight = 30
path = "GET /apis/aiops.alauda.io/v1beta1/notificationservers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers/{name}" verb="GET" %}}
