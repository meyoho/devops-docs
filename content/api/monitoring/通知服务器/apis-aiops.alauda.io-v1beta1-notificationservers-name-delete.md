+++
title = "删除指定的通知服务器"
description = "/apis/aiops.alauda.io/v1beta1/notificationservers/{name} DELETE"
weight = 100
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationservers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationservers/{name}" verb="DELETE" %}}
