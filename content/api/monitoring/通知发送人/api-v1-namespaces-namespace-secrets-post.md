+++
title = "创建通知发送人"
description = "/api/v1/namespaces/{namespace}/secrets POST"
weight = 20
path = "POST /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="POST" %}}
