+++
title = "查看通知发送人详情"
description = "/api/v1/namespaces/{namespace}/secrets/{name} GET"
weight = 30
path = "GET /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="GET" %}}
