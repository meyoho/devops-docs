+++
title = "批量删除通知发送人"
description = "/api/v1/namespaces/{namespace}/secrets DELETE"
weight = 10000
path = "DELETE /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="DELETE" %}}
