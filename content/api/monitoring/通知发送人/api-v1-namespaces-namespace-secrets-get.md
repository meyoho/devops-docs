+++
title = "查看通知发送人列表"
description = "/api/v1/namespaces/{namespace}/secrets GET"
weight = 10
path = "GET /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="GET" %}}
