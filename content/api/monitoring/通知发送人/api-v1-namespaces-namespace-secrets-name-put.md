+++
title = "更新通知发送人"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PUT"
weight = 40
path = "PUT /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PUT" %}}
