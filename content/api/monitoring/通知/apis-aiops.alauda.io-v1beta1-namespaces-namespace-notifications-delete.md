+++
title = "批量删除通知"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications DELETE"
weight = 8
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications" verb="DELETE" %}}
