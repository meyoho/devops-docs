+++
title = "差异化更新通知"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name} PATCH"
weight = 5
path = "PATCH /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}" verb="PATCH" %}}
