+++
title = "查看监控模板详情"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates/{name} GET"
weight = 3
path = "GET /apis/aiops.alauda.io/v1beta1/alerttemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates/{name}" verb="GET" %}}
