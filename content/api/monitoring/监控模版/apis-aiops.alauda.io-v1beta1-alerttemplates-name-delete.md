+++
title = "删除指定监控模板"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates/{name} DELETE"
weight = 6
path = "DELETE /apis/aiops.alauda.io/v1beta1/alerttemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates/{name}" verb="DELETE" %}}
