+++
title = "差异化更新监控模板"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates/{name} PATCH"
weight = 4
path = "PATCH /apis/aiops.alauda.io/v1beta1/alerttemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates/{name}" verb="PATCH" %}}
