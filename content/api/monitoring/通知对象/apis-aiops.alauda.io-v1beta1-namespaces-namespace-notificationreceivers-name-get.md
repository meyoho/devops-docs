+++
title = "查看通知对象详情"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name} GET"
weight = 30
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}" verb="GET" %}}
