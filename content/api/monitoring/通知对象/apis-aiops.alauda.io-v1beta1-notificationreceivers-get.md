+++
title = "查看通知对象列表"
description = "/apis/aiops.alauda.io/v1beta1/notificationreceivers GET"
weight = 15
path = "GET /apis/aiops.alauda.io/v1beta1/notificationreceivers"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationreceivers" verb="GET" %}}
