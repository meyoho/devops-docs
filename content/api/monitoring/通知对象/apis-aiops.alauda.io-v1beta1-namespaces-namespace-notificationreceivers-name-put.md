+++
title = "更新通知对象"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name} PUT"
weight = 40
path = "PUT /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}" verb="PUT" %}}
