+++
title = "查看指定命名空间下的通知对象列表"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers GET"
weight = 10
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers" verb="GET" %}}
