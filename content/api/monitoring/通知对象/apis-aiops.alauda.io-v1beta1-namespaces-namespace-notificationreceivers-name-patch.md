+++
title = "差异化更新通知对象"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name} PATCH"
weight = 50
path = "PATCH /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationreceivers/{name}" verb="PATCH" %}}
