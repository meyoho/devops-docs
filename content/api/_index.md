+++
title = "API 文档（Alpha）"
description = "API 文档（Alpha）"
weight = 4
alwaysopen = false
+++

## API 概述

平台默认提供标准 API，您在平台页面上进行的所有操作，我们都有与之对应的标准 API，因此您可以调用标准 API 来完成您在我们平台上的一切操作。

## API 简介

我们提供的 API 是遵循 RESTful 原则进行设计的，允许您使用任何自己喜欢的编程语言与之交互。

目前大部分 API 只接受内容为 application/json 格式的请求，同时使用 application/json 作为返回内容的格式。

**注意：** 请求内容需要使用 UTF-8 进行编码，否则我们将无法对一些特殊字符进行解码。