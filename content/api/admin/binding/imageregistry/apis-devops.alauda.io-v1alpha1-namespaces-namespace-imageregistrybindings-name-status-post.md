+++
title = "创建镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status" verb="POST" %}}
