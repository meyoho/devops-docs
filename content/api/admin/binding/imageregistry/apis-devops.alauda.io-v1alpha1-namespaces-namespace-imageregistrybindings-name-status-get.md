+++
title = "查看指定镜像仓库服务的状态信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status" verb="GET" %}}
