+++
title = "绑定镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings POST"
weight = 1
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings" verb="POST" %}}
