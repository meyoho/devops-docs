+++
title = "删除指定镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status DELETE"
weight = 17
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status" verb="DELETE" %}}
