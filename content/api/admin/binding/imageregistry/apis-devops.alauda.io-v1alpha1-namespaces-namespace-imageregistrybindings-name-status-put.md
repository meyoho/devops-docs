+++
title = "更新镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status PUT"
weight = 10
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status" verb="PUT" %}}
