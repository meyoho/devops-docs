+++
title = "查看镜像仓库服务下的仓库列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/repositories GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/repositories"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/repositories" verb="GET" %}}
