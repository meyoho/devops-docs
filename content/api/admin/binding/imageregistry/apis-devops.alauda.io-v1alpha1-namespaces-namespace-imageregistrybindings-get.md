+++
title = "查看已绑定的镜像仓库服务列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings" verb="GET" %}}
