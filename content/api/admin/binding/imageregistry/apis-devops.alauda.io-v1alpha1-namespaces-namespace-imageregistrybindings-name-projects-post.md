+++
title = "在指定镜像仓库服务下绑定项目"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects POST"
weight = 3
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects" verb="POST" %}}
