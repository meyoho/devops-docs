+++
title = "批量解除绑定镜像仓库"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings DELETE"
weight = 14
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings" verb="DELETE" %}}
