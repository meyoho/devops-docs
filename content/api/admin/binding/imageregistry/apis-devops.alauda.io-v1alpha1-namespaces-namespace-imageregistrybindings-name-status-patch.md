+++
title = "差异化更新镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status PATCH"
weight = 9
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/status" verb="PATCH" %}}
