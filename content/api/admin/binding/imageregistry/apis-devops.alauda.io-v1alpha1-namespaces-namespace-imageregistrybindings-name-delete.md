+++
title = "解除绑定指定镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name} DELETE"
weight = 16
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}" verb="DELETE" %}}
