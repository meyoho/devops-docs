+++
title = "差异化更新镜像仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name} PATCH"
weight = 12
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}" verb="PATCH" %}}
