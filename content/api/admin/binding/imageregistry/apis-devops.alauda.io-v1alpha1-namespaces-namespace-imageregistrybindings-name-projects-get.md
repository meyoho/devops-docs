+++
title = "查看指定镜像仓库服务下绑定的项目"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}/projects" verb="GET" %}}
