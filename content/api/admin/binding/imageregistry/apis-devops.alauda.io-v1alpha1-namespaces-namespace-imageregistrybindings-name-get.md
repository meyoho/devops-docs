+++
title = "查看指定镜像仓库服务的绑定详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name} GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}" verb="GET" %}}
