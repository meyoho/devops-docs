+++
title = "更新镜像仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name} PUT"
weight = 13
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imageregistrybindings/{name}" verb="PUT" %}}
