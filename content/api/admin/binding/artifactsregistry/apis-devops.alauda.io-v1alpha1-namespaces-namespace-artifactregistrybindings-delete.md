+++
title = "批量解除绑定制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings DELETE"
weight = 7
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings" verb="DELETE" %}}
