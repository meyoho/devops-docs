+++
title = "差异化更新制品仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name} PATCH"
weight = 4
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}" verb="PATCH" %}}
