+++
title = "查看绑定的制品仓库服务列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings GET"
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings" verb="GET" %}}
