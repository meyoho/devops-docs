+++
title = "查看制品仓库服务详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}" verb="GET" %}}
