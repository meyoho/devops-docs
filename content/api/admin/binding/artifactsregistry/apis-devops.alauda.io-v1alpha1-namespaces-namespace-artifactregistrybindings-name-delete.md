+++
title = "解除绑定指定制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name} DELETE"
weight = 6
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}" verb="DELETE" %}}
