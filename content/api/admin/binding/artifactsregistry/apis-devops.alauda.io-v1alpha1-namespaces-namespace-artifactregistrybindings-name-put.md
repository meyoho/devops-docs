+++
title = "更新制品仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name} PUT"
weight = 5
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings/{name}" verb="PUT" %}}
