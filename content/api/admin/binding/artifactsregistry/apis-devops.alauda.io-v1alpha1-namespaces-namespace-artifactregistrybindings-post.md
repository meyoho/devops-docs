+++
title = "绑定制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings POST"
weight = 1
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/artifactregistrybindings" verb="POST" %}}
