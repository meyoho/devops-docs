+++
title = "创建 Jenkins API 代理"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/proxy POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/proxy"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/proxy" verb="POST" %}}
