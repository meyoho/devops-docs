+++
title = "差异化更新 Jenkins 的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name} PATCH"
weight = 7
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}" verb="PATCH" %}}
