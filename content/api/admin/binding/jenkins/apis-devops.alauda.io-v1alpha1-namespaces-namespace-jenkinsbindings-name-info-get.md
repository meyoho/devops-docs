+++
title = "查看 Jenkins 返回的 info"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/info GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/info"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}/info" verb="GET" %}}
