+++
title = "批量解除绑定 Jenkins"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings DELETE"
weight = 9
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings" verb="DELETE" %}}
