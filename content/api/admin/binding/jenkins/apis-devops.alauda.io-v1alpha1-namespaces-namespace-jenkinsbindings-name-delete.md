+++
title = "解除绑定指定 Jenkins"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name} DELETE"
weight = 8
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}" verb="DELETE" %}}
