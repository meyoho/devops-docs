+++
title = "查看 Jenkins 的绑定详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}" verb="GET" %}}
