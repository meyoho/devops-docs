+++
title = "更新 Jenkins 的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name} PUT"
weight = 6
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings/{name}" verb="PUT" %}}
