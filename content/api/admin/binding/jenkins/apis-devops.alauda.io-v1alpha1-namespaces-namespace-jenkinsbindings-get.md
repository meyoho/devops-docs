+++
title = "查看 Jenkins 的绑定列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/jenkinsbindings" verb="GET" %}}
