+++
title = "差异化更新代码质量分析服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name} PATCH"
weight = 4
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}" verb="PATCH" %}}
