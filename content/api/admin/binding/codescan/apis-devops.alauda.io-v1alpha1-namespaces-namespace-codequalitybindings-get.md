+++
title = "查看代码质量分析服务列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings" verb="GET" %}}
