+++
title = "解除绑定指定代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name} DELETE"
weight = 6
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}" verb="DELETE" %}}
