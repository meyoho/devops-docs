+++
title = "绑定代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings POST"
weight = 1
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings" verb="POST" %}}
