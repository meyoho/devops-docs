+++
title = "批量解除绑定代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings DELETE"
weight = 7
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings" verb="DELETE" %}}
