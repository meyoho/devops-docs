+++
title = "更新代码质量分析服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name} PUT"
weight = 5
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}" verb="PUT" %}}
