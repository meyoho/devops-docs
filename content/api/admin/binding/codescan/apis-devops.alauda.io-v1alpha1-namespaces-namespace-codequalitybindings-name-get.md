+++
title = "查看代码质量分析服务详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name} GET"
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalitybindings/{name}" verb="GET" %}}
