+++
title = "删除代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status DELETE"
weight = 12
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status" verb="DELETE" %}}
