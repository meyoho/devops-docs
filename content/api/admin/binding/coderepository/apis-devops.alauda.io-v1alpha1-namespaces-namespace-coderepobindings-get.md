+++
title = "查看代码仓库服务的绑定列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings" verb="GET" %}}
