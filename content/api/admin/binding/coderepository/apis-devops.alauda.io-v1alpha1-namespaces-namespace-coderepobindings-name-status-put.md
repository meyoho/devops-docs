+++
title = "更新代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status PUT"
weight = 8
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status" verb="PUT" %}}
