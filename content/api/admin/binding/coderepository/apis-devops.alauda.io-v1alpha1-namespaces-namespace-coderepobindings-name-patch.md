+++
title = "差异化更新代码仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name} PATCH"
weight = 9
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}" verb="PATCH" %}}
