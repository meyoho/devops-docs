+++
title = "解除绑定指定代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name} DELETE"
weight = 13
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}" verb="DELETE" %}}
