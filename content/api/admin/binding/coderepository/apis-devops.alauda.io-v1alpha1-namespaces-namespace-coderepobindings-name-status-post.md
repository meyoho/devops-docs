+++
title = "创建代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status" verb="POST" %}}
