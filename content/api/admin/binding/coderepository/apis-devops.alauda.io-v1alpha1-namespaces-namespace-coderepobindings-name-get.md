+++
title = "查看代码仓库服务的绑定详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}" verb="GET" %}}
