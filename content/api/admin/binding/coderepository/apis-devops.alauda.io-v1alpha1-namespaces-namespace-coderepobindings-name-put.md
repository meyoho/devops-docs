+++
title = "更新代码仓库服务的绑定信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name} PUT"
weight = 10
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}" verb="PUT" %}}
