+++
title = "查看远端代码仓库服务的仓库"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/remoterepositories GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/remoterepositories"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/remoterepositories" verb="GET" %}}
