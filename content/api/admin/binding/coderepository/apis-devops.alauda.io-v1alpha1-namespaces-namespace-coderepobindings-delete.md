+++
title = "批量解除绑定代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings DELETE"
weight = 14
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings" verb="DELETE" %}}
