+++
title = "查看代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepobindings/{name}/status" verb="GET" %}}
