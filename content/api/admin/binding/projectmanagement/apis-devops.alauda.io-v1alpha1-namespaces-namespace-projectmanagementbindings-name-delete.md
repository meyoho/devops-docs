+++
title = "解除绑定指定项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name} DELETE"
weight = 10
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}" verb="DELETE" %}}
