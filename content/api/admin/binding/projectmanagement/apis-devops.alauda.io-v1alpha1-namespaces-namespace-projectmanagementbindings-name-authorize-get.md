+++
title = "验证项目管理工具的凭据"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/authorize GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/authorize" verb="GET" %}}
