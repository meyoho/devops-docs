+++
title = "查看项目管理工具的 issueoptions 详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions" verb="GET" %}}
