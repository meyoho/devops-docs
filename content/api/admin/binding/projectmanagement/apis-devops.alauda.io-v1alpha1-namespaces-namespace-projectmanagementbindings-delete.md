+++
title = "批量解除绑定项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings DELETE"
weight = 11
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings" verb="DELETE" %}}
