+++
title = "绑定项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings POST"
weight = 1
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings" verb="POST" %}}
