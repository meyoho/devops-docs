+++
title = "查看项目管理工具 issue 列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueslist GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueslist"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueslist" verb="GET" %}}
