+++
title = "差异化更新项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name} PATCH"
weight = 9
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}" verb="PATCH" %}}
