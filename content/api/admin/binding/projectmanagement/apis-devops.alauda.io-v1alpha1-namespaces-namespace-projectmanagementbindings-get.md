+++
title = "查看项目管理工具列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings GET"
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings" verb="GET" %}}
