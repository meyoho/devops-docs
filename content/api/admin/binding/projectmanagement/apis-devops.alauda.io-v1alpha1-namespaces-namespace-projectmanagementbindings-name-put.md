+++
title = "更新项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name} PUT"
weight = 8
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}" verb="PUT" %}}
