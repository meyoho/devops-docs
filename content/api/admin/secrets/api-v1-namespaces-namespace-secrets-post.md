+++
title = "创建凭据"
description = "/api/v1/namespaces/{namespace}/secrets POST"
weight = 1
path = "POST /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="POST" %}}
