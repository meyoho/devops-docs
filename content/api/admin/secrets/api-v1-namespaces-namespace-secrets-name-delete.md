+++
title = "删除指定凭据"
description = "/api/v1/namespaces/{namespace}/secrets/{name} DELETE"
weight = 6
path = "DELETE /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="DELETE" %}}
