+++
title = "差异化更新凭据"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PATCH"
weight = 4
path = "PATCH /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PATCH" %}}
