+++
title = "查看凭据列表"
description = "/api/v1/namespaces/{namespace}/secrets GET"
weight = 3
path = "GET /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="GET" %}}
