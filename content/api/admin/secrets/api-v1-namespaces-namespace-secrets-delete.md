+++
title = "批量删除凭据"
description = "/api/v1/namespaces/{namespace}/secrets DELETE"
weight = 7
path = "DELETE /api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets" verb="DELETE" %}}
