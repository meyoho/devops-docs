+++
title = "更新凭据"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PUT"
weight = 5
path = "PUT /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PUT" %}}
