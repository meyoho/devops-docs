+++
title = "更新流水线模版同步资源"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name} PUT"
weight = 5
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}" verb="PUT" %}}
