+++
title = "查看已同步的流水线模版列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs" verb="GET" %}}
