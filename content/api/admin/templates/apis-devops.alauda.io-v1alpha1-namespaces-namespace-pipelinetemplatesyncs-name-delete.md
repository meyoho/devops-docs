+++
title = "删除指定流水线模版同步资源"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name} DELETE"
weight = 6
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}" verb="DELETE" %}}
