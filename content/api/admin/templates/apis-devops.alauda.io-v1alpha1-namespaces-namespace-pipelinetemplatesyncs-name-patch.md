+++
title = "差异化更新流水线模版同步资源"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name} PATCH"
weight = 4
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}" verb="PATCH" %}}
