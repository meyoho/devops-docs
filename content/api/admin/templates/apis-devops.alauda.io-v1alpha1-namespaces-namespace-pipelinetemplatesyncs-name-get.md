+++
title = "查看流水线模版同步信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name} GET"
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs/{name}" verb="GET" %}}
