+++
title = "批量删除流水线模版同步资源"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs DELETE"
weight = 7
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs" verb="DELETE" %}}
