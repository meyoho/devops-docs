+++
title = "同步流水线模版"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs POST"
weight = 1
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplatesyncs" verb="POST" %}}
