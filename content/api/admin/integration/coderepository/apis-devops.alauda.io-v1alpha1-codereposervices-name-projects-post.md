+++
title = "在代码仓库服务下绑定项目"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects POST"
weight = 3
path = "POST /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects" verb="POST" %}}
