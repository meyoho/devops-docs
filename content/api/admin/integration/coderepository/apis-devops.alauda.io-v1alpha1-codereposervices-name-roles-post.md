+++
title = "创建代码仓库服务的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles" verb="POST" %}}
