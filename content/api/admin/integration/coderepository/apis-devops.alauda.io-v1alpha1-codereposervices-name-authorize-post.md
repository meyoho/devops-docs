+++
title = "创建指定代码仓库服务的凭据"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize POST"
weight = 5
path = "POST /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize" verb="POST" %}}
