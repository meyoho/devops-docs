+++
title = "查看指定的代码仓库服务详情"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name} GET"
weight = 10
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}" verb="GET" %}}
