+++
title = "差异化更新代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status PATCH"
weight = 14
path = "PATCH /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status" verb="PATCH" %}}
