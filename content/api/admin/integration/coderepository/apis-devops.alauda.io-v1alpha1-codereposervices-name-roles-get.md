+++
title = "查看指定代码仓库服务的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles GET"
weight = 11
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/roles" verb="GET" %}}
