+++
title = "查看代码仓库服务的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices" verb="GET" %}}
