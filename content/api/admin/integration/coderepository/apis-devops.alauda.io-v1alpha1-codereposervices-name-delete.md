+++
title = "删除指定集成的代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name} DELETE"
weight = 16
path = "DELETE /apis/devops.alauda.io/v1alpha1/codereposervices/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}" verb="DELETE" %}}
