+++
title = "验证指定代码仓库服务的凭据"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/authorize" verb="GET" %}}
