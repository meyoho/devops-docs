+++
title = "更新集成指定代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name} PUT"
weight = 15
path = "PUT /apis/devops.alauda.io/v1alpha1/codereposervices/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}" verb="PUT" %}}
