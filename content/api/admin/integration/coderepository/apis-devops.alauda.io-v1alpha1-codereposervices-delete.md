+++
title = "批量删除集成的代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices DELETE"
weight = 17
path = "DELETE /apis/devops.alauda.io/v1alpha1/codereposervices"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices" verb="DELETE" %}}
