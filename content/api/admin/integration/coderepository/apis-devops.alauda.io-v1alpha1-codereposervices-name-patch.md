+++
title = "差异化更新集成代码仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name} PATCH"
weight = 12
path = "PATCH /apis/devops.alauda.io/v1alpha1/codereposervices/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}" verb="PATCH" %}}
