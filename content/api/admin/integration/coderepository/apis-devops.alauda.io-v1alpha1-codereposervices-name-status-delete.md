+++
title = "删除代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status DELETE"
weight = 18
path = "DELETE /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status" verb="DELETE" %}}
