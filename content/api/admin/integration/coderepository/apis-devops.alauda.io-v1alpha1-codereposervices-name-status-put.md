+++
title = "更新集成代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status PUT"
weight = 13
path = "PUT /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status" verb="PUT" %}}
