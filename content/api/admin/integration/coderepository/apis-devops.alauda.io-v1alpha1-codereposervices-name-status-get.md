+++
title = "查看集成的代码仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/status" verb="GET" %}}
