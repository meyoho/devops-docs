+++
title = "查看代码仓库服务下的项目列表"
description = "/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects GET"
weight = 9
path = "GET /apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codereposervices/{name}/projects" verb="GET" %}}
