+++
title = "验证指定 Jenkins 的凭据"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize" verb="GET" %}}
