+++
title = "查看 Jenkins 的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/jenkinses"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses" verb="GET" %}}
