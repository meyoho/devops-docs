+++
title = "差异化更新 Jenkins 集成信息"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PATCH"
weight = 5
path = "PATCH /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PATCH" %}}
