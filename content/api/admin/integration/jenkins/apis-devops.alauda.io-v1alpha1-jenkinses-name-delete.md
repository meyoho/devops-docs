+++
title = "删除集成 Jenkins "
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} DELETE"
weight = 7
path = "DELETE /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="DELETE" %}}
