+++
title = "查看指定 Jenkins 的集成详情"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} GET"
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="GET" %}}
