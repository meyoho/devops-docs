+++
title = "更新 Jenkins 集成信息"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PUT"
weight = 6
path = "PUT /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PUT" %}}
