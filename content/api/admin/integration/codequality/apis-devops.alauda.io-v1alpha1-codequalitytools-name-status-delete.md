+++
title = "删除指定集成的代码质量分析服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status DELETE"
weight = 14
path = "DELETE /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status" verb="DELETE" %}}
