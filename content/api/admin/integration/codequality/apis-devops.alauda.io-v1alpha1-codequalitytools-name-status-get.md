+++
title = "查看代码质量分析服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status" verb="GET" %}}
