+++
title = "差异化更新集成的代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name} PATCH"
weight = 12
path = "PATCH /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}" verb="PATCH" %}}
