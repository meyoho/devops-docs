+++
title = "批量删除集成的代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools DELETE"
weight = 16
path = "DELETE /apis/devops.alauda.io/v1alpha1/codequalitytools"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools" verb="DELETE" %}}
