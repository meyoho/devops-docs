+++
title = "删除指定集成的代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name} DELETE"
weight = 15
path = "DELETE /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}" verb="DELETE" %}}
