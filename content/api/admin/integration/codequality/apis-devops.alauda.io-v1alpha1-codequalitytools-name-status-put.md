+++
title = "更新集成的代码质量分析服务状态"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status PUT"
weight = 10
path = "PUT /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status" verb="PUT" %}}
