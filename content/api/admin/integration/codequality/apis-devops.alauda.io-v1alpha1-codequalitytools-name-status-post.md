+++
title = "创建代码质量分析服务的状态信息"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status" verb="POST" %}}
