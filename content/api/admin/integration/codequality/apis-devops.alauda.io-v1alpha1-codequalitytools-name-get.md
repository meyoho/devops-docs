+++
title = "查看集成的代码质量分析服务详情"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name} GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}" verb="GET" %}}
