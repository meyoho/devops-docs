+++
title = "查看指定代码质量分析服务的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles" verb="GET" %}}
