+++
title = "更新集成的代码质量分析服务"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name} PUT"
weight = 13
path = "PUT /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}" verb="PUT" %}}
