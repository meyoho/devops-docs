+++
title = "创建代码质量分析服务的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles POST"
weight = 3
path = "POST /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/roles" verb="POST" %}}
