+++
title = "查看代码质量分析服务的凭据"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/authorize GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/authorize" verb="GET" %}}
