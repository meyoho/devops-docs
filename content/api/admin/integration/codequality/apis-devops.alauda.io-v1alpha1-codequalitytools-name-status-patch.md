+++
title = "差异化更新集成的代码质量分析服务状态"
description = "/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status PATCH"
weight = 9
path = "PATCH /apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/codequalitytools/{name}/status" verb="PATCH" %}}
