+++
title = "查看项目管理工具下的用户"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/users GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/users"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/users" verb="GET" %}}
