+++
title = "查看指定项目管理工具的凭据"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/authorize GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/authorize" verb="GET" %}}
