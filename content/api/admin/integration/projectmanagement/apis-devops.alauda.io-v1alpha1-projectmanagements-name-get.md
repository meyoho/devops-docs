+++
title = "查看指定项目管理工具详情"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name} GET"
weight = 10
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}" verb="GET" %}}
