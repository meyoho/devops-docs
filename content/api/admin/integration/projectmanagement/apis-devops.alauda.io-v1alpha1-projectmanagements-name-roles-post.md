+++
title = "在指定项目管理工具下创建角色信息"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles POST"
weight = 4
path = "POST /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles" verb="POST" %}}
