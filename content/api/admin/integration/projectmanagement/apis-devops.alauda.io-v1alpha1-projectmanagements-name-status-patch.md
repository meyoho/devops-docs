+++
title = "差异化更新项目管理工具的状态"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status PATCH"
weight = 14
path = "PATCH /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status" verb="PATCH" %}}
