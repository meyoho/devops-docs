+++
title = "批量删除集成的项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements DELETE"
weight = 17
path = "DELETE /apis/devops.alauda.io/v1alpha1/projectmanagements"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements" verb="DELETE" %}}
