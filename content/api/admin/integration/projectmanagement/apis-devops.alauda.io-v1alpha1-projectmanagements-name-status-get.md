+++
title = "查看指定项目管理工具的状态"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status" verb="GET" %}}
