+++
title = "删除指定集成的项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name} DELETE"
weight = 18
path = "DELETE /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}" verb="DELETE" %}}
