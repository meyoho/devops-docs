+++
title = "查看指定项目管理工具的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/roles" verb="GET" %}}
