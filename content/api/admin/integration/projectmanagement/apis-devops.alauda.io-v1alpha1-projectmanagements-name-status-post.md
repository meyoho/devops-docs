+++
title = "创建指定项目管理工具的状态"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status POST"
weight = 3
path = "POST /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status" verb="POST" %}}
