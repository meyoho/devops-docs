+++
title = "查看项目管理工具的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements GET"
weight = 9
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements" verb="GET" %}}
