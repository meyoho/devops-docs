+++
title = "在指定项目管理工具下创建项目"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects" verb="POST" %}}
