+++
title = "删除项目管理工具的状态"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status DELETE"
weight = 16
path = "DELETE /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status" verb="DELETE" %}}
