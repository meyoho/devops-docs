+++
title = "查看指定项目管理工具下的项目详情"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects GET"
weight = 11
path = "GET /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/remoteprojects" verb="GET" %}}
