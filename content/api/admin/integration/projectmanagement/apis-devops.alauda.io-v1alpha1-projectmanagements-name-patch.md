+++
title = "差异化更新指定项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name} PATCH"
weight = 12
path = "PATCH /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}" verb="PATCH" %}}
