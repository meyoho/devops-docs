+++
title = "更新指定项目管理工具的状态"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status PUT"
weight = 15
path = "PUT /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}/status" verb="PUT" %}}
