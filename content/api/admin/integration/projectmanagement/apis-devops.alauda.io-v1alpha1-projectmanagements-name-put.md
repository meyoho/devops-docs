+++
title = "更新指定项目管理工具"
description = "/apis/devops.alauda.io/v1alpha1/projectmanagements/{name} PUT"
weight = 13
path = "PUT /apis/devops.alauda.io/v1alpha1/projectmanagements/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/projectmanagements/{name}" verb="PUT" %}}
