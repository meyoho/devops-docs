+++
title = "删除指定镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name} DELETE"
weight = 16
path = "DELETE /apis/devops.alauda.io/v1alpha1/imageregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}" verb="DELETE" %}}
