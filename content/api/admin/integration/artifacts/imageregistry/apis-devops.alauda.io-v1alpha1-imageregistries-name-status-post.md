+++
title = "创建指定镜像仓库服务状态"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status" verb="POST" %}}
