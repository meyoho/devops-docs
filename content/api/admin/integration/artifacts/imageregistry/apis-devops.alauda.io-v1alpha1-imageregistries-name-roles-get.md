+++
title = "查看指定镜像仓库服务的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles" verb="GET" %}}
