+++
title = "删除指定镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status DELETE"
weight = 15
path = "DELETE /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status" verb="DELETE" %}}
