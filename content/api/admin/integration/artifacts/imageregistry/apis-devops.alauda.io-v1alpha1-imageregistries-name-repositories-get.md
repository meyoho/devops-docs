+++
title = "查看指定镜像仓库下的仓库"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/repositories GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/repositories"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/repositories" verb="GET" %}}
