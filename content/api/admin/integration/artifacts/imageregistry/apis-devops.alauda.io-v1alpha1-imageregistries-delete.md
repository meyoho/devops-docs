+++
title = "批量删除集成的镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries DELETE"
weight = 17
path = "DELETE /apis/devops.alauda.io/v1alpha1/imageregistries"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries" verb="DELETE" %}}
