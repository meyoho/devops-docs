+++
title = "查看指定镜像仓库服务详情"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name} GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}" verb="GET" %}}
