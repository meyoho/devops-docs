+++
title = "差异化更新指定镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name} PATCH"
weight = 12
path = "PATCH /apis/devops.alauda.io/v1alpha1/imageregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}" verb="PATCH" %}}
