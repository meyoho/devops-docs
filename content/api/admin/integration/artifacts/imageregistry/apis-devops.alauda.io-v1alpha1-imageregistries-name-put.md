+++
title = "更新指定镜像仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name} PUT"
weight = 13
path = "PUT /apis/devops.alauda.io/v1alpha1/imageregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}" verb="PUT" %}}
