+++
title = "差异化更新指定镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status PATCH"
weight = 10
path = "PATCH /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status" verb="PATCH" %}}
