+++
title = "在指定镜像仓库服务下创建角色信息"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles POST"
weight = 4
path = "POST /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/roles" verb="POST" %}}
