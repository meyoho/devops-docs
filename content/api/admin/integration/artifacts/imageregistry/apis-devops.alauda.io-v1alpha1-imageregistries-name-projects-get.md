+++
title = "查看指定镜像仓库服务下绑定的项目列表"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/projects GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/projects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/projects" verb="GET" %}}
