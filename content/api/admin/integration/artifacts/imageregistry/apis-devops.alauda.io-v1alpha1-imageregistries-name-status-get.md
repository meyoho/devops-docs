+++
title = "查看指定镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status" verb="GET" %}}
