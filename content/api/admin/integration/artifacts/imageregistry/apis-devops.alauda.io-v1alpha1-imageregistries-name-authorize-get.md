+++
title = "查看指定镜像仓库的凭据"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/authorize GET"
weight = 9
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/authorize" verb="GET" %}}
