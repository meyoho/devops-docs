+++
title = "更新指定镜像仓库服务的状态"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status PUT"
weight = 11
path = "PUT /apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries/{name}/status" verb="PUT" %}}
