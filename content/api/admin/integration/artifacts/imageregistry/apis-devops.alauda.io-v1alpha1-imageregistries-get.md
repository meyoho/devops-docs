+++
title = "查看镜像仓库服务的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/imageregistries GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/imageregistries"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/imageregistries" verb="GET" %}}
