+++
title = "更新集成的制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name} PUT"
weight = 7
path = "PUT /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}" verb="PUT" %}}
