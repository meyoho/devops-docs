+++
title = "查看制品仓库服务的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistries"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries" verb="GET" %}}
