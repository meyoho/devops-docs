+++
title = "删除指定集成的制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name} DELETE"
weight = 9 
path = "DELETE /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}" verb="DELETE" %}}
