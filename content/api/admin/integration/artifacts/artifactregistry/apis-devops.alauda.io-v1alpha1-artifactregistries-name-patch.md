+++
title = "差异化更新集成的制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name} PATCH"
weight = 8
path = "PATCH /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}" verb="PATCH" %}}
