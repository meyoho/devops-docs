+++
title = "查看集成的制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}" verb="GET" %}}
