+++
title = "批量删除集成的制品仓库服务"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries DELETE"
weight = 10
path = "DELETE /apis/devops.alauda.io/v1alpha1/artifactregistries"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries" verb="DELETE" %}}
