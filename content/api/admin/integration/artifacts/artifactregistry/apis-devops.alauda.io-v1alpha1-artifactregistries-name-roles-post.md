+++
title = "在指定制品仓库服务下创建角色信息"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles" verb="POST" %}}
