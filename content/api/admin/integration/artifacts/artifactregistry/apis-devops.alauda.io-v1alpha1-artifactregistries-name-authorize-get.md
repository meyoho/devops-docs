+++
title = "查看指定制品仓库服务的凭据"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/authorize GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/authorize" verb="GET" %}}
