+++
title = "查看指定制品仓库下的角色信息"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistries/{name}/roles" verb="GET" %}}
