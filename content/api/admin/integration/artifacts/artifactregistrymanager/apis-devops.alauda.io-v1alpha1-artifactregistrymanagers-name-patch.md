+++
title = "差异化更新制品仓库管理工具"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name} PATCH"
weight = 9
path = "PATCH /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}" verb="PATCH" %}}
