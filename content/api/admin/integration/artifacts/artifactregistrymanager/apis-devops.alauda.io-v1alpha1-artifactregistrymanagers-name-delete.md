+++
title = "删除指定集成的制品仓库管理工具"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name} DELETE"
weight = 11
path = "DELETE /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}" verb="DELETE" %}}
