+++
title = "查看制品仓库管理工具下的仓库"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/repository GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/repository"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/repository" verb="GET" %}}
