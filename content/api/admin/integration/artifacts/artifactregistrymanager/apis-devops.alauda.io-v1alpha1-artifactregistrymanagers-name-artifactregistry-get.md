+++
title = "查看在指定制品仓库管理工具下的制品仓库"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/artifactregistry GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/artifactregistry"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/artifactregistry" verb="GET" %}}
