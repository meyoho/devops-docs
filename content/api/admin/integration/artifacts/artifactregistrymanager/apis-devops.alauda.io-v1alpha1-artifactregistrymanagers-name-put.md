+++
title = "更新制品仓库管理工具"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name} PUT"
weight = 10
path = "PUT /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}" verb="PUT" %}}
