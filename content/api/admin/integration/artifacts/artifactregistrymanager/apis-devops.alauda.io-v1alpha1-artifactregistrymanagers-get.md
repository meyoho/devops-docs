+++
title = "查看制品仓库管理工具的集成列表"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers" verb="GET" %}}
