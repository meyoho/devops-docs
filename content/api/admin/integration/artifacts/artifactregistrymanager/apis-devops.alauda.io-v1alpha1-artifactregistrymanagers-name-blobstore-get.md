+++
title = "查看指定制品仓库管理工具管理器存储的数据"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/blobstore GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/blobstore"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/blobstore" verb="GET" %}}
