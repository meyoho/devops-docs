+++
title = "查看指定制品仓库管理工具凭据"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/authorize GET"
weight = 5
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/authorize" verb="GET" %}}
