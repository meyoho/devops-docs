+++
title = "批量删除集成的制品仓库管理工具"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers DELETE"
weight = 12
path = "DELETE /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers" verb="DELETE" %}}
