+++
title = "在制品仓库管理工具下绑定项目"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/project POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/project"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}/project" verb="POST" %}}
