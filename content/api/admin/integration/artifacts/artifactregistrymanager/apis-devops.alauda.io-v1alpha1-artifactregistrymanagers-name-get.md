+++
title = "查看指定制品仓库管理工具详情"
description = "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/{name}" verb="GET" %}}
