+++
title = "查看用户和角色的绑定关系详情"
description = "/apis/auth.alauda.io/v1/userbindings/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/userbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings/{name}" verb="GET" %}}
