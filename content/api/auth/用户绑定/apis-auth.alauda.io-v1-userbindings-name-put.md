+++
title = "更新用户和角色的绑定关系信息"
description = "/apis/auth.alauda.io/v1/userbindings/{name} PUT"
weight = 5
path = "PUT /apis/auth.alauda.io/v1/userbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings/{name}" verb="PUT" %}}
