+++
title = "更新计量报表"
description = "/apis/metering.alauda.io/v1beta1/reports/{name} PUT"
weight = 40
path = "PUT /apis/metering.alauda.io/v1beta1/reports/{name}"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}" verb="PUT" %}}
