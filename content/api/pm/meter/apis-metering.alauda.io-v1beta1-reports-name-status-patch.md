+++
title = "差异化更新计量报表的状态"
description = "/apis/metering.alauda.io/v1beta1/reports/{name}/status PATCH"
weight = 80
path = "PATCH /apis/metering.alauda.io/v1beta1/reports/{name}/status"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}/status" verb="PATCH" %}}
