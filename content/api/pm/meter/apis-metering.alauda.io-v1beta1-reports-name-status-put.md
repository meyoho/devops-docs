+++
title = "更新计量报表的状态"
description = "/apis/metering.alauda.io/v1beta1/reports/{name}/status PUT"
weight = 70
path = "PUT /apis/metering.alauda.io/v1beta1/reports/{name}/status"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}/status" verb="PUT" %}}
