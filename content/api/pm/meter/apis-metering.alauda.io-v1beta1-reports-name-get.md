+++
title = "查看计量报表详情"
description = "/apis/metering.alauda.io/v1beta1/reports/{name} GET"
weight = 30
path = "GET /apis/metering.alauda.io/v1beta1/reports/{name}"
+++


{{%api path="/apis/metering.alauda.io/v1beta1/reports/{name}" verb="GET" %}}
