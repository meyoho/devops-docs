+++
title = "查看指定镜像仓库版本信息"
description = ""
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories/{name}/tags"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories/{name}/tags" verb="GET" %}}
