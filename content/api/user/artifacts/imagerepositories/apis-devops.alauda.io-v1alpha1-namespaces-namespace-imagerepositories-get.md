+++
title = "查看镜像仓库列表"
description = ""
weight = 1
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories" verb="GET" %}}
