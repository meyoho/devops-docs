+++
title = "查看镜像仓库漏洞扫描结果"
description = ""
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories/{name}/security"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/imagerepositories/{name}/security" verb="GET" %}}
