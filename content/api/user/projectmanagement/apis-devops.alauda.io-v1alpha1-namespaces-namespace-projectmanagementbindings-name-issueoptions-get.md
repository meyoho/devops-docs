+++
title = "查看敏捷项目管理的 issue 筛选选项"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions GET"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issueoptions" verb="GET" %}}
