+++
title = "查看敏捷项目管理工具的 issue 详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issue GET"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issue"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/projectmanagementbindings/{name}/issue" verb="GET" %}}
