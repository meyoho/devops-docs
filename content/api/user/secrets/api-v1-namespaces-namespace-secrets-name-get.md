+++
title = "查看凭据详情"
description = ""
weight = 3
path = "GET /api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="GET" %}}
