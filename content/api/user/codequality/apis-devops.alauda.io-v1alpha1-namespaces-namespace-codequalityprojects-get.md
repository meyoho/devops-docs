+++
title = "查看代码质量分析列表"
description = ""
weight = 1
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalityprojects"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/codequalityprojects" verb="GET" %}}
