+++
title = "查看代码仓库列表"
description = ""
weight = 1
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepositories"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepositories" verb="GET" %}}
