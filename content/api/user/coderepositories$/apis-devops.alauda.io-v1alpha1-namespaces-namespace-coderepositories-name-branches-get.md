+++
title = "查看指定代码仓库的分支"
description = ""
weight = 2
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepositories/{name}/branches"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/coderepositories/{name}/branches" verb="GET" %}}
