+++
title = "更新流水线执行记录"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name} PUT"
weight = 12
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}" verb="PUT" %}}
