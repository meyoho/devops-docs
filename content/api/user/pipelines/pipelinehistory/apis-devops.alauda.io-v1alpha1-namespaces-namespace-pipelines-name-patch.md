+++
title = "差异化更新流水线执行记录"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name} PATCH"
weight = 11
path = "PATCH /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}" verb="PATCH" %}}
