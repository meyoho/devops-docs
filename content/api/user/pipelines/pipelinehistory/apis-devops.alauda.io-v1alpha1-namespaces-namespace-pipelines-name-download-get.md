+++
title = "下载指定流水线产生的制品文件"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/download GET"
weight = 9
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/download"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/download" verb="GET" %}}
