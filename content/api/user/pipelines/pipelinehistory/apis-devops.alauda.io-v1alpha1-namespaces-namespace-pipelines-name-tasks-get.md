+++
title = "查看流水线上所有 task"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/tasks GET"
weight = 6
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/tasks"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/tasks" verb="GET" %}}
