+++
title = "批量删除流水线执行记录"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines DELETE"
weight = 14
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines" verb="DELETE" %}}
