+++
title = "删除指定流水线执行记录"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name} DELETE"
weight = 13
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}" verb="DELETE" %}}
