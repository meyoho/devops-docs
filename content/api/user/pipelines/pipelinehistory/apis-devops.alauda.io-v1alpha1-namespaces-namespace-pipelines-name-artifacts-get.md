+++
title = "查看指定流水线产生的制品"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/artifacts GET"
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/artifacts"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/artifacts" verb="GET" %}}
