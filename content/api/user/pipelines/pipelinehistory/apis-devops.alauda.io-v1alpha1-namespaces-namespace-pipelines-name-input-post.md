+++
title = "执行等待输入"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/input POST"
weight = 2
path = "POST /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/input"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/input" verb="POST" %}}
