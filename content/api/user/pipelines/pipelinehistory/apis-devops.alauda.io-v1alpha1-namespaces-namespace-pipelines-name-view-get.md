+++
title = "查看指定流水线的过程信息"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/view GET"
weight = 10
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/view"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/view" verb="GET" %}}
