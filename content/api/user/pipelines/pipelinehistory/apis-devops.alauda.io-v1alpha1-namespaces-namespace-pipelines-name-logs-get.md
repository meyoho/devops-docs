+++
title = "查看流水线执行记录日志"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/logs GET"
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/logs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}/logs" verb="GET" %}}
