+++
title = "查看流水线执行记录列表"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines GET"
weight = 4
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines" verb="GET" %}}
