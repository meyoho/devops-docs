+++
title = "查看流水线执行记录详情"
description = "/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name} GET"
weight = 3
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelines/{name}" verb="GET" %}}
