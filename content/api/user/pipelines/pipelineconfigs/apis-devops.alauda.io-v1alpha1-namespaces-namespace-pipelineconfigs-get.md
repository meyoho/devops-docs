+++
title = "查看流水线列表"
description = ""
weight = 8
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs" verb="GET" %}}
