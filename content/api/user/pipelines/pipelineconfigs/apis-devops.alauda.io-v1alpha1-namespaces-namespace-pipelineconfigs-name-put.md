+++
title = "更新流水线"
description = "更新流水线"
weight = 11
path = "PUT /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}" verb="PUT" %}}
