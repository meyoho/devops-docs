+++
title = "查看流水线日志"
description = "查看流水线日志"
weight = 9
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}/logs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}/logs" verb="GET" %}}
