+++
title = "查看流水线详情"
description = ""
weight = 7
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}" verb="GET" %}}
