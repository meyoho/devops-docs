+++
title = "批量删除流水线"
description = ""
weight = 15
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs" verb="DELETE" %}}
