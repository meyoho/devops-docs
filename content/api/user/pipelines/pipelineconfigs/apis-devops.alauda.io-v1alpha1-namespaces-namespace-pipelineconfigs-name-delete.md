+++
title = "删除指定流水线"
description = "删除流水线"
weight = 12
path = "DELETE /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelineconfigs/{name}" verb="DELETE" %}}
