+++
title = "查看官方 task 流水线模版"
description = "查看官方 task 流水线模版"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/clusterpipelinetasktemplates"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/clusterpipelinetasktemplates" verb="GET" %}}
