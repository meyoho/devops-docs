+++
title = "查看自定义 task 模版"
description = "查看自定义 task 模版"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetasktemplates"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetasktemplates" verb="GET" %}}
