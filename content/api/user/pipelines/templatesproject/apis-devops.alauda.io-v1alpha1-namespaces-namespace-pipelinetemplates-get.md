+++
title = "查看自定义流水线模版"
description = "查看自定义流水线模版"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplates"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/namespaces/{namespace}/pipelinetemplates" verb="GET" %}}
