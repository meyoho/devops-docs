+++
title = "认证鉴权"
description = "平台目前支持使用 `Token` 进行身份认证，即在请求头中需加入 `Token` 信息来标识发送请求的用户的身份。"
weight = 2
+++

平台目前支持使用 `Token` 进行身份认证，即在请求头中需加入 `Token` 信息来标识发送请求的用户的身份。

Token 在计算机系统中代表令牌（临时）的意思，拥有 Token 就代表拥有某种权限。Token 认证就是在调用API的时候将 Token 加到请求消息头，从而通过身份认证，获得调用 API 操作平台资源的权限。

`Token` 在请求头（Headers）中的具体格式如下：

| KEY |  Value  |
|---------|---------|
| Authorization | `Bearer {your token}` |

**说明**：`{your token}` 为您通过平台获取的 Token，Token 在请求头中的输入格式如：`Bearer eyJhxxxxxxxxxxxxxxxUiOiJhZG1pbiIsImV4dCI6eyJpc19hZG1pbiI6dHJ1ZSwiY29ubl9pZCI6ImxvY2FsIn19.oQ`。


以下示例为通过命令行调用接口时 Token 的使用方法。

```
## List services
curl -X "GET" "https://{IP 地址或域名}/auth/v1/users?limit=20" \
     -H "Authorization: Bearer <your token>"
```


## 获取 Token

我们所提供的对外的 API 都需要使用 `Token` 进行认证与鉴权，因此在使用我们的 API 之前您首先需要获取一个有效的 Token。

平台的个人中心将根据您在平台上拥有的权限，为您自动生成一个 Token，成功登录平台后即可获取 Token。因此，您在获取 Token 之前，需要先拥有一个可正常登录平台的账号，如果您还没有平台的账号，可以通过联系平台管理员申请开通账号。

在您成功并登录平台后，您需要点击界面右上角的 ***账号名称*** > **个人信息**，进入用户中心页面。在用户中心页面，您可以通过点击 **平台 Token** 来拿到您的专属 `Token`。

**注意**：
目前，您通过平台获取到的 `Token` 唯一，为避免他人使用您的 Token 对平台执行未经许可的操作，请不要随意分享您的 `Token`。