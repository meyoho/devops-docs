+++
title = "更新项目的状态"
description = "/apis/auth.alauda.io/v1/projects/{name}/status PUT"
weight = 8
path = "PUT /apis/auth.alauda.io/v1/projects/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}/status" verb="PUT" %}}
