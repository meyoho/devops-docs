+++
title = "查看项目的状态"
description = "/apis/auth.alauda.io/v1/projects/{name}/status GET"
weight = 4
path = "GET /apis/auth.alauda.io/v1/projects/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}/status" verb="GET" %}}
