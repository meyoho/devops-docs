+++
title = "批量解除项目和集群的绑定关系"
description = "/apis/auth.alauda.io/v1/projectbindings DELETE"
weight = 7
path = "DELETE /apis/auth.alauda.io/v1/projectbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings" verb="DELETE" %}}
