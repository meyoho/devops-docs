+++
title = "查看项目配额的状态"
description = "/apis/auth.alauda.io/v1/projectquotas/{name}/status GET"
weight = 4
path = "GET /apis/auth.alauda.io/v1/projectquotas/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}/status" verb="GET" %}}
