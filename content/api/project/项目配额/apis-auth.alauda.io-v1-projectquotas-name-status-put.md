+++
title = "更新项目配额的状态"
description = "/apis/auth.alauda.io/v1/projectquotas/{name}/status PUT"
weight = 8
path = "PUT /apis/auth.alauda.io/v1/projectquotas/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}/status" verb="PUT" %}}
