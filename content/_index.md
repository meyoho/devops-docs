+++
title = "DevOps"
description = "DevOps"
+++

# 欢迎使用 DevOps 平台

## 产品介绍

DevOps 是一款基于容器的云应用平台，支持容器负载统一调度，细粒度的多租户权限。采用 Kubernetes 原生架构编排 DevOps 工具链，可灵活兼容客户已有的工具选型，同时，可适配未来可能的选型变化。

旨在为客户建立一套能够快速、高频、稳定地进行构建、测试、发布软件的开发运维一体化环境，缩短研发周期，实现动态高频部署，提升工作效率。

使用 DevOps，有助于： 

* 简化日常工作，实现流程最大程度的自动化

* 快速迭代，提升组织工作效率
￼
* 角色清晰，保证资源隔离和项目合作

## 用户手册

<ul class="children children-table">
   <span><span><a href="{{< relref "10usermanual/project/cd" >}}"> 持续交付</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/scrum">}}"> 敏捷项目管理</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/coderepo">}}"> 代码仓库</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/artifact">}}"> 制品仓库</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/sonar">}}"> 代码质量分析</a></span></span>
   <span><span><a href="{{< relref "10usermanual/admin/service">}}"> DevOps 工具链</a></span></span>
</ul>


## API 文档

<ul class="children children-table">
   <span><span><a href="{{< relref "api/user/pipelines/pipelineconfigs">}}"> 流水线</a></span></span>
   <span><span><a href="{{< relref "api/admin/templates">}}"> 流水线模版</a></span></span>
   <span><span><a href="{{< relref "api/user/coderepositories$">}}"> 代码仓库</a></span></span>
   <span><span><a href="{{< relref "api/user/artifacts">}}"> 制品仓库</a></span></span>
   <span><span><a href="{{< relref "api/user/codequality">}}"> 代码质量分析</a></span></span>
   <span><span><a href="{{< relref "api/user/projectmanagement">}}"> 敏捷项目管理</a></span></span>
</ul>

## 常见问题

<ul class="children children-table">
   <span><span><a href="{{< relref "100faq/p0">}}"> 平台兼容的 PC 端浏览器有哪些</a></span></span>
   <span><span><a href="{{< relref "100faq/faq10">}}"> Jenkins绑定项目失败的原因</a></span></span>
   <span><span><a href="{{< relref "100faq/faq11">}}"> 自动同步仓库后仓库未显示的原因
</ul>

