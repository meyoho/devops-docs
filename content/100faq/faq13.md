+++
title = "Token、OAuth2 两种认证方式的区别"
Description = "在 DevOps 工具链中，对已添加的工具绑定项目时，认证方式有 Token、OAuth2，这两种认证方式在使用时有什么区别？"
weight = 13
+++

**问题描述**：

在 DevOps 工具链中，对已添加的工具进行绑定项目时，认证方式中有 Token、OAuth2，这两种认证方式在使用时有什么区别。

**解决方法**：
		
1. 仅认证模式不同，使用无差别。

* **Token 认证**：需在绑定的工具设置 Token 权限（权限由用户自主设置，可修改）并生成 Token，用生成的 Token 复制到项目中绑定，绑定时无需跳转到第三方授权。

* **OAuth2 认证**：需在绑定的工具创建 OAuth2 应用（根据平台不同，权限可由平台默认设置，也可由用户自主设置，建议全选），并根据生成的 Client ID 和 Client Secret 绑定项目，绑定时需跳转到第三方工具授权。
	 