+++
title = "Nexus 的展示形式和其他工具不同的原因"
Description = "在 DevOps 工具链中，Nexus 的展示形式为什么和其他的工具不一样？"
weight = 14
+++

**问题描述**：

在 DevOps 工具链中，Nexus 的展示形式与其他工具不同。

**解决方法**：
	
Nexus 为仓库管理器，用户真正会使用的是 Maven、PYPI、NPM 等制品仓库，Nexus 只是用于这些仓库的管理，不同于其他的单一仓库，其下会存在多种仓库并对其进行展示。


	

	 