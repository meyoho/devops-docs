+++
title = "Jenkins 绑定项目失败原因"
Description = "已集成的 Jenkins 绑定项目，添加凭据时，输入用户名+密码提示：账号绑定失败。"
weight = 10
+++

**问题描述**：

对于已集成的 Jenkins工具，在绑定项目添加凭据时，输入用户名和密码提示：**账号绑定失败**。

**解决方法**：
	
添加凭据时，密码处应为 Jenkins API Token 而非 Jenkins 的登陆密码，需要前往 Jenkins 生成 API Token。

![faq1](/img/faq1.png?classes=big)


	

	 