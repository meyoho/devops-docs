+++
title = "如何实现在自定义流水线模版中添加通知？"
Description = "如何实现在自定义流水线模版中添加通知？"
weight = 15
+++

**问题描述**：

在 DevOps 2.8 版本中新增流水线通知功能，虽然在所有官方模版中已经实现通知功能开关，但是如何实现在自定义流水线模版中添加通知功能？

原理概述：通知会绑定通知发送人、通知对象、通知服务器和通知模板。通知信息可以绑定多个通知，平台根据数据和通知模板渲染成通知信息，发送给通知对象。

**注意**：实现此功能目前需要您跳转到 **容器平台** 中创建通知和使用默认的模版，**若您只购买了 DevOps 产品，暂时不支持此功能**。

**操作步骤**：
	
1. 您需要跳转到 **容器平台** 上的 **管理视图** 中 **创建通知**，请填写通知名称、通知方式，其中通知方式中的通知模板请选择 DevOps 平台默认模版：`notificationtemplate-devops-default`，具体操作请详见容器平台用户手册中的 **创建通知** 部分。

2. 当您配置好通知后，登录 **DevOps** 平台，选择 ***项目名称***，进入 **业务视图** ，单击 **持续交付** > **流水线** > **创建流水线** > **脚本创建**。

3. 在 **基本信息** 步骤，配置流水线的基本信息：
		
	* 在 **名称** 框中，输入 Jenkins 流水线的名称。  
	名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线或数字开头且不支持以中横线为结尾。

	* (非必填) 在 **显示名称** 框中，输入 Jenkins 流水线的显示名称，支持中文字符。如未输入，默认显示为空。
		
	* 在 **Jenkins 实例** 框中，选择一个已创建的 Jenkins 实例。
				
	* **Jenkinsfile 位置** ：选择获取 Jenkinsfile 文件的方式，选择**页面编写**。
		
	* **执行方式**：选择流水线的执行方式，支持 **依次执行** 和 **并发执行**：
		
		* **依次执行**：流水线被触发后，在没有结束执行前，如果再次触发，后一次触发的流水线会变成等待状态。
		
		* **并发执行**：流水线被触发后，在没有结束执行前，如果再次触发，两次触发的流水线会同时执行。
		
	配置完成后，单击 **下一步**。
	
4. 在 **Jenkinsfile** 步骤，编写 Jenkinsfile，通知部分 Jenkinsfile 代码如下。

	```
	alaudaPipeline.appendInfo("name", ["key":"value"], Desc)//在流水线中增加过程信息。
	
	alaudaPipeline.createOrUpdateInfo("summary", [status: currentBuild.currentResult, 
	create_date: new Date().format("yyyy-MM-dd HH:mm:ss"), link: BUILD_URL], "_summary")//概要信息
	
	def data = alaudaPipeline.getData();
	alaudaDevops.notificationSender()
	    .bindingNotification(notificationNameSpace, noificationName)//获取已创建通知的命名空间和通知名称
	    .setData(data)//渲染流水线执行中产生的信息
	    .send()//发送信息
	```
	使用一个 Jenkinsfile 举例说明脚本创建流水线中，如何使用通知，Jenkinsfile 如下：
	
	```
	pipeline {
  agent {
    label 'golang'
  }
  stages {
    stage('Checkout') {
      steps {
        echo "Checkout…"
        script {
          alaudaPipeline.appendInfo("name-test","abc","string", "descaaa")
          alaudaPipeline.appendInfo("名字","数据")
        }
      }
    }
  }
    post{
	    always{
	        script {
            alaudaPipeline.createOrUpdateInfo("summary", [status: currentBuild.currentResult, 
            create_date: new Date().format("yyyy-MM-dd HH:mm:ss"), link: BUILD_URL], "_summary")
            
                    def data = alaudaPipeline.getData();
                    alaudaDevops.notificationSender()
                    .setData(data)
                    .bindingNotification("cpaas-system", "document")
                    .send()
    			}
  			}
		}
}
	```

	编写完成后，单击 **下一步**。
			
5. 在 **触发器** 步骤，根据实际策略，选择定时扫描或设置定时触发器，并且支持多选操作。开启定时扫描或设置触发器，配置相应的触发规则，可实现流水线的自动触发。
		
	* **定时扫描**：在指定的时间检查代码仓库是否有变更，如有变更，将触发流水线。支持选择预设的触发器规则。  
	例如选择 **每 2 分钟检查新提交**，则每隔 2 分钟系统会自动检查代码仓库是否有新提交的变更，若有新提交的变更则会执行流水线。
		
	* **定时触发器**：在指定的时间触发流水线。选择 **定时触发器** 后，在 **触发器规则** 区域，选择星期中的某一天或某几天，再单击 **配置时间**，选择某个特定时间来触发流水线，支持添加多个时间点的触发。单击 **自定义**，输入自定义触发器规则。 
		
	自定义触发器规则的语法说明，参考 [触发器规则]({{< relref "10usermanual/project/CD/pipeline/timetrigger.md" >}})。
			
	配置完成后，单击 **创建**。流水线创建完成后，跳转到流水线详情页面。

6. 单击右上角 **操作** > **执行**，等待流水线执行完毕后，您会收到通知，示例如下：
![faq1](/img/faq15.png?classes=big)

	 