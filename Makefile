PWD=$(shell pwd)
TAG=dev-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/devops-docs
image=$(IMAGE)
tag=$(TAG)

.PHONE: setup
setup:
	echo "check this document: https://bitbucket.org/mathildetech/hugo-alauda-theme/src"

run:
	kubectl devops apidocs run

build-site:
	kubectl devops apidocs build

build-pdf:
	kubectl devops apidocs build-pdf --pdf-page-url /pdf --target-file static/docs.pdf

build-docker:
	# cp artifacts/Caddyfile public
	docker build -t ${IMAGE}:${TAG} -f artifacts/Dockerfile .
	docker push ${IMAGE}:${TAG}

build: build-docker


apidocs-all:
	@echo "===========> Merging swagger"
	kubectl devops apidocs merge --config=apidocs.yaml --force
	@echo "===========> Generating translation from swagger"
	kubectl devops apidocs translate --config=apidocs.yaml --output-file translations/en.json --force
	@echo "===========> Fetching translations. TODO: Change to use weblate..."
	cp translations/en.json translations/zh.json
	@echo "===========> Converting translations to hugo format"
	kubectl devops apidocs convert --config=apidocs.yaml --input-folder translations --output-folder i18n
	@echo "===========> Generating docs content"
	kubectl devops apidocs generate --config=apidocs.yaml --data-folder data
