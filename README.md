# DevOps平台文档网站

[TOC]

## Getting started

### Installing the tools

#### Automagically

run: `make setup`

#### Manual install

  1. Install [brew](https://brew.sh/): 
      `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
  2. Install [hugo](https://gohugo.io):
     `brew install hugo`
  3. Install [sass](https://sass-lang.com/):
     `sudo gem install sass`
  4. Install [go](https://golang.org):
     `brew install go`
  5. Install [linkcrawler](https://github.com/danielfbm/linkcrawler)
     `go get -u github.com/danielfbm/linkcrawler`
  6. Install [wkhtmltopdf](https://wkhtmltopdf.org/)
     `curl -L -O https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg && open wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg`



### Developing

In the root folder of the project: `make run -j2` or `hugo server`

If there is a need to change the flags used to start hugo please check the [documentation](https://gohugo.io/getting-started/usage/) or run `hugo help`

Once the hugo server starts it will reload automatically once the content has changed

### Building website's PDF

Quit your hugo server if any and type:

`make build-pdf`

This will create a pdf file inside the `static/docs.pdf`

### Adding content

hugo comes with many commands to add new documents quickly, but it is strongly recommended to understand the underlying structure and the why things work to avoid unexpected results. For more information regarding content management please check the [documentation](https://gohugo.io/content-management/)



### Changing PDF cover

1. Open `static/cover.html` html file
2. Do any changes in style or text
3. Build PDF


### Changing product name

1. Open `themes/hugo-theme-docdock/i18n/zh.toml`
2. change the following section:

```
[Product]
other = "DevOps"
```

3. any other translations can be changed here


### Download latest swagger API version

**Attention: Before updating the file, please make sure the current API files and versions are correct"**

The main steps are:

1. Download the new swagger document file from an environment
2. Save according to versioning standards (refer to documentation)
3. Change `apidocs.yaml` to refer to new version files
4. Run the proceedures as usual (merging, generation, translation, etc.). For specifics on the overall workflow please refer to documentation

A full example of the API update process below:

Lets say that the new version is v2.8. For comprehention sake lets use the same step numbers:
1 and 2. Download file in a `v2.8` folder inside `api` folder: `api/v2.8/k8s.json`
3. Change `apidocs.yaml` most top part to:

```yaml
source:
  version: v2.8                     # 当前 API 版本（产品版本）
  folderPath: api/versions          # 保存最终合并过的文档
  files:                            # 声明所有的原swagger/crd文件
  - name: k8s                       # 名称，没有具体使用
    folderPath: api/v2.8/k8s.json   # swagger 文件路径
    apiPathPrefix: ""               # API路径前缀（增加固定的前缀

# the rest of the document
```

#### from prod environment

```bash
export CURRENT_VERSION=v2.8
export ENV_PROXY="http://alauda:Ah%23U4TSwnjERBU%40E1KZN8@139.186.17.154:52975"
mkdir -p api/$CURRENT_VERSION
curl --proxy $ENV_PROXY -k https://cloud-prod.alauda.cn:6443/swagger.json > api/$CURRENT_VERSION/k8s.json
```

#### from staging environment


**Change the `CURRENT_VERSION` env below**

```bash
export CURRENT_VERSION=v2.8
export ENV_PROXY="http://alauda:Ah%23U4TSwnjERBU%40E1KZN8@139.186.17.154:52975"
mkdir -p api/$CURRENT_VERSION
curl --proxy $ENV_PROXY -k https://10.0.129.100:6443/swagger.json > api/$CURRENT_VERSION/k8s.json
```